import {BinanceRoutes, BinanceRouteArgs, BinanceRouteResponse, BinanceRouteOptions, Tail} from './src/model';

function request<M extends keyof BinanceRoutes, P extends keyof BinanceRoutes[M]>(method: M, path: P, ...args: BinanceRouteArgs<M, P>): BinanceRouteResponse<M, P> {
	const [params = {}, options = {}] = args;

	return undefined as any;
}

const x = request('GET', '/api/v3/ping');
const y = request('POST', '/api/v3/order', {
	params: {
		symbol: 'BTC',
		side: 'BUY',
		type: 'MARKET'
	}
});
const z = request('GET', '/api/v3/ticker/24hr', {
	weight: {"IP": 4}
})
