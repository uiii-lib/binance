import crypto from 'crypto';
import axios, { AxiosResponse, Method } from "axios";
import moment from 'moment';
import { differenceInMilliseconds, endOfDay, endOfMinute } from 'date-fns';

import { sleep } from '@uiii-lib/helpers';

import { enqueueRequest } from '../../core/helpers/enqueu-request';

import { BinanceExchangeInfo } from './model/exchange-info';

import { WsClient, WsSendOptions } from './ws-client';
import { BinanceRouteArgs, BinanceRouteOptions, BinanceRouteResponse, BinanceRoutes, binanceRoutesConfig } from './model';
import { binanceRoutesOptions } from '../apigen';

export interface BinanceAccessData {
	apiKey: string;
	apiSecret: string;
}

export interface BinanceApiRequestOptions {
	payload?: any;
	sign?: boolean;
	useTimestamp?: boolean;
	skipRateLimit?: boolean;
}

export class BinanceApi {
	private REST_URI = "https://api.binance.com";
	private WS_URI = "wss://stream.binance.com:9443/ws";

	private static requestQueue: any[] = [];
	private static promiseQueue: Promise<any>[] = [];
	private static exchangeInfo: any;

	private static isProcessingRequestQueue: boolean = false;

	private static ws: WsClient;

	constructor(private accessData?: BinanceAccessData) {}


	async request<
		M extends keyof BinanceRoutes,
		P extends keyof BinanceRoutes[M]
	>(method: M, path: P, ...args: BinanceRouteArgs<M, P>) {
		return new Promise<BinanceRouteResponse<M, P>>((resolve, reject) => {
			const request = {
				method,
				path,
				args,
				onSuccess: resolve,
				onFailure: reject
			};

			BinanceApi.requestQueue.push(request);

			this.processRequestQueue();
		});
	}

	wsOn(type: string, callback: (...args: any[]) => void) {
		const ws = this.getWs()
		ws.on(type, callback);
	}

	wsSend(message: any, options: WsSendOptions = {}) {
		const ws = this.getWs();
		ws.send(message, options);
	}

	private getWs() {
		if (!BinanceApi.ws) {
			BinanceApi.ws = new WsClient(this.WS_URI, {
				keepAlive: true,
				requestRateLimit: (1000 / 1) * 1.1 // 200 ms increased by 10% for sure
			});

			BinanceApi.ws.connect();
		}

		return BinanceApi.ws;
	}

	protected async doRequest<
		M extends keyof BinanceRoutes,
		P extends keyof BinanceRoutes[M]
	>(method: M, path: P, options: BinanceRouteOptions = {}): BinanceRouteResponse<M, P> {
		const r = Math.round(Math.random() * 100)
		console.log("binance request start", r, method, path, params)
		let requestPath = path.replace(/^\/+/, '');

		if (binanceRoutesConfig[method][path].isSigned) {
			const signature = this.createSignature(params);
			params['signature'] = signature;
			params['timestamp'] = Date.now();
		}

		const qs = this.paramsToQs(options.params);
		//console.log(qs);
		if (qs) {
			requestPath = `${requestPath}?${qs}`;
		}

		const headers: any = {};

		if (this.accessData?.apiKey) {
			headers['X-MBX-APIKEY'] = this.accessData.apiKey;
		}

		const response = await axios.request<BinanceRouteResponse<M, P>>({
			url: `${this.REST_URI}/${requestPath}`,
			method,
			headers,
			//data: options.payload,
			validateStatus: null
		});

		if (!options.skipRateLimit) {
			await this.limitRate(response);
		}

		if ([429, 418].includes(response.status)) {
			// rate limit exceeded -> wait requested time
			console.warn(`[${response.status}] Binance rate limit exceeded, waiting ${response.headers['retry-after']}s`);
			await sleep(response.headers['retry-after'] * 1000);
		}

		//console.log("binance request end", r)

		return response;
	}

	protected async limitRate(response: AxiosResponse<any>) {
		const exchangeInfo = await this.getExchangeInfo();

		const requestWeightLimit = exchangeInfo.rateLimits.find((l: any) => l.rateLimitType === 'REQUEST_WEIGHT')!;
		const interval = requestWeightLimit.interval.replace('DAY', 'DATE') as moment.unitOfTime.Base;
		const intervalNum = requestWeightLimit.intervalNum;

		console.log("rate limit", intervalNum, interval);

		const responseDate = moment(response.headers['date']);
		const startOfInterval = moment(responseDate)
			.startOf(interval) // reset all smaller date interval parts to 0
			.set(interval, 0)
			.add(
				Math.floor(responseDate.get(interval) / intervalNum) * intervalNum, // round to interval portion
				interval
			);
		const intervalDuration = moment.duration(responseDate.diff(startOfInterval));

		console.log("duration", intervalDuration.asSeconds());

		const allowedPerSecond = requestWeightLimit.limit / moment.duration(intervalNum, interval).asSeconds();
		console.log("allowed per sec", allowedPerSecond);

		const currentSeconds = Math.round(intervalDuration.asSeconds())
		const consumed = response.headers[`x-mbx-used-weight-${intervalNum}${interval[0]!.toLowerCase()}`];

		const expectedSeconds = consumed / allowedPerSecond;
		const waitSeconds = Math.max(expectedSeconds - currentSeconds, 0);

		console.log("consumed", consumed)
		console.log("expected", expectedSeconds)
		console.log("wait", waitSeconds)

		await sleep(waitSeconds * 1000);
	}

	async getExchangeInfo() {
		if (!BinanceApi.exchangeInfo || (Date.now() - BinanceApi.exchangeInfo.serverTime) > 60 * 1000) {
			const response = await this.doRequest('GET', '/api/v3/exchangeInfo', {}, {skipRateLimit: true});
			BinanceApi.exchangeInfo = response.data;
		}

		return BinanceApi.exchangeInfo as BinanceExchangeInfo;
	}

	protected createSignature(params: any = {}) {
		if (!this.accessData?.apiSecret) {
			throw new Error("Api key is required to create signed request to Binance");
		}

		return crypto.createHmac('sha256', this.accessData.apiSecret).update(this.paramsToQs(params)).digest('hex');
	}

	protected paramsToQs(params: any = {}) {
		return Object.entries(params)
			.filter(e => e[1] !== undefined)
			.map(e => `${e[0]}=${e[1]}`)
			.join('&');
	}

	private async getBinanceTime() {
		this.doRequest()
	}

	private async processRequestQueue() {
		if (BinanceApi.isProcessingRequestQueue || BinanceApi.requestQueue.length === 0) {
			return;
		}

		BinanceApi.isProcessingRequestQueue = true;

		const currentBinanceTime = this.getExchangeTime();
		const requestIndex = BinanceApi.requestQueue.findIndex(r => true /* TODO fit weight */)
		let waitTime = 0;

		if (requestIndex !== -1) {
			const [request] = BinanceApi.requestQueue.splice(requestIndex, 1);

			// TODO process request
		} else {
			// no request fitting with weight found, wait until the end of minute (Binance time)
			waitTime = differenceInMilliseconds(endOfMinute(currentBinanceTime), currentBinanceTime);
		}

		BinanceApi.isProcessingRequestQueue = false;
		setTimeout(() => this.processRequestQueue(), waitTime);
	}
}
