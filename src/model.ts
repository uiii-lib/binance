interface Account {
    /**
     * int64
     * @example 15
     */
    "makerCommission": number;
    /**
     * int64
     * @example 15
     */
    "takerCommission": number;
    /**
     * int64
     */
    "buyerCommission": number;
    /**
     * int64
     */
    "sellerCommission": number;
    "canTrade": boolean;
    "canWithdraw": boolean;
    "canDeposit": boolean;
    "brokered": boolean;
    /**
     * int64
     * @example 123456789
     */
    "updateTime": number;
    /**
     * @example "SPOT"
     */
    "accountType": string;
    "balances": {
        /**
         * @example "BTC"
         */
        "asset": string;
        /**
         * @example "4723846.89208129"
         */
        "free": string;
        /**
         * @example "0.00000000"
         */
        "locked": string;
    }[];
}

interface Order {
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    /**
     * @example "msXkySR3u5uYwpvRMFsi3u"
     */
    "origClientOrderId": string;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * Unless OCO, value will be -1
     * int64
     * @example -1
     */
    "orderListId": number;
    /**
     * @example "6gCrw2kRUAF9CvJDGP16IP"
     */
    "clientOrderId": string;
    /**
     * @example "1.00000000"
     */
    "price": string;
    /**
     * @example "10.00000000"
     */
    "origQty": string;
    /**
     * @example "10.00000000"
     */
    "executedQty": string;
    /**
     * @example "10.00000000"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "FILLED"
     */
    "status": string;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "LIMIT"
     */
    "type": string;
    /**
     * @example "SELL"
     */
    "side": string;
}

interface OcoOrder {
    /**
     * int64
     * @example 1929
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "ALL_DONE"
     */
    "listStatusType": string;
    /**
     * @example "ALL_DONE"
     */
    "listOrderStatus": string;
    /**
     * @example "C3wyj4WVEktd7u9aVBRXcN"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1574040868128
     */
    "transactionTime": number;
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    /**
     * @example [{"symbol":"BNBBTC","orderId":2,"clientOrderId":"pO9ufTiFGg3nw2fOdgeOXa"},{"symbol":"BNBBTC","orderId":3,"clientOrderId":"TXOvglzXuaubXAaENpaRCB"}]
     */
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
    /**
     * @example [{"symbol":"BNBBTC","origClientOrderId":"pO9ufTiFGg3nw2fOdgeOXa","orderId":2,"orderListId":0,"clientOrderId":"unfWT8ig8i0uj6lPuYLez6","price":"1.00000000","origQty":"10.00000000","executedQty":"0.00000000","cummulativeQuoteQty":"0.00000000","status":"CANCELED","timeInForce":"GTC","type":"STOP_LOSS_LIMIT","side":"SELL","stopPrice":"1.00000000"},{"symbol":"BNBBTC","origClientOrderId":"TXOvglzXuaubXAaENpaRCB","orderId":3,"orderListId":0,"clientOrderId":"unfWT8ig8i0uj6lPuYLez6","price":"3.00000000","origQty":"10.00000000","executedQty":"0.00000000","cummulativeQuoteQty":"0.00000000","status":"CANCELED","timeInForce":"GTC","type":"LIMIT_MAKER","side":"SELL"}]
     */
    "orderReports": {
        "symbol": string;
        "origClientOrderId": string;
        /**
         * int64
         */
        "orderId": number;
        /**
         * int64
         */
        "orderListId": number;
        "clientOrderId": string;
        "price": string;
        "origQty": string;
        "executedQty": string;
        "cummulativeQuoteQty": string;
        "status": string;
        "timeInForce": string;
        "type": string;
        "side": string;
        "stopPrice": string;
    }[];
}

interface MarginOcoOrder {
    /**
     * int64
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "ALL_DONE"
     */
    "listStatusType": string;
    /**
     * @example "ALL_DONE"
     */
    "listOrderStatus": string;
    /**
     * @example "C3wyj4WVEktd7u9aVBRXcN"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1574040868128
     */
    "transactionTime": number;
    /**
     * @example "BNBUSDT"
     */
    "symbol": string;
    "isIsolated": boolean;
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
    "orderReports": {
        "symbol": string;
        "origClientOrderId": string;
        /**
         * int64
         */
        "orderId": number;
        /**
         * int64
         */
        "orderListId": number;
        "clientOrderId": string;
        "price": string;
        "origQty": string;
        "executedQty": string;
        "cummulativeQuoteQty": string;
        "status": string;
        "timeInForce": string;
        "type": string;
        "side": string;
        "stopPrice": string;
    }[];
}

interface OrderDetails {
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    /**
     * int64
     * @example 1
     */
    "orderId": number;
    /**
     * Unless OCO, value will be -1
     * int64
     * @example -1
     */
    "orderListId": number;
    /**
     * @example "myOrder1"
     */
    "clientOrderId": string;
    /**
     * @example "0.1"
     */
    "price": string;
    /**
     * @example "1.0"
     */
    "origQty": string;
    /**
     * @example "0.0"
     */
    "executedQty": string;
    /**
     * @example "0.0"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "NEW"
     */
    "status": string;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "LIMIT"
     */
    "type": string;
    /**
     * @example "BUY"
     */
    "side": string;
    /**
     * @example "0.0"
     */
    "stopPrice": string;
    /**
     * @example "0.0"
     */
    "icebergQty": string;
    /**
     * int64
     * @example 1499827319559
     */
    "time": number;
    /**
     * int64
     * @example 1499827319559
     */
    "updateTime": number;
    "isWorking": boolean;
    /**
     * @example "0.00000000"
     */
    "origQuoteOrderQty": string;
}

interface OrderResponseAck {
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * int64
     * @example -1
     */
    "orderListId": number;
    /**
     * @example "6gCrw2kRUAF9CvJDGP16IP"
     */
    "clientOrderId": string;
    /**
     * int64
     * @example 1507725176595
     */
    "transactTime": number;
}

interface OrderResponseResult {
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * int64
     * @example -1
     */
    "orderListId": number;
    /**
     * @example "6gCrw2kRUAF9CvJDGP16IP"
     */
    "clientOrderId": string;
    /**
     * int64
     * @example 1507725176595
     */
    "transactTime": number;
    /**
     * @example "0.00000000"
     */
    "price": string;
    /**
     * @example "10.00000000"
     */
    "origQty": string;
    /**
     * @example "10.00000000"
     */
    "executedQty": string;
    /**
     * @example "10.00000000"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "FILLED"
     */
    "status": string;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "MARKET"
     */
    "type": string;
    /**
     * @example "SELL"
     */
    "side": string;
    /**
     * int64
     * @example 1
     */
    "strategyId"?: number;
    /**
     * int64
     * @example 1000000
     */
    "strategyType"?: number;
}

interface OrderResponseFull {
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * int64
     * @example -1
     */
    "orderListId": number;
    /**
     * @example "6gCrw2kRUAF9CvJDGP16IP"
     */
    "clientOrderId": string;
    /**
     * int64
     * @example 1507725176595
     */
    "transactTime": number;
    /**
     * @example "0.00000000"
     */
    "price": string;
    /**
     * @example "10.00000000"
     */
    "origQty": string;
    /**
     * @example "10.00000000"
     */
    "executedQty": string;
    /**
     * @example "10.00000000"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "FILLED"
     */
    "status": string;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "MARKET"
     */
    "type": string;
    /**
     * @example "SELL"
     */
    "side": string;
    /**
     * int64
     * @example 1
     */
    "strategyId"?: number;
    /**
     * int64
     * @example 1000000
     */
    "strategyType"?: number;
    "fills": {
        /**
         * @example "4000.00000000"
         */
        "price": string;
        /**
         * @example "1.00000000"
         */
        "qty": string;
        /**
         * @example "4.00000000"
         */
        "commission": string;
        /**
         * @example "USDT"
         */
        "commissionAsset": string;
    }[];
}

interface MarginOrder {
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * @example "msXkySR3u5uYwpvRMFsi3u"
     */
    "origClientOrderId": string;
    /**
     * @example "6gCrw2kRUAF9CvJDGP16IP"
     */
    "clientOrderId": string;
    /**
     * @example "1.00000000"
     */
    "price": string;
    /**
     * @example "10.00000000"
     */
    "origQty": string;
    /**
     * @example "8.00000000"
     */
    "executedQty": string;
    /**
     * @example "8.00000000"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "CANCELED"
     */
    "status": string;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "LIMIT"
     */
    "type": string;
    /**
     * @example "SELL"
     */
    "side": string;
}

interface MarginOrderDetail {
    /**
     * @example "ZwfQzuDIGpceVhKW5DvCmO"
     */
    "clientOrderId": string;
    /**
     * @example "0.00000000"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "0.00000000"
     */
    "executedQty": string;
    /**
     * @example "0.00000000"
     */
    "icebergQty": string;
    "isWorking": boolean;
    /**
     * int64
     * @example 213205622
     */
    "orderId": number;
    /**
     * @example "0.30000000"
     */
    "origQty": string;
    /**
     * @example "0.00493630"
     */
    "price": string;
    /**
     * @example "SELL"
     */
    "side": string;
    /**
     * @example "NEW"
     */
    "status": string;
    /**
     * @example "0.00000000"
     */
    "stopPrice": string;
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    "isIsolated": boolean;
    /**
     * int64
     * @example 1562133008725
     */
    "time": number;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "LIMIT"
     */
    "type": string;
    /**
     * int64
     * @example 1562133008725
     */
    "updateTime": number;
}

interface CanceledMarginOrderDetail {
    /**
     * @example "BNBUSDT"
     */
    "symbol": string;
    "isIsolated": boolean;
    /**
     * @example "E6APeyTJvkMvLMYMqu1KQ4"
     */
    "origClientOrderId": string;
    /**
     * int64
     * @example 11
     */
    "orderId": number;
    /**
     * int64
     * @example -1
     */
    "orderListId": number;
    /**
     * @example "pXLV6Hz6mprAcVYpVMTGgx"
     */
    "clientOrderId": string;
    /**
     * @example "0.089853"
     */
    "price": string;
    /**
     * @example "0.178622"
     */
    "origQty": string;
    /**
     * @example "0.000000"
     */
    "executedQty": string;
    /**
     * @example "0.000000"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "CANCELED"
     */
    "status": string;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "LIMIT"
     */
    "type": string;
    /**
     * @example "BUY"
     */
    "side": string;
}

interface MarginOrderResponseAck {
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * @example "6gCrw2kRUAF9CvJDGP16IP"
     */
    "clientOrderId": string;
    "isIsolated": boolean;
    /**
     * int64
     * @example 1507725176595
     */
    "transactTime": number;
}

interface MarginOrderResponseResult {
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * @example "6gCrw2kRUAF9CvJDGP16IP"
     */
    "clientOrderId": string;
    /**
     * int64
     * @example 1507725176595
     */
    "transactTime": number;
    /**
     * @example "1.00000000"
     */
    "price": string;
    /**
     * @example "10.00000000"
     */
    "origQty": string;
    /**
     * @example "10.00000000"
     */
    "executedQty": string;
    /**
     * @example "10.00000000"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "FILLED"
     */
    "status": string;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "MARKET"
     */
    "type": string;
    "isIsolated": boolean;
    /**
     * @example "SELL"
     */
    "side": string;
}

interface MarginOrderResponseFull {
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * @example "6gCrw2kRUAF9CvJDGP16IP"
     */
    "clientOrderId": string;
    /**
     * int64
     * @example 1507725176595
     */
    "transactTime": number;
    /**
     * @example "1.00000000"
     */
    "price": string;
    /**
     * @example "10.00000000"
     */
    "origQty": string;
    /**
     * @example "10.00000000"
     */
    "executedQty": string;
    /**
     * @example "10.00000000"
     */
    "cummulativeQuoteQty": string;
    /**
     * @example "FILLED"
     */
    "status": string;
    /**
     * @example "GTC"
     */
    "timeInForce": string;
    /**
     * @example "MARKET"
     */
    "type": string;
    /**
     * @example "SELL"
     */
    "side": string;
    /**
     * will not return if no margin trade happens
     * double
     * @example 5
     */
    "marginBuyBorrowAmount": number;
    /**
     * will not return if no margin trade happens
     * @example "BTC"
     */
    "marginBuyBorrowAsset": string;
    "isIsolated": boolean;
    "fills": {
        /**
         * @example "4000.00000000"
         */
        "price": string;
        /**
         * @example "1.00000000"
         */
        "qty": string;
        /**
         * @example "4.00000000"
         */
        "commission": string;
        /**
         * @example "USDT"
         */
        "commissionAsset": string;
    }[];
}

interface MarginTrade {
    /**
     * @example "0.00006000"
     */
    "commission": string;
    /**
     * @example "BTC"
     */
    "commissionAsset": string;
    /**
     * int64
     * @example 28
     */
    "id": number;
    "isBestMatch": boolean;
    "isBuyer": boolean;
    "isMaker": boolean;
    /**
     * int64
     * @example 28
     */
    "orderId": number;
    /**
     * @example "0.02000000"
     */
    "price": string;
    /**
     * @example "1.02000000"
     */
    "qty": string;
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    "isIsolated": boolean;
    /**
     * int64
     * @example 1507725176595
     */
    "time": number;
}

interface MarginTransferDetails {
    "rows": {
        /**
         * @example "0.10000000"
         */
        "amount": string;
        /**
         * @example "BNB"
         */
        "asset": string;
        /**
         * @example "CONFIRMED"
         */
        "status": string;
        /**
         * int64
         * @example 1566898617000
         */
        "timestamp": number;
        /**
         * int64
         * @example 5240372201
         */
        "txId": number;
        /**
         * @example "SPOT"
         */
        "transFrom": string;
        /**
         * @example "ISOLATED_MARGIN"
         */
        "transTo": string;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface IsolatedMarginAccountInfo {
    "assets": {
        "baseAsset": {
            /**
             * @example "BTC"
             */
            "asset": string;
            "borrowEnabled": boolean;
            /**
             * @example "0.00000000"
             */
            "borrowed": string;
            /**
             * @example "0.00000000"
             */
            "free": string;
            /**
             * @example "0.00000000"
             */
            "interest": string;
            /**
             * @example "0.00000000"
             */
            "locked": string;
            /**
             * @example "0.00000000"
             */
            "netAsset": string;
            /**
             * @example "0.00000000"
             */
            "netAssetOfBtc": string;
            "repayEnabled": boolean;
            /**
             * @example "0.00000000"
             */
            "totalAsset": string;
        };
        "quoteAsset": {
            /**
             * @example "USDT"
             */
            "asset": string;
            "borrowEnabled": boolean;
            /**
             * @example "0.00000000"
             */
            "borrowed": string;
            /**
             * @example "0.00000000"
             */
            "free": string;
            /**
             * @example "0.00000000"
             */
            "interest": string;
            /**
             * @example "0.00000000"
             */
            "locked": string;
            /**
             * @example "0.00000000"
             */
            "netAsset": string;
            /**
             * @example "0.00000000"
             */
            "netAssetOfBtc": string;
            "repayEnabled": boolean;
            /**
             * @example "0.00000000"
             */
            "totalAsset": string;
        };
        /**
         * @example "BTCUSDT"
         */
        "symbol": string;
        "isolatedCreated": boolean;
        /**
         * true-enabled, false-disabled
         */
        "enabled": boolean;
        /**
         * @example "0.00000000"
         */
        "marginLevel": string;
        /**
         * "EXCESSIVE", "NORMAL", "MARGIN_CALL", "PRE_LIQUIDATION", "FORCE_LIQUIDATION"
         * @example "EXCESSIVE"
         */
        "marginLevelStatus": string;
        /**
         * @example "0.00000000"
         */
        "marginRatio": string;
        /**
         * @example "10000.00000000"
         */
        "indexPrice": string;
        /**
         * @example "1000.00000000"
         */
        "liquidatePrice": string;
        /**
         * @example "1.00000000"
         */
        "liquidateRate": string;
        "tradeEnabled": boolean;
    }[];
    /**
     * @example "0.00000000"
     */
    "totalAssetOfBtc": string;
    /**
     * @example "0.00000000"
     */
    "totalLiabilityOfBtc": string;
    /**
     * @example "0.00000000"
     */
    "totalNetAssetOfBtc": string;
}

type BookTickerList = BookTicker[];

interface BookTicker {
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    /**
     * @example "16.36240000"
     */
    "bidPrice": string;
    /**
     * @example "256.78000000"
     */
    "bidQty": string;
    /**
     * @example "16.36450000"
     */
    "askPrice": string;
    /**
     * @example "12.56000000"
     */
    "askQty": string;
}

type PriceTickerList = PriceTicker[];

interface PriceTicker {
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    /**
     * @example "0.17160000"
     */
    "price": string;
}

interface RepaymentInfo {
    /**
     * @example "BUSD"
     */
    "loanCoin": string;
    /**
     * @example "100.5"
     */
    "remainingPrincipal": string;
    /**
     * @example "0"
     */
    "remainingInterest": string;
    /**
     * @example "BNB"
     */
    "collateralCoin": string;
    /**
     * @example "5.253"
     */
    "remainingCollateral": string;
    /**
     * @example "0.25"
     */
    "currentLTV": string;
    /**
     * @example "Repaying"
     */
    "repayStatus": string;
}

interface RepaymentInfo2 {
    /**
     * @example "BUSD"
     */
    "loanCoin": string;
    /**
     * @example "BNB"
     */
    "collateralCoin": string;
    /**
     * @example "Repaying"
     */
    "repayStatus": string;
}

type TickerList = Ticker[];

interface Ticker {
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    /**
     * @example "0.17160000"
     */
    "priceChange": string;
    /**
     * @example "1.060"
     */
    "priceChangePercent": string;
    /**
     * @example "16.35920000"
     */
    "prevClosePrice": string;
    /**
     * @example "27.84000000"
     */
    "lastPrice": string;
    /**
     * @example "16.34488284"
     */
    "bidPrice": string;
    /**
     * @example "16.34488284"
     */
    "bidQty": string;
    /**
     * @example "16.35920000"
     */
    "askPrice": string;
    /**
     * @example "25.06000000"
     */
    "askQty": string;
    /**
     * @example "16.18760000"
     */
    "openPrice": string;
    /**
     * @example "16.55000000"
     */
    "highPrice": string;
    /**
     * @example "16.16940000"
     */
    "lowPrice": string;
    /**
     * @example "1678279.95000000"
     */
    "volume": string;
    /**
     * @example "27431289.14792300"
     */
    "quoteVolume": string;
    /**
     * int64
     * @example 1592808788637
     */
    "openTime": number;
    /**
     * int64
     * @example 1592895188637
     */
    "closeTime": number;
    /**
     * int64
     * @example 62683296
     */
    "firstId": number;
    /**
     * int64
     * @example 62739253
     */
    "lastId": number;
    /**
     * int64
     * @example 55958
     */
    "count": number;
}

interface MyTrade {
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    /**
     * Trade id
     * int64
     * @example 28457
     */
    "id": number;
    /**
     * int64
     * @example 100234
     */
    "orderId": number;
    /**
     * int64
     * @example -1
     */
    "orderListId": number;
    /**
     * Price
     * @example "4.00000100"
     */
    "price": string;
    /**
     * Amount of base asset
     * @example "12.00000000"
     */
    "qty": string;
    /**
     * Amount of quote asset
     * @example "48.000012"
     */
    "quoteQty": string;
    /**
     * @example "10.10000000"
     */
    "commission": string;
    /**
     * @example "BNB"
     */
    "commissionAsset": string;
    /**
     * Trade timestamp
     * int64
     * @example 1499865549590
     */
    "time": number;
    "isBuyer": boolean;
    "isMaker": boolean;
    "isBestMatch": boolean;
}

interface Transaction {
    /**
     * transaction id
     * int64
     * @example 345196462
     */
    "tranId": number;
}

interface Trade {
    /**
     * trade id
     * int64
     * @example 345196462
     */
    "id": number;
    /**
     * price
     * @example "9638.99000000"
     */
    "price": string;
    /**
     * amount of base asset
     * @example "0.02077200"
     */
    "qty": string;
    /**
     * amount of quote asset
     * @example "0.02077200"
     */
    "quoteQty": string;
    /**
     * Trade executed timestamp, as same as `T` in the stream
     * int64
     * @example 1592887772684
     */
    "time": number;
    "isBuyerMaker": boolean;
    "isBestMatch": boolean;
}

interface AggTrade {
    /**
     * Aggregate tradeId
     * int64
     * @example 26129
     */
    "a": number;
    /**
     * Price
     * @example "0.01633102"
     */
    "p": string;
    /**
     * Quantity
     * @example "4.70443515"
     */
    "q": string;
    /**
     * First tradeId
     * int64
     * @example 27781
     */
    "f": number;
    /**
     * Last tradeId
     * int64
     * @example 27781
     */
    "l": number;
    /**
     * Timestamp
     * @example 1498793709153
     */
    "T": boolean;
    /**
     * Was the buyer the maker?
     */
    "m": boolean;
    /**
     * Was the trade the best price match?
     */
    "M": boolean;
}

interface BnbBurnStatus {
    "spotBNBBurn": boolean;
    "interestBNBBurn": boolean;
}

interface SnapshotSpot {
    /**
     * int64
     * @example 200
     */
    "code": number;
    "msg": string;
    "snapshotVos": {
        "data": {
            "balances": {
                /**
                 * @example "BTC"
                 */
                "asset": string;
                /**
                 * @example "0.2"
                 */
                "free": string;
                /**
                 * @example "0.001"
                 */
                "locked": string;
            }[];
            /**
             * @example "0.09905021"
             */
            "totalAssetOfBtc": string;
        };
        /**
         * @example "spot"
         */
        "type": string;
        /**
         * int64
         * @example 1576281599000
         */
        "updateTime": number;
    }[];
}

interface SnapshotMargin {
    /**
     * int64
     * @example 200
     */
    "code": number;
    "msg": string;
    "snapshotVos": {
        "data": {
            /**
             * @example "2748.02909813"
             */
            "marginLevel": string;
            /**
             * @example "0.00274803"
             */
            "totalAssetOfBtc": string;
            /**
             * @example "0.00000100"
             */
            "totalLiabilityOfBtc": string;
            /**
             * @example "0.00274750"
             */
            "totalNetAssetOfBtc": string;
            "userAssets": {
                /**
                 * @example "XRP"
                 */
                "asset": string;
                /**
                 * @example "0.00000000"
                 */
                "borrowed": string;
                /**
                 * @example "1.00000000"
                 */
                "free": string;
                /**
                 * @example "0.00000000"
                 */
                "interest": string;
                /**
                 * @example "0.00000000"
                 */
                "locked": string;
                /**
                 * @example "1.00000000"
                 */
                "netAsset": string;
            }[];
        };
        /**
         * @example "margin"
         */
        "type": string;
        /**
         * int64
         * @example 1576281599000
         */
        "updateTime": number;
    }[];
}

interface SnapshotFutures {
    /**
     * int64
     * @example 200
     */
    "code": number;
    "msg": string;
    "snapshotVos": {
        "data": {
            "assets": {
                /**
                 * @example "USDT"
                 */
                "asset": string;
                /**
                 * @example "118.99782335"
                 */
                "marginBalance": string;
                /**
                 * @example "120.23811389"
                 */
                "walletBalance": string;
            }[];
            "position": {
                /**
                 * @example "7130.41000000"
                 */
                "entryPrice": string;
                /**
                 * @example "7257.66239673"
                 */
                "markPrice": string;
                /**
                 * @example "0.01000000"
                 */
                "positionAmt": string;
                /**
                 * @example "BTCUSDT"
                 */
                "symbol": string;
                /**
                 * @example "1.24029054"
                 */
                "unRealizedProfit": string;
            }[];
        };
        /**
         * @example "futures"
         */
        "type": string;
        /**
         * int64
         * @example 1576281599000
         */
        "updateTime": number;
    }[];
}

interface SubAccountUSDTFuturesDetails {
    "futureAccountResp": {
        /**
         * @example "abc@test.com"
         */
        "email": string;
        "assets": {
            /**
             * @example "USDT"
             */
            "asset": string;
            /**
             * @example "0.00000000"
             */
            "initialMargin": string;
            /**
             * @example "0.00000000"
             */
            "maintenanceMargin": string;
            /**
             * @example "0.88308000"
             */
            "marginBalance": string;
            /**
             * @example "0.88308000"
             */
            "maxWithdrawAmount": string;
            /**
             * @example "0.00000000"
             */
            "openOrderInitialMargin": string;
            /**
             * @example "0.00000000"
             */
            "positionInitialMargin": string;
            /**
             * @example "0.00000000"
             */
            "unrealizedProfit": string;
            /**
             * @example "0.88308000"
             */
            "walletBalance": string;
        }[];
        "canDeposit": boolean;
        "canTrade": boolean;
        "canWithdraw": boolean;
        /**
         * int64
         * @example 2
         */
        "feeTier": number;
        /**
         * @example "0.88308000"
         */
        "maxWithdrawAmount": string;
        /**
         * @example "0.00000000"
         */
        "totalInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "totalMaintenanceMargin": string;
        /**
         * @example "0.88308000"
         */
        "totalMarginBalance": string;
        /**
         * @example "0.00000000"
         */
        "totalOpenOrderInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "totalPositionInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "totalUnrealizedProfit": string;
        /**
         * @example "0.88308000"
         */
        "totalWalletBalance": string;
        /**
         * int64
         * @example 1576756674610
         */
        "updateTime": number;
    };
}

interface SubAccountCOINFuturesDetails {
    /**
     * @example "abc@test.com"
     */
    "email": string;
    "assets": {
        /**
         * @example "BTC"
         */
        "asset": string;
        /**
         * @example "0.00000000"
         */
        "initialMargin": string;
        /**
         * @example "0.00000000"
         */
        "maintenanceMargin": string;
        /**
         * @example "0.88308000"
         */
        "marginBalance": string;
        /**
         * @example "0.88308000"
         */
        "maxWithdrawAmount": string;
        /**
         * @example "0.00000000"
         */
        "openOrderInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "positionInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "unrealizedProfit": string;
        /**
         * @example "0.88308000"
         */
        "walletBalance": string;
    }[];
    "canDeposit": boolean;
    "canTrade": boolean;
    "canWithdraw": boolean;
    /**
     * int64
     * @example 2
     */
    "feeTier": number;
    /**
     * int64
     * @example 1598959682001
     */
    "updateTime": number;
}

interface SubAccountUSDTFuturesSummary {
    "futureAccountSummaryResp": {
        /**
         * @example "9.83137400"
         */
        "totalInitialMargin": string;
        /**
         * @example "0.41568700"
         */
        "totalMaintenanceMargin": string;
        /**
         * @example "23.03235621"
         */
        "totalMarginBalance": string;
        /**
         * @example "9.00000000"
         */
        "totalOpenOrderInitialMargin": string;
        /**
         * @example "0.83137400"
         */
        "totalPositionInitialMargin": string;
        /**
         * @example "0.03219710"
         */
        "totalUnrealizedProfit": string;
        /**
         * @example "22.15879444"
         */
        "totalWalletBalance": string;
        /**
         * The sum of BUSD and USDT
         * @example "USD"
         */
        "asset": string;
        "subAccountList": {
            /**
             * @example "123@test.com"
             */
            "email": string;
            /**
             * @example "9.00000000"
             */
            "totalInitialMargin": string;
            /**
             * @example "0.00000000"
             */
            "totalMaintenanceMargin": string;
            /**
             * @example "22.12659734"
             */
            "totalMarginBalance": string;
            /**
             * @example "9.00000000"
             */
            "totalOpenOrderInitialMargin": string;
            /**
             * @example "0.00000000"
             */
            "totalPositionInitialMargin": string;
            /**
             * @example "0.00000000"
             */
            "totalUnrealizedProfit": string;
            /**
             * @example "22.12659734"
             */
            "totalWalletBalance": string;
            /**
             * The sum of BUSD and USDT
             * @example "USD"
             */
            "asset": string;
        }[];
    };
}

interface SubAccountCOINFuturesSummary {
    "deliveryAccountSummaryResp": {
        /**
         * @example 25.03221121
         */
        "totalMarginBalanceOfBTC": string;
        /**
         * @example 0.1223341
         */
        "totalUnrealizedProfitOfBTC": string;
        /**
         * @example 22.15879444
         */
        "totalWalletBalanceOfBTC": string;
        /**
         * @example "BTC"
         */
        "asset": string;
        "subAccountList": {
            /**
             * @example "123@test.com"
             */
            "email": string;
            /**
             * @example 22.12659734
             */
            "totalMarginBalance": string;
            "totalUnrealizedProfit": string;
            /**
             * @example 22.12659734
             */
            "totalWalletBalance": string;
            /**
             * @example "BTC"
             */
            "asset": string;
        }[];
    };
}

interface SubAccountUSDTFuturesPositionRisk {
    "futurePositionRiskVos": {
        /**
         * @example "9975.12000"
         */
        "entryPrice": string;
        /**
         * current initial leverage
         * @example "50"
         */
        "leverage": string;
        /**
         * notional value limit of current initial leverage
         * @example "1000000"
         */
        "maxNotional": string;
        /**
         * @example "7963.54"
         */
        "liquidationPrice": string;
        /**
         * @example "9973.50770517"
         */
        "markPrice": string;
        /**
         * @example "0.010"
         */
        "positionAmount": string;
        /**
         * @example "BTCUSDT"
         */
        "symbol": string;
        /**
         * @example "-0.01612295"
         */
        "unrealizedProfit": string;
    }[];
}

interface SubAccountCOINFuturesPositionRisk {
    "deliveryPositionRiskVos": {
        /**
         * @example "9975.12000"
         */
        "entryPrice": string;
        /**
         * @example "9973.50770517"
         */
        "markPrice": string;
        /**
         * @example "20"
         */
        "leverage": string;
        "isolated": string;
        /**
         * @example "9973.50770517"
         */
        "isolatedWallet": string;
        /**
         * @example "0.00000000"
         */
        "isolatedMargin": string;
        /**
         * @example "false"
         */
        "isAutoAddMargin": string;
        /**
         * @example "BOTH"
         */
        "positionSide": string;
        /**
         * @example "1.230"
         */
        "positionAmount": string;
        /**
         * @example "BTCUSD_201225"
         */
        "symbol": string;
        /**
         * @example "-0.01612295"
         */
        "unrealizedProfit": string;
    }[];
}

type SavingsFlexiblePurchaseRecord = {
    /**
     * @example "100.00000000"
     */
    "amount": string;
    /**
     * @example "USDT"
     */
    "asset": string;
    /**
     * int64
     * @example 1575018510000
     */
    "createTime": number;
    /**
     * @example "DAILY"
     */
    "lendingType": string;
    /**
     * @example "USDT"
     */
    "productName": string;
    /**
     * int64
     * @example 26055
     */
    "purchaseId": number;
    /**
     * @example "SUCCESS"
     */
    "status": string;
}[];

type SavingsFixedActivityPurchaseRecord = {
    /**
     * @example "100.00000000"
     */
    "amount": string;
    /**
     * @example "USDT"
     */
    "asset": string;
    /**
     * int64
     * @example 1575018453000
     */
    "createTime": number;
    /**
     * @example "ACTIVITY"
     */
    "lendingType": string;
    /**
     * int64
     * @example 1
     */
    "lot": number;
    /**
     * @example "【Special】USDT 7D (8%)"
     */
    "productName": string;
    /**
     * int64
     * @example 36857
     */
    "purchaseId": number;
    /**
     * @example "SUCCESS"
     */
    "status": string;
}[];

type SavingsFlexibleRedemptionRecord = {
    /**
     * @example "10.54000000"
     */
    "amount": string;
    /**
     * @example "USDT"
     */
    "asset": string;
    /**
     * int64
     * @example 1577257222000
     */
    "createTime": number;
    /**
     * @example "10.54000000"
     */
    "principal": string;
    /**
     * @example "USDT001"
     */
    "projectId": string;
    /**
     * @example "USDT"
     */
    "projectName": string;
    /**
     * @example "PAID"
     */
    "status": string;
    /**
     * @example "FAST"
     */
    "type": string;
}[];

type SavingsFixedActivityRedemptionRecord = {
    /**
     * @example "0.07070000"
     */
    "amount": string;
    /**
     * @example "USDT"
     */
    "asset": string;
    /**
     * int64
     * @example 1566200161000
     */
    "createTime": number;
    /**
     * @example "0.00070000"
     */
    "interest": string;
    /**
     * @example "0.07000000"
     */
    "principal": string;
    /**
     * @example "test06"
     */
    "projectId": string;
    /**
     * @example "USDT 1 day (10% annualized)"
     */
    "projectName": string;
    /**
     * int64
     * @example 1566198000000
     */
    "startTime": number;
    /**
     * @example "PAID"
     */
    "status": string;
}[];

interface BswapAddLiquidityPreviewCombination {
    /**
     * @example "USDT"
     */
    "quoteAsset": string;
    /**
     * @example "BUSD"
     */
    "baseAsset": string;
    /**
     * int64
     * @example 300000
     */
    "quoteAmt": number;
    /**
     * int64
     * @example 299975
     */
    "baseAmt": number;
    /**
     * double
     * @example 1.00008334
     */
    "price": number;
    /**
     * double
     * @example 1.23
     */
    "share": number;
}

interface BswapAddLiquidityPreviewSingle {
    /**
     * @example "USDT"
     */
    "quoteAsset": string;
    /**
     * int64
     * @example 300000
     */
    "quoteAmt": number;
    /**
     * double
     * @example 1.00008334
     */
    "price": number;
    /**
     * double
     * @example 1.23
     */
    "share": number;
    /**
     * double
     * @example 0.00007245
     */
    "slippage": number;
    /**
     * double
     * @example 120
     */
    "fee": number;
}

interface BswapRmvLiquidityPreviewCombination {
    /**
     * @example "USDT"
     */
    "quoteAsset": string;
    /**
     * @example "BUSD"
     */
    "baseAsset": string;
    /**
     * int64
     * @example 300000
     */
    "quoteAmt": number;
    /**
     * int64
     * @example 299975
     */
    "baseAmt": number;
    /**
     * double
     * @example 1.00008334
     */
    "price": number;
}

interface BswapRmvLiquidityPreviewSingle {
    /**
     * @example "USDT"
     */
    "quoteAsset": string;
    /**
     * int64
     * @example 300000
     */
    "quoteAmt": number;
    /**
     * double
     * @example 1.00008334
     */
    "price": number;
    /**
     * double
     * @example 0.00007245
     */
    "slippage": number;
    /**
     * double
     * @example 120
     */
    "fee": number;
}

interface Error {
    /**
     * Error code
     * int64
     */
    "code": number;
    /**
     * Error message
     * @example "error message"
     */
    "msg": string;
}

interface GetApiV3PingResponse {
}

interface GetApiV3TimeResponse {
    /**
     * int64
     * @example 1499827319559
     */
    "serverTime": number;
}

interface GetApiV3ExchangeInfoParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * @example "[\"BTCUSDT\",\"BNBBTC\"]"
     */
    symbols?: string;
    /**
     * @example "'SPOT' or ['MARGIN','LEVERAGED']"
     */
    permissions?: string;
}

interface GetApiV3ExchangeInfoResponse {
    /**
     * @example "UTC"
     */
    "timezone": string;
    /**
     * int64
     * @example 1592882214236
     */
    "serverTime": number;
    "rateLimits": {
        /**
         * @example "REQUEST_WEIGHT"
         */
        "rateLimitType": string;
        /**
         * @example "MINUTE"
         */
        "interval": string;
        /**
         * int32
         * @example 1
         */
        "intervalNum": number;
        /**
         * int32
         * @example 1200
         */
        "limit": number;
    }[];
    "exchangeFilters": {}[];
    "symbols": {
        /**
         * @example "ETHBTC"
         */
        "symbol": string;
        /**
         * @example "TRADING"
         */
        "status": string;
        /**
         * @example "ETH"
         */
        "baseAsset": string;
        /**
         * int32
         * @example 8
         */
        "baseAssetPrecision": number;
        /**
         * @example "BTC"
         */
        "quoteAsset": string;
        /**
         * int32
         * @example 8
         */
        "quoteAssetPrecision": number;
        /**
         * int32
         * @example 8
         */
        "baseCommissionPrecision": number;
        /**
         * int32
         * @example 8
         */
        "quoteCommissionPrecision": number;
        "orderTypes": string[];
        "icebergAllowed": boolean;
        "ocoAllowed": boolean;
        "quoteOrderQtyMarketAllowed": boolean;
        "allowTrailingStop": boolean;
        "isSpotTradingAllowed": boolean;
        "isMarginTradingAllowed": boolean;
        "filters": {
            /**
             * @example "PRICE_FILTER"
             */
            "filterType": string;
            /**
             * @example "0.00000100"
             */
            "minPrice": string;
            /**
             * @example "100000.00000000"
             */
            "maxPrice": string;
            /**
             * @example "0.00000100"
             */
            "tickSize": string;
        }[];
        "permissions": string[];
    }[];
}

interface GetApiV3DepthParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * If limit > 5000, then the response will truncate to 5000
     * int32
     * @example 100
     */
    limit?: number;
}

interface GetApiV3DepthResponse {
    /**
     * int64
     */
    "lastUpdateId": number;
    "bids": string[][];
    "asks": string[][];
}

interface GetApiV3TradesParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
}

interface GetApiV3HistoricalTradesParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * Trade id to fetch from. Default gets most recent trades.
     * int64
     */
    fromId?: number;
}

interface GetApiV3AggTradesParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * Trade id to fetch from. Default gets most recent trades.
     * int64
     */
    fromId?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
}

interface GetApiV3KlinesParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * kline intervals
     */
    interval: "1s" | "1m" | "3m" | "5m" | "15m" | "30m" | "1h" | "2h" | "4h" | "6h" | "8h" | "12h" | "1d" | "3d" | "1w" | "1M";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
}

interface GetApiV3UiKlinesParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * kline intervals
     */
    interval: "1s" | "1m" | "3m" | "5m" | "15m" | "30m" | "1h" | "2h" | "4h" | "6h" | "8h" | "12h" | "1d" | "3d" | "1w" | "1M";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
}

interface GetApiV3AvgPriceParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
}

interface GetApiV3AvgPriceResponse {
    /**
     * int64
     * @example 5
     */
    "mins": number;
    /**
     * @example "9.35751834"
     */
    "price": string;
}

interface GetApiV3Ticker24HrParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * @example "[\"BTCUSDT\",\"BNBBTC\"]"
     */
    symbols?: string;
    /**
     * Supported values: FULL or MINI.
     * If none provided, the default is FULL
     */
    type?: string;
}

interface GetApiV3TickerPriceParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * @example "[\"BTCUSDT\",\"BNBBTC\"]"
     */
    symbols?: string;
}

interface GetApiV3TickerBookTickerParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * @example "[\"BTCUSDT\",\"BNBBTC\"]"
     */
    symbols?: string;
}

interface GetApiV3TickerParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * @example "[\"BTCUSDT\",\"BNBBTC\"]"
     */
    symbols?: string;
    /**
     * Defaults to 1d if no parameter provided.
     * Supported windowSize values:
     * 1m,2m....59m for minutes
     * 1h, 2h....23h - for hours
     * 1d...7d - for days.
     *
     * Units cannot be combined (e.g. 1d2h is not allowed)
     */
    windowSize?: string;
    /**
     * Supported values: FULL or MINI.
     * If none provided, the default is FULL
     */
    type?: string;
}

interface GetApiV3TickerResponse {
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
    /**
     * @example "-8.00000000"
     */
    "priceChange": string;
    /**
     * @example "-88.889"
     */
    "priceChangePercent": string;
    /**
     * @example "2.60427807"
     */
    "weightedAvgPrice": string;
    /**
     * @example "9.00000000"
     */
    "openPrice": string;
    /**
     * @example "9.00000000"
     */
    "highPrice": string;
    /**
     * @example "1.00000000"
     */
    "lowPrice": string;
    /**
     * @example "1.00000000"
     */
    "lastPrice": string;
    /**
     * @example "187.00000000"
     */
    "volume": string;
    /**
     * @example "487.00000000"
     */
    "quoteVolume": string;
    /**
     * int64
     * @example 1641859200000
     */
    "openTime": number;
    /**
     * int64
     * @example 1642031999999
     */
    "closeTime": number;
    /**
     * int64
     */
    "firstId": number;
    /**
     * int64
     * @example 60
     */
    "lastId": number;
    /**
     * int64
     * @example 61
     */
    "count": number;
}

interface GetApiV3OrderParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * Order id
     * int64
     */
    orderId?: number;
    /**
     * Order id from client
     */
    origClientOrderId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3OpenOrdersParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3AllOrdersParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * Order id
     * int64
     */
    orderId?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3OrderListParams {
    /**
     * Order list id
     * int64
     */
    orderListId?: number;
    /**
     * Order id from client
     */
    origClientOrderId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3OrderListResponse {
    /**
     * int64
     * @example 27
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "EXEC_STARTED"
     */
    "listStatusType": string;
    /**
     * @example "EXECUTING"
     */
    "listOrderStatus": string;
    /**
     * @example "h2USkA5YQpaXHPIrkd96xE"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1565245656253
     */
    "transactionTime": number;
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
}

interface GetApiV3AllOrderListParams {
    /**
     * Trade id to fetch from. Default gets most recent trades.
     * int64
     */
    fromId?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3AllOrderListResponseItem {
    /**
     * int64
     * @example 29
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "EXEC_STARTED"
     */
    "listStatusType": string;
    /**
     * @example "EXECUTING"
     */
    "listOrderStatus": string;
    /**
     * @example "amEEAXryFzFwYF1FeRpUoZ"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1565245913483
     */
    "transactionTime": number;
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    "isIsolated": boolean;
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
}

interface GetApiV3OpenOrderListParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3OpenOrderListResponseItem {
    /**
     * int64
     * @example 31
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "EXEC_STARTED"
     */
    "listStatusType": string;
    /**
     * @example "EXECUTING"
     */
    "listOrderStatus": string;
    /**
     * @example "wuB13fmulKj3YjdqWEcsnp"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1565246080644
     */
    "transactionTime": number;
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
}

interface GetApiV3AccountParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3MyTradesParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * This can only be used in combination with symbol.
     * int64
     */
    orderId?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Trade id to fetch from. Default gets most recent trades.
     * int64
     */
    fromId?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3RateLimitOrderParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetApiV3RateLimitOrderResponseItem {
    "rateLimitType": string;
    "interval": string;
    /**
     * int32
     */
    "intervalNum": number;
    /**
     * int32
     */
    "limit": number;
    /**
     * int32
     */
    "count"?: number;
}

interface GetSapiV1MarginTransferParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * Transfer Type
     */
    type?: "ROLL_IN" | "ROLL_OUT";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * Default: false. Set to true for archived data from 6 months ago
     */
    archived?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginTransferResponse {
    "rows": {
        "amount": string;
        "asset": string;
        "status": string;
        /**
         * int64
         */
        "timestamp": number;
        /**
         * int64
         */
        "txId": number;
        "type": string;
    }[];
    /**
     * int32
     * @example 3
     */
    "total": number;
}

interface GetSapiV1MarginLoanParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * Isolated symbol
     */
    isolatedSymbol?: string;
    /**
     * the tranId in  `POST /sapi/v1/margin/loan`
     * int64
     * @example 123456789
     */
    txId?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * Default: false. Set to true for archived data from 6 months ago
     */
    archived?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginLoanResponse {
    "rows": {
        "isolatedSymbol": string;
        /**
         * int64
         */
        "txId": number;
        "asset": string;
        "principal": string;
        /**
         * int64
         */
        "timestamp": number;
        "status": string;
    }[];
    /**
     * int32
     */
    "total": number;
}

interface GetSapiV1MarginRepayParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * Isolated symbol
     */
    isolatedSymbol?: string;
    /**
     * the tranId in  `POST /sapi/v1/margin/repay`
     * int64
     * @example 2970933056
     */
    txId?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * Default: false. Set to true for archived data from 6 months ago
     */
    archived?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginRepayResponse {
    "rows": {
        /**
         * Isolated symbol, will not be returned for crossed margin
         * @example "BNBUSDT"
         */
        "isolatedSymbol": string;
        /**
         * Total amount repaid
         * @example "14.00000000"
         */
        "amount": string;
        /**
         * @example "BNB"
         */
        "asset": string;
        /**
         * Interest repaid
         * @example "0.01866667"
         */
        "interest": string;
        /**
         * Principal repaid
         * @example "13.98133333"
         */
        "principal": string;
        /**
         * One of PENDING (pending execution), CONFIRMED (successfully execution), FAILED (execution failed, nothing happened to your account)
         * @example "CONFIRMED"
         */
        "status": string;
        /**
         * int64
         * @example 1563438204000
         */
        "timestamp": number;
        /**
         * int64
         * @example 2970933056
         */
        "txId": number;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1MarginAssetParams {
    /**
     * @example "BTC"
     */
    asset: string;
}

interface GetSapiV1MarginAssetResponse {
    /**
     * @example "Binance Coin"
     */
    "assetFullName": string;
    /**
     * @example "BNB"
     */
    "assetName": string;
    "isBorrowable": boolean;
    "isMortgageable": boolean;
    /**
     * @example "0.00000000"
     */
    "userMinBorrow": string;
    /**
     * @example "0.00000000"
     */
    "userMinRepay": string;
}

interface GetSapiV1MarginPairParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
}

interface GetSapiV1MarginPairResponse {
    /**
     * int64
     * @example 323355778339572400
     */
    "id": number;
    /**
     * @example "BNBUSDT"
     */
    "symbol": string;
    /**
     * @example "BTC"
     */
    "base": string;
    /**
     * @example "USDT"
     */
    "quote": string;
    "isMarginTrade": boolean;
    "isBuyAllowed": boolean;
    "isSellAllowed": boolean;
}

interface GetSapiV1MarginAllAssetsResponseItem {
    /**
     * @example "Binance coin"
     */
    "assetFullName": string;
    /**
     * @example "BNB"
     */
    "assetName": string;
    "isBorrowable": boolean;
    "isMortgageable": boolean;
    /**
     * @example "0.00000000"
     */
    "userMinBorrow": string;
    /**
     * @example "0.00000000"
     */
    "userMinRepay": string;
}

interface GetSapiV1MarginAllPairsResponseItem {
    /**
     * @example "BNB"
     */
    "base": string;
    /**
     * int64
     * @example 351637150141315840
     */
    "id": number;
    "isBuyAllowed": boolean;
    "isMarginTrade": boolean;
    "isSellAllowed": boolean;
    /**
     * @example "BTC"
     */
    "quote": string;
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
}

interface GetSapiV1MarginPriceIndexParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
}

interface GetSapiV1MarginPriceIndexResponse {
    /**
     * int64
     * @example 1562046418000
     */
    "calcTime": number;
    /**
     * @example "0.00333930"
     */
    "price": string;
    /**
     * @example "BNBBTC"
     */
    "symbol": string;
}

interface GetSapiV1MarginOrderParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Order id
     * int64
     */
    orderId?: number;
    /**
     * Order id from client
     */
    origClientOrderId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginInterestHistoryParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * Isolated symbol
     */
    isolatedSymbol?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * Default: false. Set to true for archived data from 6 months ago
     */
    archived?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginInterestHistoryResponse {
    "rows": {
        /**
         * @example "BNBUSDT"
         */
        "isolatedSymbol": string;
        /**
         * @example "BNB"
         */
        "asset": string;
        /**
         * @example "0.01866667"
         */
        "interest": string;
        /**
         * int64
         * @example 1566813600
         */
        "interestAccuredTime": number;
        /**
         * @example "0.01600000"
         */
        "interestRate": string;
        /**
         * @example "36.22000000"
         */
        "principal": string;
        /**
         * @example "ON_BORROW"
         */
        "type": string;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1MarginForceLiquidationRecParams {
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Isolated symbol
     */
    isolatedSymbol?: string;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginForceLiquidationRecResponse {
    "rows": {
        "avgPrice": string;
        "executedQty": string;
        /**
         * int64
         */
        "orderId": number;
        "price": string;
        "qty": string;
        "side": string;
        "symbol": string;
        "timeInForce": string;
        "isIsolated": boolean;
        /**
         * int64
         */
        "updatedTime": number;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1MarginAccountParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginAccountResponse {
    "borrowEnabled": boolean;
    /**
     * @example "11.64405625"
     */
    "marginLevel": string;
    /**
     * @example "6.82728457"
     */
    "totalAssetOfBtc": string;
    /**
     * @example "0.58633215"
     */
    "totalLiabilityOfBtc": string;
    /**
     * @example "6.24095242"
     */
    "totalNetAssetOfBtc": string;
    "tradeEnabled": boolean;
    "transferEnabled": boolean;
    "userAssets": {
        /**
         * @example "BTC"
         */
        "asset": string;
        /**
         * @example "0.00000000"
         */
        "borrowed": string;
        /**
         * @example "0.00499500"
         */
        "free": string;
        /**
         * @example "0.00000000"
         */
        "interest": string;
        /**
         * @example "0.00000000"
         */
        "locked": string;
        /**
         * @example "0.00499500"
         */
        "netAsset": string;
    }[];
}

interface GetSapiV1MarginOpenOrdersParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginAllOrdersParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Order id
     * int64
     */
    orderId?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginOrderListParams {
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Mandatory for isolated margin, not supported for cross margin
     */
    symbol?: string;
    /**
     * Order list id
     * int64
     */
    orderListId?: number;
    /**
     * Order id from client
     */
    origClientOrderId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginOrderListResponse {
    /**
     * int64
     * @example 27
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "EXEC_STARTED"
     */
    "listStatusType": string;
    /**
     * @example "EXECUTING"
     */
    "listOrderStatus": string;
    /**
     * @example "h2USkA5YQpaXHPIrkd96xE"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1565245656253
     */
    "transactionTime": number;
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    "isIsolated": boolean;
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
}

interface GetSapiV1MarginAllOrderListParams {
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Mandatory for isolated margin, not supported for cross margin
     */
    symbol?: string;
    /**
     * If supplied, neither `startTime` or `endTime` can be provided
     */
    fromId?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default Value: 500; Max Value: 1000
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginAllOrderListResponseItem {
    /**
     * int64
     * @example 29
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "EXEC_STARTED"
     */
    "listStatusType": string;
    /**
     * @example "EXECUTING"
     */
    "listOrderStatus": string;
    /**
     * @example "amEEAXryFzFwYF1FeRpUoZ"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1565245913483
     */
    "transactionTime": number;
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    "isIsolated": boolean;
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
}

interface GetSapiV1MarginOpenOrderListParams {
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Mandatory for isolated margin, not supported for cross margin
     */
    symbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginOpenOrderListResponseItem {
    /**
     * int64
     * @example 31
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "EXEC_STARTED"
     */
    "listStatusType": string;
    /**
     * @example "EXECUTING"
     */
    "listOrderStatus": string;
    /**
     * @example "wuB13fmulKj3YjdqWEcsnp"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1565246080644
     */
    "transactionTime": number;
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    "isIsolated": boolean;
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
}

interface GetSapiV1MarginMyTradesParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Trade id to fetch from. Default gets most recent trades.
     * int64
     */
    fromId?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginMaxBorrowableParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * Isolated symbol
     */
    isolatedSymbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginMaxBorrowableResponse {
    /**
     * account's currently max borrowable amount with sufficient system availability
     * @example "1.69248805"
     */
    "amount": string;
    /**
     * max borrowable amount limited by the account level
     * @example "60"
     */
    "borrowLimit": string;
}

interface GetSapiV1MarginMaxTransferableParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * Isolated symbol
     */
    isolatedSymbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginMaxTransferableResponse {
    /**
     * Account's currently max borrowable amount with sufficient system availability
     */
    "amount": string;
    /**
     * Max borrowable amount limited by the account level
     */
    "borrowLimit": string;
}

interface GetSapiV1MarginTradeCoeffParams {
    /**
     * Email Address
     * @example "me@email.com"
     */
    email: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginTradeCoeffResponse {
    /**
     * Account's currently max borrowable amount with sufficient system availability
     * @example "1.5"
     */
    "normalBar"?: string;
    /**
     * Max borrowable amount limited by the account level
     * @example "1.3"
     */
    "marginCallBar"?: string;
    /**
     * Liquidation Margin Ratio
     * @example "1.1"
     */
    "forceLiquidationBar"?: string;
}

interface GetSapiV1MarginIsolatedTransferParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * @example "SPOT"
     */
    transFrom?: "SPOT" | "ISOLATED_MARGIN";
    /**
     * @example "ISOLATED_MARGIN"
     */
    transTo?: "SPOT" | "ISOLATED_MARGIN";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginIsolatedAccountParams {
    /**
     * Max 5 symbols can be sent; separated by ','
     * @example "BTCUSDT,BNBUSDT,ADAUSDT"
     */
    symbols?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginIsolatedAccountLimitParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginIsolatedAccountLimitResponse {
    /**
     * int64
     * @example 5
     */
    "enabledAccount": number;
    /**
     * int64
     * @example 20
     */
    "maxAccount": number;
}

interface GetSapiV1MarginIsolatedPairParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginIsolatedPairResponse {
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * @example "BTC"
     */
    "base": string;
    /**
     * @example "USDT"
     */
    "quote": string;
    "isMarginTrade": boolean;
    "isBuyAllowed": boolean;
    "isSellAllowed": boolean;
}

interface GetSapiV1MarginIsolatedAllPairsParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginIsolatedAllPairsResponseItem {
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * @example "BTC"
     */
    "base": string;
    /**
     * @example "USDT"
     */
    "quote": string;
    "isMarginTrade": boolean;
    "isBuyAllowed": boolean;
    "isSellAllowed": boolean;
}

interface GetSapiV1BnbBurnParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginInterestRateHistoryParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * Defaults to user's vip level
     * int32
     * @example 1
     */
    vipLevel?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginInterestRateHistoryResponseItem {
    /**
     * @example "BTC"
     */
    "asset": string;
    /**
     * @example "0.00025000"
     */
    "dailyInterestRate": string;
    /**
     * int64
     * @example 1611544731000
     */
    "timestamp": number;
    /**
     * int32
     * @example 1
     */
    "vipLevel": number;
}

interface GetSapiV1MarginCrossMarginDataParams {
    /**
     * Defaults to user's vip level
     * int32
     * @example 1
     */
    vipLevel?: number;
    /**
     * Coin name
     * @example "BNB"
     */
    coin?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginCrossMarginDataResponseItem {
    /**
     * int32
     */
    "vipLevel": number;
    /**
     * @example "BTC"
     */
    "coin": string;
    "transferIn": boolean;
    "borrowable": boolean;
    /**
     * @example "0.00026125"
     */
    "dailyInterest": string;
    /**
     * @example "0.0953"
     */
    "yearlyInterest": string;
    /**
     * @example "180"
     */
    "borrowLimit": string;
    /**
     * @example ["BNBBTC","TRXBTC","ETHBTC","BTCUSDT"]
     */
    "marginablePairs": string[];
}

interface GetSapiV1MarginIsolatedMarginDataParams {
    /**
     * Defaults to user's vip level
     * int32
     * @example 1
     */
    vipLevel?: number;
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginIsolatedMarginDataResponseItem {
    /**
     * int32
     */
    "vipLevel"?: number;
    /**
     * @example "BTCUSDT"
     */
    "symbol"?: string;
    /**
     * @example "10"
     */
    "leverage"?: string;
    /**
     * @example [{"coin":"BTC","dailyInterest":"0.00026125","borrowLimit":"270"},{"coin":"USDT","dailyInterest":"0.000475","borrowLimit":"2100000"}]
     */
    "data"?: {
        "coin"?: string;
        "dailyInterest"?: string;
        "borrowLimit"?: string;
    }[];
}

interface GetSapiV1MarginIsolatedMarginTierParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * All margin tier data will be returned if tier is omitted
     * @example 1
     */
    tier?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginIsolatedMarginTierResponseItem {
    /**
     * @example "BTCUSDT"
     */
    "symbol"?: string;
    /**
     * int32
     * @example 1
     */
    "tier"?: number;
    /**
     * @example "10"
     */
    "effectiveMultiple"?: string;
    /**
     * @example "1.111"
     */
    "initialRiskRatio"?: string;
    /**
     * @example "1.05"
     */
    "liquidationRiskRatio"?: string;
    /**
     * @example "9"
     */
    "baseAssetMaxBorrowable"?: string;
    /**
     * @example "70000"
     */
    "quoteAssetMaxBorrowable"?: string;
}

interface GetSapiV1MarginRateLimitOrderParams {
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: string;
    /**
     * isolated symbol, mandatory for isolated margin
     */
    symbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginRateLimitOrderResponseItem {
    /**
     * @example "ORDERS"
     */
    "rateLimitType": string;
    /**
     * @example "SECOND"
     */
    "interval": string;
    /**
     * int64
     * @example 10
     */
    "intervalNum": number;
    /**
     * int64
     * @example 10000
     */
    "limit": number;
    /**
     * int64
     */
    "count": number;
}

interface GetSapiV1MarginDribbletParams {
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MarginDribbletResponse {
    /**
     * int64
     * @example 8
     */
    "total": number;
    "userAssetDribblets": {
        /**
         * int64
         * @example 1615985535000
         */
        "operateTime": number;
        /**
         * @example "0.00132256"
         */
        "totalTransferedAmount": string;
        /**
         * @example "0.00002699"
         */
        "totalServiceChargeAmount": string;
        /**
         * int64
         * @example 45178372831
         */
        "transId": number;
        "userAssetDribbletDetails": {
            /**
             * int64
             * @example 4359321
             */
            "transId": number;
            /**
             * @example "0.000009"
             */
            "serviceChargeAmount": string;
            /**
             * @example "0.0009"
             */
            "amount": string;
            /**
             * int64
             * @example 1615985535000
             */
            "operateTime": number;
            /**
             * @example "0.000441"
             */
            "transferedAmount": string;
            /**
             * @example "USDT"
             */
            "fromAsset": string;
        }[];
    }[];
}

interface GetSapiV1SystemStatusResponse {
    /**
     * 0: normal, 1：system maintenance
     * int32
     */
    "status": number;
    /**
     * "normal", "system_maintenance"
     * @example "normal"
     */
    "msg": string;
}

interface GetSapiV1CapitalConfigGetallParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1CapitalConfigGetallResponseItem {
    /**
     * @example "BTC"
     */
    "coin": string;
    "depositAllEnable": boolean;
    /**
     * @example "0.00000000"
     */
    "free": string;
    /**
     * @example "0.00000000"
     */
    "freeze": string;
    /**
     * @example "0.00000000"
     */
    "ipoable": string;
    /**
     * @example "0.00000000"
     */
    "ipoing": string;
    "isLegalMoney": boolean;
    /**
     * @example "0.00000000"
     */
    "locked": string;
    /**
     * @example "Bitcoin"
     */
    "name": string;
    "networkList": {
        /**
         * @example "^(bnb1)[0-9a-z]{38}$"
         */
        "addressRegex": string;
        /**
         * @example "BTC"
         */
        "coin": string;
        /**
         * shown only when "depositEnable" is false.
         * @example "Wallet Maintenance, Deposit Suspended"
         */
        "depositDesc": string;
        "depositEnable": boolean;
        "isDefault": boolean;
        /**
         * @example "^[0-9A-Za-z\\-_]{1,120}$"
         */
        "memoRegex": string;
        /**
         * min number for balance confirmation.
         * int64
         * @example 1
         */
        "minConfirm": number;
        /**
         * @example "BEP2"
         */
        "name": string;
        /**
         * @example "ETH"
         */
        "network": string;
        "resetAddressStatus": boolean;
        /**
         * @example "Both a MEMO and an Address are required to successfully deposit your BEP2-BTCB tokens to Binance."
         */
        "specialTips": string;
        /**
         * confirmation number for balance unlock.
         * int64
         */
        "unLockConfirm": number;
        /**
         * shown only when "withdrawEnable" is false
         * @example "Wallet Maintenance, Withdrawal Suspended"
         */
        "withdrawDesc": string;
        "withdrawEnable": boolean;
        /**
         * @example "0.00000220"
         */
        "withdrawFee": string;
        /**
         * @example "0.00000001"
         */
        "withdrawIntegerMultiple": string;
        /**
         * @example "9999999999.99999999"
         */
        "withdrawMax": string;
        /**
         * @example "0.00000440"
         */
        "withdrawMin": string;
        "sameAddress": boolean;
    }[];
    /**
     * @example "0.00000000"
     */
    "storage": string;
    "trading": boolean;
    "withdrawAllEnable": boolean;
    /**
     * @example "0.00000000"
     */
    "withdrawing": string;
}

interface GetSapiV1AccountSnapshotParams {
    type: "SPOT" | "MARGIN" | "FUTURES";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1CapitalDepositHisrecParams {
    /**
     * Coin name
     * @example "BNB"
     */
    coin?: string;
    /**
     * * `0` - pending
     * * `6` - credited but cannot withdraw
     * * `1` - success
     * int32
     */
    status?: 0 | 6 | 1;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * int32
     */
    offset?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1CapitalDepositHisrecResponseItem {
    /**
     * @example "0.00999800"
     */
    "amount": string;
    /**
     * @example "PAXG"
     */
    "coin": string;
    /**
     * @example "ETH"
     */
    "network": string;
    /**
     * int32
     * @example 1
     */
    "status": number;
    /**
     * @example "0x788cabe9236ce061e5a892e1a59395a81fc8d62c"
     */
    "address": string;
    "addressTag": string;
    /**
     * @example "0xaad4654a3234aa6118af9b4b335f5ae81c360b2394721c019b5d1e75328b09f3"
     */
    "txId": string;
    /**
     * int64
     * @example 1599621997000
     */
    "insertTime": number;
    /**
     * int32
     */
    "transferType": number;
    /**
     * confirm times for unlocking
     * @example "12/12"
     */
    "unlockConfirm": string;
    /**
     * @example "12/12"
     */
    "confirmTimes": string;
}

interface GetSapiV1CapitalWithdrawHistoryParams {
    /**
     * Coin name
     * @example "BNB"
     */
    coin?: string;
    withdrawOrderId?: string;
    /**
     * * `0` - Email Sent
     * * `1` - Cancelled
     * * `2` - Awaiting Approval
     * * `3` - Rejected
     * * `4` - Processing
     * * `5` - Failure
     * * `6` - Completed
     * int32
     */
    status?: 0 | 1 | 2 | 3 | 4 | 5 | 6;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * int32
     */
    offset?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1CapitalWithdrawHistoryResponseItem {
    /**
     * @example "0x94df8b352de7f46f64b01d3666bf6e936e44ce60"
     */
    "address": string;
    /**
     * @example "8.91000000"
     */
    "amount": string;
    /**
     * @example "2019-10-12 11:12:02"
     */
    "applyTime": string;
    /**
     * @example "USDT"
     */
    "coin": string;
    /**
     * @example "b6ae22b3aa844210a7041aee7589627c"
     */
    "id": string;
    /**
     * will not be returned if there's no withdrawOrderId for this withdraw.
     * @example "WITHDRAWtest123"
     */
    "withdrawOrderId": string;
    /**
     * @example "ETH"
     */
    "network": string;
    /**
     * 1 for internal transfer, 0 for external transfer
     * int32
     */
    "transferType": number;
    /**
     * int32
     * @example 6
     */
    "status": number;
    /**
     * @example "0.004"
     */
    "transactionFee": string;
    /**
     * int32
     * @example 3
     */
    "confirmNo"?: number;
    /**
     * Reason for withdrawal failure
     * @example "The address is not valid. Please confirm with the recipient"
     */
    "info"?: string;
    /**
     * @example "0xb5ef8c13b968a406cc62a93a8bd80f9e9a906ef1b3fcf20a2e48573c17659268"
     */
    "txId": string;
}

interface GetSapiV1CapitalDepositAddressParams {
    /**
     * Coin name
     * @example "BNB"
     */
    coin: string;
    /**
     * @example "ETH"
     */
    network?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1CapitalDepositAddressResponse {
    /**
     * @example "1HPn8Rx2y6nNSfagQBKy27GB99Vbzg89wv"
     */
    "address": string;
    /**
     * @example "BTC"
     */
    "coin": string;
    "tag": string;
    /**
     * @example "https://btc.com/1HPn8Rx2y6nNSfagQBKy27GB99Vbzg89wv"
     */
    "url": string;
}

interface GetSapiV1AccountStatusParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AccountStatusResponse {
    /**
     * @example "Normal"
     */
    "data": string;
}

interface GetSapiV1AccountApiTradingStatusParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AccountApiTradingStatusResponse {
    "data": {
        /**
         * API trading function is locked or not
         */
        "isLocked": boolean;
        /**
         * If API trading function is locked, this is the planned recover time
         * int64
         */
        "plannedRecoverTime": number;
        "triggerCondition": {
            /**
             * Number of GTC orders
             * int64
             * @example 150
             */
            "GCR": number;
            /**
             * Number of FOK/IOC orders
             * int64
             * @example 150
             */
            "IFER": number;
            /**
             * Number of orders
             * int64
             * @example 300
             */
            "UFR": number;
        };
        /**
         * The indicators updated every 30 seconds
         */
        "indicators": {
            "BTCUSDT": {
                /**
                 * Unfilled Ratio (UFR)
                 * @example "UFR"
                 */
                "i": string;
                /**
                 * Count of all orders
                 * int64
                 * @example 20
                 */
                "c": number;
                /**
                 * Current UFR value
                 * float
                 * @example 0.05
                 */
                "v": number;
                /**
                 * Trigger UFR value
                 * float
                 * @example 0.99
                 */
                "t": number;
            }[];
        };
        /**
         * int64
         * @example 1547630471725
         */
        "updateTime": number;
    };
}

interface GetSapiV1AssetDribbletParams {
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AssetDribbletResponse {
    /**
     * Total counts of exchange
     * int64
     * @example 8
     */
    "total": number;
    "userAssetDribblets": {
        /**
         * int64
         * @example 1615985535000
         */
        "operateTime": number;
        /**
         * Total transfered BNB amount for this exchange.
         * @example "0.00132256"
         */
        "totalTransferedAmount": string;
        /**
         * Total service charge amount for this exchange.
         * @example "0.00002699"
         */
        "totalServiceChargeAmount": string;
        /**
         * int64
         * @example 45178372831
         */
        "transId": number;
        "userAssetDribbletDetails": {
            /**
             * int64
             * @example 4359321
             */
            "transId": number;
            /**
             * @example "0.000009"
             */
            "serviceChargeAmount": string;
            /**
             * @example "0.0009"
             */
            "amount": string;
            /**
             * int64
             * @example 1615985535000
             */
            "operateTime": number;
            /**
             * @example "0.000441"
             */
            "transferedAmount": string;
            /**
             * @example "USDT"
             */
            "fromAsset": string;
        }[];
    }[];
}

interface GetSapiV1AssetAssetDividendParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AssetAssetDividendResponse {
    "rows": {
        /**
         * int64
         * @example 242006910
         */
        "id": number;
        /**
         * @example "10.00000000"
         */
        "amount": string;
        /**
         * @example "BHFT"
         */
        "asset": string;
        /**
         * int64
         * @example 1563189166000
         */
        "divTime": number;
        /**
         * @example "BHFT distribution"
         */
        "enInfo": string;
        /**
         * int64
         * @example 2968885920
         */
        "tranId": number;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1AssetAssetDetailParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AssetAssetDetailResponse {
    "CTR": {
        /**
         * @example "70.00000000"
         */
        "minWithdrawAmount": string;
        /**
         * deposit status (false if ALL of networks' are false)
         */
        "depositStatus": boolean;
        /**
         * int64
         * @example 35
         */
        "withdrawFee": number;
        /**
         * withdrawStatus status (false if ALL of networks' are false)
         */
        "withdrawStatus": boolean;
        /**
         * @example "Delisted, Deposit Suspended"
         */
        "depositTip": string;
    };
}

interface GetSapiV1AssetTradeFeeParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AssetTradeFeeResponseItem {
    /**
     * @example "ADABNB"
     */
    "symbol": string;
    /**
     * @example "0.001"
     */
    "makerCommission": string;
    /**
     * @example "0.001"
     */
    "takerCommission": string;
}

interface GetSapiV1AssetTransferParams {
    /**
     * Universal transfer type
     * @example "MAIN_C2C"
     */
    type: "MAIN_C2C" | "MAIN_UMFUTURE" | "MAIN_CMFUTURE" | "MAIN_MARGIN" | "MAIN_MINING" | "C2C_MAIN" | "C2C_UMFUTURE" | "C2C_MINING" | "C2C_MARGIN" | "UMFUTURE_MAIN" | "UMFUTURE_C2C" | "UMFUTURE_MARGIN" | "CMFUTURE_MAIN" | "CMFUTURE_MARGIN" | "MARGIN_MAIN" | "MARGIN_UMFUTURE" | "MARGIN_CMFUTURE" | "MARGIN_MINING" | "MARGIN_C2C" | "MINING_MAIN" | "MINING_UMFUTURE" | "MINING_C2C" | "MINING_MARGIN" | "MAIN_PAY" | "PAY_MAIN" | "ISOLATEDMARGIN_MARGIN" | "MARGIN_ISOLATEDMARGIN" | "ISOLATEDMARGIN_ISOLATEDMARGIN";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * Must be sent when type are ISOLATEDMARGIN_MARGIN and ISOLATEDMARGIN_ISOLATEDMARGIN
     * @example "BNBUSDT"
     */
    fromSymbol?: string;
    /**
     * Must be sent when type are MARGIN_ISOLATEDMARGIN and ISOLATEDMARGIN_ISOLATEDMARGIN
     * @example "BNBUSDT"
     */
    toSymbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AssetTransferResponse {
    /**
     * int32
     * @example 1
     */
    "total": number;
    "rows": {
        /**
         * @example "USDT"
         */
        "asset": string;
        /**
         * @example "1"
         */
        "amount": string;
        /**
         * @example "MAIN_UMFUTUR"
         */
        "type": string;
        /**
         * @example "CONFIRMED"
         */
        "status": string;
        /**
         * int64
         * @example 11415955596
         */
        "tranId": number;
        /**
         * int64
         * @example 1544433328000
         */
        "timestamp": number;
    }[];
}

interface GetSapiV1AccountApiRestrictionsParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AccountApiRestrictionsResponse {
    "ipRestrict": boolean;
    /**
     * int64
     * @example 1623840271000
     */
    "createTime": number;
    /**
     * This option allows you to withdraw via API. You must apply the IP Access Restriction filter in order to enable withdrawals
     */
    "enableWithdrawals": boolean;
    /**
     * This option authorizes this key to transfer funds between your master account and your sub account instantly
     */
    "enableInternalTransfer": boolean;
    /**
     * Authorizes this key to be used for a dedicated universal transfer API to transfer multiple supported currencies. Each business's own transfer API rights are not affected by this authorization
     */
    "permitsUniversalTransfer": boolean;
    /**
     * Authorizes this key to Vanilla options trading
     */
    "enableVanillaOptions": boolean;
    "enableReading": boolean;
    /**
     * API Key created before your futures account opened does not support futures API service
     */
    "enableFutures": boolean;
    /**
     * This option can be adjusted after the Cross Margin account transfer is completed
     */
    "enableMargin": boolean;
    "enableSpotAndMarginTrading": boolean;
    /**
     * Expiration time for spot and margin trading permission
     * int64
     * @example 1628985600000
     */
    "tradingAuthorityExpirationTime": number;
}

interface GetSapiV1SubAccountListParams {
    /**
     * Sub-account email
     */
    email?: string;
    isFreeze?: "true" | "false";
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Default 1; max 200
     * int32
     * @example 1
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountListResponse {
    "subAccounts": {
        /**
         * @example "testsub@gmail.com"
         */
        "email": string;
        "isFreeze": boolean;
        /**
         * int64
         * @example 1544433328000
         */
        "createTime": number;
        "isManagedSubAccount": boolean;
        "isAssetManagementSubAccount": boolean;
    }[];
}

interface GetSapiV1SubAccountSubTransferHistoryParams {
    /**
     * Sub-account email
     */
    fromEmail?: string;
    /**
     * Sub-account email
     */
    toEmail?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountSubTransferHistoryResponseItem {
    /**
     * @example "aaa@test.com"
     */
    "from": string;
    /**
     * @example "bbb@test.com"
     */
    "to": string;
    /**
     * @example "BTC"
     */
    "asset": string;
    /**
     * @example 10
     */
    "qty": string;
    /**
     * @example "SUCCESS"
     */
    "status": string;
    /**
     * int64
     * @example 6489943656
     */
    "tranId": number;
    /**
     * int64
     * @example 1544433328000
     */
    "time": number;
}

interface GetSapiV1SubAccountFuturesInternalTransferParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * 1:USDT-margined Futures, 2: Coin-margined Futures
     * int32
     * @example 2
     */
    futuresType: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Default value: 50, Max value: 500
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountFuturesInternalTransferResponse {
    "success": boolean;
    /**
     * int32
     * @example 2
     */
    "futuresType": number;
    "transfers": {
        /**
         * @example "aaa@test.com"
         */
        "from": string;
        /**
         * @example "bbb@test.com"
         */
        "to": string;
        /**
         * @example "BTC"
         */
        "asset": string;
        /**
         * @example "1"
         */
        "qty": string;
        /**
         * int64
         * @example 11897001102
         */
        "tranId": number;
        /**
         * int64
         * @example 1544433328000
         */
        "time": number;
    }[];
}

interface GetSapiV3SubAccountAssetsParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV3SubAccountAssetsResponse {
    "balances": {
        /**
         * @example "ADA"
         */
        "asset": string;
        /**
         * int64
         * @example 10000
         */
        "free": number;
        /**
         * int64
         */
        "locked": number;
    }[];
}

interface GetSapiV1SubAccountSpotSummaryParams {
    /**
     * Sub-account email
     */
    email?: string;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Default:10 Max:20
     * int32
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountSpotSummaryResponse {
    /**
     * int64
     * @example 1
     */
    "totalCount": number;
    /**
     * @example "0.23231201"
     */
    "masterAccountTotalAsset": string;
    "spotSubUserAssetBtcVoList": {
        /**
         * @example "sub123@test.com"
         */
        "email": string;
        /**
         * @example "9999.00000000"
         */
        "totalAsset": string;
    }[];
}

interface GetSapiV1CapitalDepositSubAddressParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * Coin name
     * @example "BNB"
     */
    coin: string;
    network?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1CapitalDepositSubAddressResponse {
    /**
     * @example "TDunhSa7jkTNuKrusUTU1MUHtqXoBPKETV"
     */
    "address": string;
    /**
     * @example "USDT"
     */
    "coin": string;
    "tag": string;
    /**
     * @example "https://tronscan.org/#/address/TDunhSa7jkTNuKrusUTU1MUHtqXoBPKETV"
     */
    "url": string;
}

interface GetSapiV1CapitalDepositSubHisrecParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * Coin name
     * @example "BNB"
     */
    coin?: string;
    /**
     * 0(0:pending,6: credited but cannot withdraw, 1:success)
     * int32
     */
    status?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * int64
     */
    limit?: number;
    /**
     * int32
     */
    offset?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1CapitalDepositSubHisrecResponseItem {
    /**
     * @example "0.00999800"
     */
    "amount": string;
    /**
     * @example "PAXG"
     */
    "coin": string;
    /**
     * @example "ETH"
     */
    "network": string;
    /**
     * int32
     * @example 1
     */
    "status": number;
    /**
     * @example "0x788cabe9236ce061e5a892e1a59395a81fc8d62c"
     */
    "address": string;
    "addressTag": string;
    /**
     * @example "0xaad4654a3234aa6118af9b4b335f5ae81c360b2394721c019b5d1e75328b09f3"
     */
    "txId": string;
    /**
     * int64
     * @example 1599621997000
     */
    "insertTime": number;
    /**
     * int32
     */
    "transferType": number;
    /**
     * @example "12/12"
     */
    "confirmTimes": string;
}

interface GetSapiV1SubAccountStatusParams {
    /**
     * Sub-account email
     */
    email?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountStatusResponseItem {
    /**
     * @example "123@test.com"
     */
    "email": string;
    "isSubUserEnabled": boolean;
    "isUserActive": boolean;
    /**
     * sub account create time
     * int64
     * @example 1570791523523
     */
    "insertTime": number;
    "isMarginEnabled": boolean;
    "isFutureEnabled": boolean;
    /**
     * user mobile number
     * int64
     * @example 1570791523523
     */
    "mobile": number;
}

interface GetSapiV1SubAccountMarginAccountParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountMarginAccountResponse {
    /**
     * @example "123@test.com"
     */
    "email": string;
    /**
     * @example "11.64405625"
     */
    "marginLevel": string;
    /**
     * @example "6.82728457"
     */
    "totalAssetOfBtc": string;
    /**
     * @example "0.58633215"
     */
    "totalLiabilityOfBtc": string;
    /**
     * @example "6.24095242"
     */
    "totalNetAssetOfBtc": string;
    "marginTradeCoeffVo": {
        /**
         * Liquidation margin ratio
         * @example "1.10000000"
         */
        "forceLiquidationBar": string;
        /**
         * Margin call margin ratio
         * @example "1.50000000"
         */
        "marginCallBar": string;
        /**
         * Initial margin ratio
         * @example "2.00000000"
         */
        "normalBar": string;
    };
    "marginUserAssetVoList": {
        /**
         * @example "BTC"
         */
        "asset": string;
        /**
         * @example "0.00000000"
         */
        "borrowed": string;
        /**
         * @example "0.00499500"
         */
        "free": string;
        /**
         * @example "0.00000000"
         */
        "interest": string;
        /**
         * @example "0.00000000"
         */
        "locked": string;
        /**
         * @example "0.00499500"
         */
        "netAsset": string;
    }[];
}

interface GetSapiV1SubAccountMarginAccountSummaryParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountMarginAccountSummaryResponse {
    /**
     * @example "4.33333333"
     */
    "totalAssetOfBtc": string;
    /**
     * @example "2.11111112"
     */
    "totalLiabilityOfBtc": string;
    /**
     * @example "2.22222221"
     */
    "totalNetAssetOfBtc": string;
    "subAccountList": {
        /**
         * @example "123@test.com"
         */
        "email": string;
        /**
         * @example "2.11111111"
         */
        "totalAssetOfBtc": string;
        /**
         * @example "1.11111111"
         */
        "totalLiabilityOfBtc": string;
        /**
         * @example "1.00000000"
         */
        "totalNetAssetOfBtc": string;
    }[];
}

interface GetSapiV1SubAccountFuturesAccountParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountFuturesAccountResponse {
    /**
     * @example "abc@test.com"
     */
    "email": string;
    /**
     * @example "USDT"
     */
    "asset": string;
    "assets": {
        /**
         * @example "USDT"
         */
        "asset": string;
        /**
         * @example "0.00000000"
         */
        "initialMargin": string;
        /**
         * @example "0.00000000"
         */
        "maintenanceMargin": string;
        /**
         * @example "0.88308000"
         */
        "marginBalance": string;
        /**
         * @example "0.88308000"
         */
        "maxWithdrawAmount": string;
        /**
         * @example "0.00000000"
         */
        "openOrderInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "positionInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "unrealizedProfit": string;
        /**
         * @example "0.88308000"
         */
        "walletBalance": string;
    }[];
    "canDeposit": boolean;
    "canTrade": boolean;
    "canWithdraw": boolean;
    /**
     * int64
     * @example 2
     */
    "feeTier": number;
    /**
     * @example "0.88308000"
     */
    "maxWithdrawAmount": string;
    /**
     * @example "0.00000000"
     */
    "totalInitialMargin": string;
    /**
     * @example "0.00000000"
     */
    "totalMaintenanceMargin": string;
    /**
     * @example "0.88308000"
     */
    "totalMarginBalance": string;
    /**
     * @example "0.00000000"
     */
    "totalOpenOrderInitialMargin": string;
    /**
     * @example "0.00000000"
     */
    "totalPositionInitialMargin": string;
    /**
     * @example "0.00000000"
     */
    "totalUnrealizedProfit": string;
    /**
     * @example "0.88308000"
     */
    "totalWalletBalance": string;
    /**
     * int64
     * @example 1576756674610
     */
    "updateTime": number;
}

interface GetSapiV1SubAccountFuturesAccountSummaryParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountFuturesAccountSummaryResponse {
    /**
     * @example "9.83137400"
     */
    "totalInitialMargin": string;
    /**
     * @example "0.41568700"
     */
    "totalMaintenanceMargin": string;
    /**
     * @example "23.03235621"
     */
    "totalMarginBalance": string;
    /**
     * @example "9.00000000"
     */
    "totalOpenOrderInitialMargin": string;
    /**
     * @example "0.83137400"
     */
    "totalPositionInitialMargin": string;
    /**
     * @example "0.03219710"
     */
    "totalUnrealizedProfit": string;
    /**
     * @example "22.15879444"
     */
    "totalWalletBalance": string;
    /**
     * @example "USD"
     */
    "asset": string;
    "subAccountList": {
        /**
         * @example "123@test.com"
         */
        "email": string;
        /**
         * @example "9.00000000"
         */
        "totalInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "totalMaintenanceMargin": string;
        /**
         * @example "22.12659734"
         */
        "totalMarginBalance": string;
        /**
         * @example "9.00000000"
         */
        "totalOpenOrderInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "totalPositionInitialMargin": string;
        /**
         * @example "0.00000000"
         */
        "totalUnrealizedProfit": string;
        /**
         * @example "22.12659734"
         */
        "totalWalletBalance": string;
        /**
         * @example "USD"
         */
        "asset": string;
    }[];
}

interface GetSapiV1SubAccountFuturesPositionRiskParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountFuturesPositionRiskResponseItem {
    /**
     * @example "9975.12000"
     */
    "entryPrice": string;
    /**
     * current initial leverage
     * @example "50"
     */
    "leverage": string;
    /**
     * notional value limit of current initial leverage
     * @example "1000000"
     */
    "maxNotional": string;
    /**
     * @example "7963.54"
     */
    "liquidationPrice": string;
    /**
     * @example "9973.50770517"
     */
    "markPrice": string;
    /**
     * @example "0.010"
     */
    "positionAmount": string;
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
    /**
     * @example "-0.01612295"
     */
    "unrealizedProfit": string;
}

interface GetSapiV1SubAccountTransferSubUserHistoryParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * * `1` - transfer in
     * * `2` - transfer out
     * int32
     */
    type?: 1 | 2;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountTransferSubUserHistoryResponseItem {
    /**
     * @example "master"
     */
    "counterParty": string;
    /**
     * @example "master@test.com"
     */
    "email": string;
    /**
     * 1 for transfer in, 2 for transfer out
     * int32
     * @example 1
     */
    "type": number;
    /**
     * @example "BTC"
     */
    "asset": string;
    /**
     * @example "1"
     */
    "qty": string;
    /**
     * @example "SPOT"
     */
    "fromAccountType": string;
    /**
     * @example "SPOT"
     */
    "toAccountType": string;
    /**
     * @example "SUCCESS"
     */
    "status": string;
    /**
     * int64
     * @example 11798835829
     */
    "tranId": number;
    /**
     * int64
     * @example 1544433325000
     */
    "time": number;
}

interface GetSapiV1SubAccountUniversalTransferParams {
    /**
     * Sub-account email
     */
    fromEmail?: string;
    /**
     * Sub-account email
     */
    toEmail?: string;
    clientTranId?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Default 500, Max 500
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountUniversalTransferResponseItem {
    /**
     * int64
     * @example 11945860693
     */
    "tranId": number;
    /**
     * @example "master@test.com"
     */
    "fromEmail": string;
    /**
     * @example "subaccount1@test.com"
     */
    "toEmail": string;
    /**
     * @example "BTC"
     */
    "asset": string;
    /**
     * @example "0.1"
     */
    "amount": string;
    /**
     * @example "SPOT"
     */
    "fromAccountType": string;
    /**
     * @example "COIN_FUTURE"
     */
    "toAccountType": string;
    /**
     * @example "SUCCESS"
     */
    "status": string;
    /**
     * int64
     * @example 1544433325000
     */
    "createTimeStamp": number;
    /**
     * @example "11945860694"
     */
    "clientTranId": string;
}

interface GetSapiV2SubAccountFuturesAccountParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * * `1` - USDT Margined Futures
     * * `2` - COIN Margined Futures
     * int32
     */
    futuresType: 1 | 2;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV2SubAccountFuturesAccountSummaryParams {
    /**
     * * `1` - USDT Margined Futures
     * * `2` - COIN Margined Futures
     * int32
     */
    futuresType: 1 | 2;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Default 10, Max 20
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV2SubAccountFuturesPositionRiskParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * * `1` - USDT Margined Futures
     * * `2` - COIN Margined Futures
     * int32
     */
    futuresType: 1 | 2;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1ManagedSubaccountAssetParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1ManagedSubaccountAssetResponseItem {
    /**
     * @example "INJ"
     */
    "coin": string;
    /**
     * @example "Injective Protocol"
     */
    "name": string;
    /**
     * @example "0"
     */
    "totalBalance": string;
    /**
     * @example "0"
     */
    "availableBalance": string;
    /**
     * @example "0"
     */
    "inOrder": string;
    /**
     * @example "0"
     */
    "btcValue": string;
}

interface GetSapiV1ManagedSubaccountAccountSnapshotParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * "SPOT", "MARGIN"(cross), "FUTURES"(UM)
     * @example "SPOT"
     */
    type: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * min 7, max 30, default 7
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1ManagedSubaccountAccountSnapshotResponse {
    /**
     * int32
     * @example 200
     */
    "code": number;
    "msg": string;
    "snapshotVos": {
        "data": {
            "balances": {
                /**
                 * @example "BTC"
                 */
                "asset": string;
                /**
                 * @example "0.09905021"
                 */
                "free": string;
                /**
                 * @example "0.00000000"
                 */
                "locked": string;
            }[];
            /**
             * @example "0.09942700"
             */
            "totalAssetOfBtc": string;
        };
        /**
         * @example "spot"
         */
        "type": string;
        /**
         * int64
         * @example 1576281599000
         */
        "updateTime": number;
    }[];
}

interface GetSapiV1SubAccountSubAccountApiIpRestrictionParams {
    /**
     * Sub-account email
     */
    email: string;
    subAccountApiKey: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1SubAccountSubAccountApiIpRestrictionResponse {
    /**
     * @example "true"
     */
    "ipRestrict": string;
    /**
     * @example ["69.210.67.14","8.34.21.10"]
     */
    "ipList": string[];
    /**
     * int64
     * @example 1636369557189
     */
    "updateTime": number;
    /**
     * @example "k5V49ldtn4tszj6W3hystegdfvmGbqDzjmkCtpTvC0G74WhK7yd4rfCTo4lShf"
     */
    "apiKey": string;
}

interface GetSapiV1FiatOrdersParams {
    /**
     * * `0` - deposit
     * * `1` - withdraw
     * @example "0"
     */
    transactionType: "0" | "1";
    /**
     * int64
     * @example 1626144956000
     */
    beginTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Default 100, max 500
     * int32
     * @example 300
     */
    rows?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1FiatOrdersResponse {
    /**
     * @example "000000"
     */
    "code": string;
    /**
     * @example "success"
     */
    "message": string;
    "data": {
        /**
         * @example "7d76d611-0568-4f43-afb6-24cac7767365"
         */
        "orderNo": string;
        /**
         * @example "BRL"
         */
        "fiatCurrency": string;
        /**
         * @example "10.00"
         */
        "indicatedAmount": string;
        /**
         * @example "10.00"
         */
        "amount": string;
        /**
         * @example "0.00"
         */
        "totalFee": string;
        /**
         * @example "BankAccount"
         */
        "method": string;
        /**
         * Processing, Failed, Successful, Finished, Refunding, Refunded, Refund Failed, Order Partial credit Stopped
         * @example "Expired"
         */
        "status": string;
        /**
         * int64
         * @example 1626144956000
         */
        "createTime": number;
        /**
         * int64
         * @example 1626400907000
         */
        "updateTime": number;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
    "success": boolean;
}

interface GetSapiV1FiatPaymentsParams {
    /**
     * * `0` - deposit
     * * `1` - withdraw
     * @example "0"
     */
    transactionType: "0" | "1";
    /**
     * int64
     * @example 1626144956000
     */
    beginTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Default 100, max 500
     * int32
     * @example 300
     */
    rows?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1FiatPaymentsResponse {
    /**
     * @example "000000"
     */
    "code": string;
    /**
     * @example "success"
     */
    "message": string;
    "data": {
        /**
         * @example "353fca443f06466db0c4dc89f94f027a"
         */
        "orderNo": string;
        /**
         * Fiat trade amount
         * @example "20.00"
         */
        "sourceAmount": string;
        /**
         * Fiat token
         * @example "EUR"
         */
        "fiatCurrency": string;
        /**
         * Crypto trade amount
         * @example "4.462"
         */
        "obtainAmount": string;
        /**
         * Crypto token
         * @example "LUNA"
         */
        "cryptoCurrency": string;
        /**
         * Trade fee
         * @example "0.2"
         */
        "totalFee": string;
        /**
         * @example "4.437472"
         */
        "price": string;
        /**
         * Processing, Completed, Failed, Refunded
         * @example "Failed"
         */
        "status": string;
        /**
         * int64
         * @example 1624529919000
         */
        "createTime": number;
        /**
         * int64
         * @example 1624529919000
         */
        "updateTime": number;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
    "success": boolean;
}

interface GetSapiV1LendingDailyProductListParams {
    /**
     * Default `ALL`
     */
    status?: "ALL" | "SUBSCRIBABLE" | "UNSUBSCRIBABLE";
    /**
     * Default `ALL`
     */
    featured?: "ALL" | "TRUE";
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingDailyProductListResponseItem {
    /**
     * @example "BTC"
     */
    "asset": string;
    /**
     * @example "0.00250025"
     */
    "avgAnnualInterestRate": string;
    "canPurchase": boolean;
    "canRedeem": boolean;
    /**
     * @example "0.00685000"
     */
    "dailyInterestPerThousand": string;
    "featured": boolean;
    /**
     * @example "0.01000000"
     */
    "minPurchaseAmount": string;
    /**
     * @example "BTC001"
     */
    "productId": string;
    /**
     * @example "16.32467016"
     */
    "purchasedAmount": string;
    /**
     * @example "PURCHASING"
     */
    "status": string;
    /**
     * @example "200.00000000"
     */
    "upLimit": string;
    /**
     * @example "5.00000000"
     */
    "upLimitPerUser": string;
}

interface GetSapiV1LendingDailyUserLeftQuotaParams {
    productId: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingDailyUserLeftQuotaResponse {
    /**
     * @example "BUSD"
     */
    "asset": string;
    /**
     * @example "50000.00000000"
     */
    "leftQuota": string;
}

interface GetSapiV1LendingDailyUserRedemptionQuotaParams {
    productId: string;
    type: "FAST" | "NORMAL";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingDailyUserRedemptionQuotaResponse {
    /**
     * @example "USDT"
     */
    "asset": string;
    /**
     * @example "10000000.00000000"
     */
    "dailyQuota": string;
    /**
     * @example "0.00000000"
     */
    "leftQuota": string;
    /**
     * @example "0.10000000"
     */
    "minRedemptionAmount": string;
}

interface GetSapiV1LendingDailyTokenPositionParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingDailyTokenPositionResponseItem {
    /**
     * @example "0.02600000"
     */
    "annualInterestRate": string;
    /**
     * @example "USDT"
     */
    "asset": string;
    /**
     * @example "0.02599895"
     */
    "avgAnnualInterestRate": string;
    "canRedeem": boolean;
    /**
     * @example "0.00007123"
     */
    "dailyInterestRate": string;
    /**
     * @example "75.46000000"
     */
    "freeAmount": string;
    /**
     * @example "0.00000000"
     */
    "freezeAmount": string;
    /**
     * @example "0.00000000"
     */
    "lockedAmount": string;
    /**
     * @example "USDT001"
     */
    "productId": string;
    /**
     * @example "USDT"
     */
    "productName": string;
    /**
     * @example "0.00000000"
     */
    "redeemingAmount": string;
    /**
     * @example "0.00000000"
     */
    "todayPurchasedAmount": string;
    /**
     * @example "75.46000000"
     */
    "totalAmount": string;
    /**
     * @example "0.22759183"
     */
    "totalInterest": string;
}

interface GetSapiV1LendingProjectListParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    type: "ACTIVITY" | "CUSTOMIZED_FIXED";
    /**
     * Default `ALL`
     */
    status?: "ALL" | "SUBSCRIBABLE" | "UNSUBSCRIBABLE";
    /**
     * default "true"
     */
    isSortAsc?: boolean;
    /**
     * Default `START_TIME`
     */
    sortBy?: "START_TIME" | "LOT_SIZE" | "INTEREST_RATE" | "DURATION";
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingProjectListResponseItem {
    /**
     * @example "USDT"
     */
    "asset": string;
    /**
     * int64
     * @example 1
     */
    "displayPriority": number;
    /**
     * int64
     * @example 90
     */
    "duration": number;
    /**
     * @example "1.35810000"
     */
    "interestPerLot": string;
    /**
     * @example "0.05510000"
     */
    "interestRate": string;
    /**
     * @example "100.00000000"
     */
    "lotSize": string;
    /**
     * int64
     * @example 1
     */
    "lotsLowLimit": number;
    /**
     * int64
     * @example 74155
     */
    "lotsPurchased": number;
    /**
     * int64
     * @example 80000
     */
    "lotsUpLimit": number;
    /**
     * int64
     * @example 2000
     */
    "maxLotsPerUser": number;
    "needKyc": boolean;
    /**
     * @example "CUSDT90DAYSS001"
     */
    "projectId": string;
    /**
     * @example "USDT"
     */
    "projectName": string;
    /**
     * @example "PURCHASING"
     */
    "status": string;
    /**
     * @example "CUSTOMIZED_FIXED"
     */
    "type": string;
    "withAreaLimitation": boolean;
}

interface GetSapiV1LendingProjectPositionListParams {
    /**
     * @example "BTC"
     */
    asset: string;
    projectId?: string;
    /**
     * Default `ALL`
     */
    status?: "ALL" | "SUBSCRIBABLE" | "UNSUBSCRIBABLE";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingProjectPositionListResponseItem {
    /**
     * @example "USDT"
     */
    "asset": string;
    "canTransfer": boolean;
    /**
     * int64
     * @example 1587010770000
     */
    "createTimestamp": number;
    /**
     * int64
     * @example 14
     */
    "duration": number;
    /**
     * int64
     * @example 1588291200000
     */
    "endTime": number;
    /**
     * @example "0.19950000"
     */
    "interest": string;
    /**
     * @example "0.05201250"
     */
    "interestRate": string;
    /**
     * int64
     * @example 1
     */
    "lot": number;
    /**
     * int64
     * @example 51724
     */
    "positionId": number;
    /**
     * @example "100.00000000"
     */
    "principal": string;
    /**
     * @example "CUSDT14DAYSS001"
     */
    "projectId": string;
    /**
     * @example "USDT"
     */
    "projectName": string;
    /**
     * int64
     * @example 1587010771000
     */
    "purchaseTime": number;
    /**
     * date
     * @example "2020-05-01"
     */
    "redeemDate": string;
    /**
     * int64
     * @example 1587081600000
     */
    "startTime": number;
    /**
     * @example "HOLDING"
     */
    "status": string;
    /**
     * @example "CUSTOMIZED_FIXED"
     */
    "type": string;
}

interface GetSapiV1LendingUnionAccountParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingUnionAccountResponse {
    "positionAmountVos": {
        /**
         * @example "75.46000000"
         */
        "amount": string;
        /**
         * @example "0.01044819"
         */
        "amountInBTC": string;
        /**
         * @example "75.46000000"
         */
        "amountInUSDT": string;
        /**
         * @example "USDT"
         */
        "asset": string;
    }[];
    /**
     * @example "0.01067982"
     */
    "totalAmountInBTC": string;
    /**
     * @example "77.13289230"
     */
    "totalAmountInUSDT": string;
    /**
     * @example "0.00000000"
     */
    "totalFixedAmountInBTC": string;
    /**
     * @example "0.00000000"
     */
    "totalFixedAmountInUSDT": string;
    /**
     * @example "0.01067982"
     */
    "totalFlexibleInBTC": string;
    /**
     * @example "77.13289230"
     */
    "totalFlexibleInUSDT": string;
}

interface GetSapiV1LendingUnionPurchaseRecordParams {
    /**
     * * `DAILY` - for flexible
     * * `ACTIVITY` - for activity
     * * `CUSTOMIZED_FIXED` for fixed
     */
    lendingType: "DAILY" | "ACTIVITY" | "CUSTOMIZED_FIXED";
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingUnionRedemptionRecordParams {
    /**
     * * `DAILY` - for flexible
     * * `ACTIVITY` - for activity
     * * `CUSTOMIZED_FIXED` for fixed
     */
    lendingType: "DAILY" | "ACTIVITY" | "CUSTOMIZED_FIXED";
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingUnionInterestHistoryParams {
    /**
     * * `DAILY` - for flexible
     * * `ACTIVITY` - for activity
     * * `CUSTOMIZED_FIXED` for fixed
     */
    lendingType: "DAILY" | "ACTIVITY" | "CUSTOMIZED_FIXED";
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LendingUnionInterestHistoryResponseItem {
    /**
     * @example "BUSD"
     */
    "asset": string;
    /**
     * @example "0.00006408"
     */
    "interest": string;
    /**
     * @example "DAILY"
     */
    "lendingType": string;
    /**
     * @example "BUSD"
     */
    "productName": string;
    /**
     * int64
     * @example 1577233578000
     */
    "time": number;
}

interface GetSapiV1StakingProductListParams {
    /**
     * * `STAKING` - for Locked Staking
     * * `F_DEFI` - for flexible DeFi Staking
     * * `L_DEFI` - for locked DeFi Staking
     */
    product: string;
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1StakingProductListResponseItem {
    /**
     * @example "Axs*90"
     */
    "projectId": string;
    "detail": {
        /**
         * @example "AXS"
         */
        "asset": string;
        /**
         * @example "AXS"
         */
        "rewardAsset": string;
        /**
         * int64
         * @example 90
         */
        "duration": number;
        /**
         * @example true
         */
        "renewable": boolean;
        /**
         * @example "1.2069"
         */
        "apy": string;
    };
    "quota": {
        /**
         * @example "2"
         */
        "totalPersonalQuota": string;
        /**
         * @example "0.001"
         */
        "minimum": string;
    };
}

interface GetSapiV1StakingPositionParams {
    /**
     * * `STAKING` - for Locked Staking
     * * `F_DEFI` - for flexible DeFi Staking
     * * `L_DEFI` - for locked DeFi Staking
     */
    product: string;
    productId?: string;
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1StakingPositionResponseItem {
    /**
     * @example "123123"
     */
    "positionId": string;
    /**
     * @example "Axs*90"
     */
    "projectId": string;
    /**
     * @example "AXS"
     */
    "asset": string;
    /**
     * @example "122.09202928"
     */
    "amount": string;
    /**
     * @example "1646182276000"
     */
    "purchaseTime": string;
    /**
     * @example "60"
     */
    "duration": string;
    /**
     * @example "4"
     */
    "accrualDays": string;
    /**
     * @example "AXS"
     */
    "rewardAsset": string;
    /**
     * @example "0.2032"
     */
    "APY": string;
    /**
     * @example "5.17181528"
     */
    "rewardAmt": string;
    /**
     * @example "BNB"
     */
    "extraRewardAsset": string;
    /**
     * @example "0.0203"
     */
    "extraRewardAPY": string;
    /**
     * @example "5.17181528"
     */
    "estExtraRewardAmt": string;
    /**
     * @example "1.29295383"
     */
    "nextInterestPay": string;
    /**
     * @example "1646697600000"
     */
    "nextInterestPayDate": string;
    /**
     * @example "1"
     */
    "payInterestPeriod": string;
    /**
     * @example "2802.24068892"
     */
    "redeemAmountEarly": string;
    /**
     * @example "1651449600000"
     */
    "interestEndDate": string;
    /**
     * @example "1651536000000"
     */
    "deliverDate": string;
    /**
     * @example "1"
     */
    "redeemPeriod": string;
    /**
     * @example "232.2323"
     */
    "redeemingAmt": string;
    /**
     * @example "1651536000000"
     */
    "partialAmtDeliverDate": string;
    /**
     * @example true
     */
    "canRedeemEarly": boolean;
    /**
     * @example true
     */
    "renewable": boolean;
    /**
     * @example "AUTO"
     */
    "type": string;
    /**
     * @example "HOLDING"
     */
    "status": string;
}

interface GetSapiV1StakingStakingRecordParams {
    /**
     * * `STAKING` - for Locked Staking
     * * `F_DEFI` - for flexible DeFi Staking
     * * `L_DEFI` - for locked DeFi Staking
     */
    product: string;
    /**
     * `SUBSCRIPTION`, `REDEMPTION`, `INTEREST`
     */
    txnType: string;
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1StakingStakingRecordResponseItem {
    /**
     * @example "123123"
     */
    "positionId": string;
    /**
     * int64
     * @example 1575018510000
     */
    "time": number;
    /**
     * @example "BNB"
     */
    "asset": string;
    /**
     * @example "BSC"
     */
    "project": string;
    /**
     * @example "21312.23223"
     */
    "amount": string;
    /**
     * @example "30"
     */
    "lockPeriod": string;
    /**
     * @example "1575018510000"
     */
    "deliverDate": string;
    /**
     * @example "AUTO"
     */
    "type": string;
    /**
     * @example "success"
     */
    "status": string;
}

interface GetSapiV1StakingPersonalLeftQuotaParams {
    /**
     * * `STAKING` - for Locked Staking
     * * `F_DEFI` - for flexible DeFi Staking
     * * `L_DEFI` - for locked DeFi Staking
     */
    product: string;
    productId: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1StakingPersonalLeftQuotaResponseItem {
    /**
     * @example "1000"
     */
    "leftPersonalQuota": string;
}

interface GetSapiV1MiningPubAlgoListParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningPubAlgoListResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        /**
         * @example "sha256"
         */
        "algoName": string;
        /**
         * int64
         * @example 1
         */
        "algoId": number;
        /**
         * int64
         */
        "poolIndex": number;
        /**
         * @example "h/s"
         */
        "unit": string;
    }[];
}

interface GetSapiV1MiningPubCoinListParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningPubCoinListResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        /**
         * @example "BTC"
         */
        "coinName": string;
        /**
         * int64
         * @example 1
         */
        "coinId": number;
        /**
         * int64
         */
        "poolIndex": number;
        /**
         * int64
         * @example 1
         */
        "algoId": number;
        /**
         * @example "sha256"
         */
        "algoName": string;
    }[];
}

interface GetSapiV1MiningWorkerDetailParams {
    /**
     * Algorithm(sha256)
     */
    algo: string;
    /**
     * Mining Account
     */
    userName: string;
    /**
     * Miner’s name
     */
    workerName: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningWorkerDetailResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        /**
         * Mining Account name
         * @example "bhdc1.16A10404B"
         */
        "workerName": string;
        /**
         * Type of hourly hashrate
         * @example "H_hashrate"
         */
        "type": string;
        "hashrateDatas": {
            /**
             * int64
             * @example 1587902400000
             */
            "time": number;
            /**
             * @example "0"
             */
            "hashrate": string;
            /**
             * Rejection Rate
             * int64
             */
            "reject": number;
        }[];
    }[];
}

interface GetSapiV1MiningWorkerListParams {
    /**
     * Algorithm(sha256)
     */
    algo: string;
    /**
     * Mining Account
     */
    userName: string;
    /**
     * Page number, default is first page, start form 1
     * int32
     */
    pageIndex?: number;
    /**
     * sort sequence（default=0）0 positive sequence, 1 negative sequence
     * int32
     */
    sort?: number;
    /**
     * Sort by( default 1): 1: miner name, 2: real-time computing power, 3: daily average computing power, 4: real-time rejection rate, 5: last submission time
     * int32
     */
    sortColumn?: number;
    /**
     * miners status（default=0）0 all, 1 valid, 2 invalid, 3 failure
     * int32
     */
    workerStatus?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningWorkerListResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        "workerDatas": {
            /**
             * @example "1420554439452400131"
             */
            "workerId": string;
            /**
             * @example "2X73"
             */
            "workerName": string;
            /**
             * Status：1 valid, 2 invalid, 3 no longer valid
             * int64
             * @example 3
             */
            "status": number;
            /**
             * Real-time rate
             * int64
             */
            "hashRate": number;
            /**
             * 24H Hashrate
             * int64
             */
            "dayHashRate": number;
            /**
             * Real-time Rejection Rate
             * int64
             */
            "rejectRate": number;
            /**
             * Last submission time
             * int64
             * @example 1587712919000
             */
            "lastShareTime": number;
        }[];
        /**
         * int64
         * @example 18530
         */
        "totalNum": number;
        /**
         * int64
         * @example 20
         */
        "pageSize": number;
    };
}

interface GetSapiV1MiningPaymentListParams {
    /**
     * Algorithm(sha256)
     */
    algo: string;
    /**
     * Mining Account
     */
    userName: string;
    /**
     * Coin name
     * @example "BNB"
     */
    coin?: string;
    /**
     * Search date, millisecond timestamp, while empty query all
     */
    startDate?: string;
    /**
     * Search date, millisecond timestamp, while empty query all
     */
    endDate?: string;
    /**
     * Page number, default is first page, start form 1
     * int32
     */
    pageIndex?: number;
    /**
     * Number of pages, minimum 10, maximum 200
     */
    pageSize?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningPaymentListResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        "accountProfits": {
            /**
             * Mining date
             * int64
             * @example 1586188800000
             */
            "time": number;
            /**
             * 0:Mining Wallet,5:Mining Address,7:Pool Savings,8:Transferred,31:Income Transfer ,32:Hashrate Resale-Mining Wallet 33:Hashrate Resale-Pool Savings
             * int64
             * @example 31
             */
            "type": number;
            /**
             * Transferred Hashrate
             * nullable
             */
            "hashTransfer": number;
            /**
             * Transferred Income
             * nullable
             */
            "transferAmount": number;
            /**
             * Daily Hashrate
             * int64
             * @example 129129903378244
             */
            "dayHashRate": number;
            /**
             * Earnings Amount
             * double
             * @example 8.6083060304
             */
            "profitAmount": number;
            /**
             * Coin Type
             * @example "BTC"
             */
            "coinName": string;
            /**
             * Status：0:Unpaid, 1:Paying  2：Paid
             * int32
             * @example 2
             */
            "status": number;
        }[];
        /**
         * Total Rows
         * int64
         * @example 3
         */
        "totalNum": number;
        /**
         * Rows per page
         * int64
         * @example 20
         */
        "pageSize": number;
    };
}

interface GetSapiV1MiningPaymentOtherParams {
    /**
     * Algorithm(sha256)
     */
    algo: string;
    /**
     * Mining Account
     */
    userName: string;
    /**
     * Coin name
     * @example "BNB"
     */
    coin?: string;
    /**
     * Search date, millisecond timestamp, while empty query all
     */
    startDate?: string;
    /**
     * Search date, millisecond timestamp, while empty query all
     */
    endDate?: string;
    /**
     * Page number, default is first page, start form 1
     * int32
     */
    pageIndex?: number;
    /**
     * Number of pages, minimum 10, maximum 200
     */
    pageSize?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningPaymentOtherResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        "otherProfits": {
            /**
             * Mining date
             * int64
             * @example 1607443200000
             */
            "time": number;
            /**
             * Coin Name
             * @example "BTC"
             */
            "coinName": string;
            /**
             * 1: Merged Mining, 2: Activity Bonus, 3:Rebate 4:Smart Pool 6:Income Transfer 7:Pool Savings
             * int32
             * @example 4
             */
            "type": number;
            /**
             * double
             * @example 0.0011859
             */
            "profitAmount": number;
            /**
             * 0:Unpaid, 1:Paying  2：Paid
             * int32
             * @example 2
             */
            "status": number;
        }[];
        /**
         * Total Rows
         * int64
         * @example 3
         */
        "totalNum": number;
        /**
         * Rows per page
         * int64
         * @example 20
         */
        "pageSize": number;
    };
}

interface GetSapiV1MiningHashTransferConfigDetailsListParams {
    /**
     * Page number, default is first page, start form 1
     * int32
     */
    pageIndex?: number;
    /**
     * Number of pages, minimum 10, maximum 200
     */
    pageSize?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningHashTransferConfigDetailsListResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        "configDetails": {
            /**
             * Mining ID
             * int64
             * @example 168
             */
            "configId": number;
            /**
             * Transfer out of subaccount
             * @example "123"
             */
            "poolUsername": string;
            /**
             * Transfer into subaccount
             * @example "user1"
             */
            "toPoolUsername": string;
            /**
             * Transfer algorithm
             * @example "Ethash"
             */
            "algoName": string;
            /**
             * Transferred Hashrate quantity
             * int64
             * @example 5000000
             */
            "hashRate": number;
            /**
             * Start date
             * int64
             * @example 20201210
             */
            "startDay": number;
            /**
             * End date
             * int64
             * @example 20210405
             */
            "endDay": number;
            /**
             * 0 Processing, 1：Cancelled, 2：Terminated
             * int32
             * @example 1
             */
            "status": number;
        }[];
        /**
         * int64
         * @example 21
         */
        "totalNum": number;
        /**
         * int64
         * @example 200
         */
        "pageSize": number;
    };
}

interface GetSapiV1MiningHashTransferProfitDetailsParams {
    /**
     * Mining ID
     */
    configId: string;
    /**
     * Mining Account
     */
    userName: string;
    /**
     * Page number, default is first page, start form 1
     * int32
     */
    pageIndex?: number;
    /**
     * Number of pages, minimum 10, maximum 200
     */
    pageSize?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningHashTransferProfitDetailsResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        "profitTransferDetails": {
            /**
             * Transfer out of sub-account
             * @example "test4001"
             */
            "poolUsername": string;
            /**
             * Transfer into subaccount
             * @example "pop"
             */
            "toPoolUsername": string;
            /**
             * Transfer algorithm
             * @example "sha256"
             */
            "algoName": string;
            /**
             * Transferred Hashrate quantity
             * int64
             * @example 200000000000
             */
            "hashRate": number;
            /**
             * Transfer date
             * int64
             * @example 20201213
             */
            "day": number;
            /**
             * Transfer income
             * double
             * @example 0.2256872
             */
            "amount": number;
            /**
             * @example "BTC"
             */
            "coinName": string;
        }[];
        /**
         * int64
         * @example 8
         */
        "totalNum": number;
        /**
         * int64
         * @example 200
         */
        "pageSize": number;
    };
}

interface GetSapiV1MiningStatisticsUserStatusParams {
    /**
     * Algorithm(sha256)
     */
    algo: string;
    /**
     * Mining Account
     */
    userName: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningStatisticsUserStatusResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        /**
         * @example "457835490067496409.00000000"
         */
        "fifteenMinHashRate": string;
        /**
         * @example "214289268068874127.65000000"
         */
        "dayHashRate": string;
        /**
         * int64
         */
        "validNum": number;
        /**
         * int64
         * @example 17562
         */
        "invalidNum": number;
        "profitToday": {
            /**
             * @example "0.00314332"
             */
            "BTC": string;
            /**
             * @example "56.17055953"
             */
            "BSV": string;
            /**
             * @example "106.61586001"
             */
            "BCH": string;
        };
        "profitYesterday": {
            /**
             * @example "0.00314332"
             */
            "BTC": string;
            /**
             * @example "56.17055953"
             */
            "BSV": string;
            /**
             * @example "106.61586001"
             */
            "BCH": string;
        };
        /**
         * @example "test"
         */
        "userName": string;
        /**
         * @example "h/s"
         */
        "unit": string;
        /**
         * @example "sha256"
         */
        "algo": string;
    };
}

interface GetSapiV1MiningStatisticsUserListParams {
    /**
     * Algorithm(sha256)
     */
    algo: string;
    /**
     * Mining Account
     */
    userName: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningStatisticsUserListResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": {
        /**
         * @example "H_hashrate"
         */
        "type": string;
        /**
         * @example "test"
         */
        "userName": string;
        "list": {
            /**
             * int64
             * @example 1585267200000
             */
            "time": number;
            /**
             * @example "0.00000000"
             */
            "hashrate": string;
            /**
             * @example "0.00000000"
             */
            "reject": string;
        }[];
    }[];
}

interface GetSapiV1MiningPaymentUidParams {
    /**
     * Algorithm(sha256)
     */
    algo: string;
    /**
     * Search date, millisecond timestamp, while empty query all
     */
    startDate?: string;
    /**
     * Search date, millisecond timestamp, while empty query all
     */
    endDate?: string;
    /**
     * Page number, default is first page, start form 1
     * int32
     */
    pageIndex?: number;
    /**
     * Number of pages, minimum 10, maximum 200
     */
    pageSize?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1MiningPaymentUidResponse {
    /**
     * int32
     */
    "code": number;
    "msg": string;
    "data": {
        "accountProfits": {
            /**
             * int64
             * @example 1607443200000
             */
            "time": number;
            /**
             * @example "BTC"
             */
            "coinName": string;
            /**
             * 0:Referral 1:Refund 2:Rebate
             * int32
             * @example 2
             */
            "type": number;
            /**
             * puid
             * int32
             * @example 59985472
             */
            "puid": number;
            /**
             * Mining account
             * @example "vdvaghani"
             */
            "subName": string;
            /**
             * @example 0.09186957
             */
            "amount": number;
        }[];
        /**
         * int32
         * @example 3
         */
        "totalNum": number;
        /**
         * int32
         * @example 20
         */
        "pageSize": number;
    };
}

interface GetSapiV1FuturesTransferParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1FuturesTransferResponse {
    "rows": {
        /**
         * @example "USDT"
         */
        "asset": string;
        /**
         * int64
         * @example 100000001
         */
        "tranId": number;
        /**
         * @example "40.84624400"
         */
        "amount": string;
        /**
         * @example "1"
         */
        "type": string;
        /**
         * int64
         * @example 1555056425000
         */
        "timestamp": number;
        /**
         * one of PENDING (pending to execution), CONFIRMED (successfully transfered), FAILED (execution failed, nothing happened to your account);
         * @example "CONFIRMED"
         */
        "status": string;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1FuturesLoanBorrowHistoryParams {
    /**
     * Coin name
     * @example "BNB"
     */
    coin?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1FuturesLoanBorrowHistoryResponse {
    "rows": {
        /**
         * int64
         * @example 1582540328433
         */
        "confirmedTime": number;
        /**
         * @example "USDT"
         */
        "coin": string;
        /**
         * @example "0.89991001"
         */
        "collateralRate": string;
        /**
         * @example "4.5"
         */
        "leftTotal": string;
        /**
         * @example "4.5"
         */
        "leftPrincipal": string;
        /**
         * int64
         * @example 4736102399000
         */
        "deadline": number;
        /**
         * @example "BUSD"
         */
        "collateralCoin": string;
        /**
         * @example "5.0"
         */
        "collateralAmount": string;
        /**
         * @example "PENDING"
         */
        "orderStatus": string;
        /**
         * @example "438648398970089472"
         */
        "borrowId": string;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1FuturesLoanRepayHistoryParams {
    /**
     * Coin name
     * @example "BNB"
     */
    coin?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1FuturesLoanRepayHistoryResponse {
    "rows": {
        /**
         * @example "USDT"
         */
        "coin": string;
        /**
         * @example "1.68"
         */
        "amount": string;
        /**
         * @example "BUSD"
         */
        "collateralCoin": string;
        /**
         * @example "NORMAL"
         */
        "repayType": string;
        /**
         * @example "1.80288889"
         */
        "releasedCollateral": string;
        /**
         * @example "1.001"
         */
        "price": string;
        /**
         * @example "10010"
         */
        "repayCollateral": string;
        /**
         * int64
         * @example 1582781327575
         */
        "confirmedTime": number;
        /**
         * int64
         * @example 1582794387516
         */
        "updateTime": number;
        /**
         * @example "PENDING"
         */
        "status": string;
        /**
         * @example "439659223998894080"
         */
        "repayId": string;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV2FuturesLoanWalletParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV2FuturesLoanWalletResponse {
    /**
     * @example "5.8238577133"
     */
    "totalCrossCollateral": string;
    /**
     * @example "5.07000000"
     */
    "totalBorrowed": string;
    /**
     * @example "0.0"
     */
    "totalInterest": string;
    /**
     * @example "100000"
     */
    "interestFreeLimit": string;
    /**
     * @example "USD"
     */
    "asset": string;
    "crossCollaterals": {
        /**
         * @example "USDT"
         */
        "loanCoin": string;
        /**
         * @example "BUSD"
         */
        "collateralCoin": string;
        /**
         * @example "5.82211108"
         */
        "locked": string;
        /**
         * @example "5.07"
         */
        "loanAmount": string;
        /**
         * @example "0.87168984"
         */
        "currentCollateralRate": string;
        /**
         * @example "5.07"
         */
        "interestFreeLimitUsed": string;
        /**
         * @example "0.0"
         */
        "principalForInterest": string;
        /**
         * @example "0.0"
         */
        "interest": string;
    }[];
}

interface GetSapiV1FuturesLoanAdjustCollateralHistoryParams {
    /**
     * Coin loaned
     * @example "BUSD"
     */
    loanCoin?: string;
    /**
     * Coin used as collateral
     * @example "BNB"
     */
    collateralCoin?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1FuturesLoanAdjustCollateralHistoryResponse {
    "rows": {
        /**
         * @example ".17398184"
         */
        "amount": string;
        /**
         * @example "BUSD"
         */
        "collateralCoin": string;
        /**
         * @example "USDT"
         */
        "coin": string;
        /**
         * @example "0.87054861"
         */
        "preCollateralRate"?: string;
        /**
         * @example "0.89736451"
         */
        "afterCollateralRate"?: string;
        /**
         * @example "REDUCED"
         */
        "direction"?: string;
        /**
         * @example "COMPLETED"
         */
        "status": string;
        /**
         * int64
         * @example 1583978243588
         */
        "adjustTime"?: number;
    }[];
    /**
     * int64
     * @example 1
     */
    "total": number;
}

interface GetSapiV1FuturesLoanLiquidationHistoryParams {
    /**
     * Coin loaned
     * @example "BUSD"
     */
    loanCoin?: string;
    /**
     * Coin used as collateral
     * @example "BNB"
     */
    collateralCoin?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1FuturesLoanLiquidationHistoryResponse {
    "rows": {
        /**
         * @example "10.12345678"
         */
        "collateralAmountForLiquidation": string;
        /**
         * @example "BUSD"
         */
        "collateralCoin": string;
        /**
         * int64
         * @example 1583978243588
         */
        "forceLiquidationStartTime": number;
        /**
         * @example "USDT"
         */
        "coin": string;
        /**
         * @example "15.12345678"
         */
        "restCollateralAmountAfterLiquidation": string;
        /**
         * @example "11.12345678"
         */
        "restLoanAmount": string;
        /**
         * @example "PENDING"
         */
        "status": string;
    }[];
    /**
     * int64
     * @example 1
     */
    "total": number;
}

interface GetSapiV1FuturesLoanInterestHistoryParams {
    /**
     * Coin used as collateral
     * @example "BNB"
     */
    collateralCoin?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1FuturesLoanInterestHistoryResponse {
    "rows": {
        /**
         * @example "BUSD"
         */
        "collateralCoin": string;
        /**
         * @example "USDT"
         */
        "interestCoin": string;
        /**
         * @example "2.354"
         */
        "interest": string;
        /**
         * @example "0"
         */
        "interestFreeLimitUsed": string;
        /**
         * @example "10000"
         */
        "principalForInterest": string;
        /**
         * @example "0.002"
         */
        "interestRate": string;
        /**
         * int64
         * @example 1582794387516
         */
        "time": number;
    }[];
    /**
     * int64
     * @example 1
     */
    "total": number;
}

interface GetSapiV1AlgoFuturesOpenOrdersParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AlgoFuturesOpenOrdersResponse {
    /**
     * int32
     * @example 1
     */
    "total": number;
    "orders"?: {
        /**
         * int64
         * @example 14517
         */
        "algoId": number;
        /**
         * @example "ETHUSDT"
         */
        "symbol": string;
        /**
         * @example "SELL"
         */
        "side": string;
        /**
         * @example "SHORT"
         */
        "positionSide": string;
        /**
         * @example "5.00"
         */
        "totalQty": string;
        /**
         * @example "0.000"
         */
        "executedQty": string;
        /**
         * @example "0.00000000"
         */
        "executedAmt": string;
        /**
         * @example "0.00"
         */
        "avgPrice": string;
        /**
         * @example "d7096549481642f8a0bb69e9e2e31f2e"
         */
        "clientAlgoId": string;
        /**
         * int64
         * @example 1649756817004
         */
        "bookTime": number;
        /**
         * int64
         */
        "endTime": number;
        /**
         * @example "WORKING"
         */
        "algoStatus": string;
        /**
         * @example "VP"
         */
        "algoType": string;
        /**
         * @example "LOW"
         */
        "urgency": string;
    }[];
}

interface GetSapiV1AlgoFuturesHistoricalOrdersParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * @example "SELL"
     */
    side?: "SELL" | "BUY";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Page size, minimum 1, maximum 100, default 100
     */
    pageSize?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AlgoFuturesHistoricalOrdersResponse {
    /**
     * int32
     * @example 1
     */
    "total": number;
    "orders": {
        /**
         * int64
         * @example 14517
         */
        "algoId": number;
        /**
         * @example "ETHUSDT"
         */
        "symbol": string;
        /**
         * @example "SELL"
         */
        "side": string;
        /**
         * @example "SHORT"
         */
        "positionSide": string;
        /**
         * @example "5.00"
         */
        "totalQty": string;
        /**
         * @example "0.000"
         */
        "executedQty": string;
        /**
         * @example "0.00000000"
         */
        "executedAmt": string;
        /**
         * @example "0.00"
         */
        "avgPrice": string;
        /**
         * @example "d7096549481642f8a0bb69e9e2e31f2e"
         */
        "clientAlgoId": string;
        /**
         * int64
         * @example 1649756817004
         */
        "bookTime": number;
        /**
         * int64
         * @example 1649757088101
         */
        "endTime": number;
        /**
         * @example "WORKING"
         */
        "algoStatus": string;
        /**
         * @example "VP"
         */
        "algoType": string;
        /**
         * @example "LOW"
         */
        "urgency": string;
    }[];
}

interface GetSapiV1AlgoFuturesSubOrdersParams {
    /**
     * int64
     */
    algoId: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * Page size, minimum 1, maximum 100, default 100
     */
    pageSize?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1AlgoFuturesSubOrdersResponse {
    /**
     * int32
     * @example 1
     */
    "total": number;
    /**
     * @example "1.000"
     */
    "executedQty": string;
    /**
     * @example "3229.44000000"
     */
    "executedAmt": string;
    "subOrders": {
        /**
         * int64
         * @example 13723
         */
        "algoId": number;
        /**
         * int64
         * @example 8389765519993909000
         */
        "orderId": number;
        /**
         * @example "FILLED"
         */
        "orderStatus": string;
        /**
         * @example "1.000"
         */
        "executedQty"?: string;
        /**
         * @example "3229.44000000"
         */
        "executedAmt": string;
        /**
         * @example "-1.61471999"
         */
        "feeAmt": string;
        /**
         * @example "USDT"
         */
        "feeAsset": string;
        /**
         * int64
         * @example 1649756817004
         */
        "bookTime": number;
        /**
         * @example "3229.44"
         */
        "avgPrice": string;
        /**
         * @example "SELL"
         */
        "side": string;
        /**
         * @example "ETHUSDT"
         */
        "symbol": string;
        /**
         * int64
         * @example 1
         */
        "subId": number;
        /**
         * @example "IMMEDIATE_OR_CANCEL"
         */
        "timeInForce": string;
        /**
         * @example "1.000"
         */
        "origQty": string;
    }[];
}

interface GetSapiV1PortfolioAccountParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1PortfolioAccountResponse {
    /**
     * @example "1.87987800"
     */
    "uniMMR": string;
    /**
     * @example "122607.35137903"
     */
    "accountEquity": string;
    /**
     * @example "23.72469206"
     */
    "accountMaintMargin": string;
    /**
     * @example "NORMAL"
     */
    "accountStatus": string;
}

interface GetSapiV1PortfolioCollateralRateResponseItem {
    /**
     * @example "USDC"
     */
    "asset": string;
    /**
     * @example "1.0000"
     */
    "collateralRate": string;
}

interface GetSapiV1PortfolioPmLoanParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1PortfolioPmLoanResponse {
    /**
     * @example "BUSD"
     */
    "asset": string;
    /**
     * @example "579.45"
     */
    "amount": string;
}

interface GetSapiV1BlvtTokenInfoParams {
    /**
     * BTCDOWN, BTCUP
     */
    tokenName?: string;
}

interface GetSapiV1BlvtTokenInfoResponseItem {
    /**
     * @example "BTCDOWN"
     */
    "tokenName": string;
    /**
     * @example "3X Short Bitcoin Token"
     */
    "description": string;
    /**
     * @example "BTC"
     */
    "underlying": string;
    /**
     * @example "717953.95"
     */
    "tokenIssued": string;
    /**
     * @example "-821.474 BTCUSDT Futures"
     */
    "basket": string;
    "currentBaskets": {
        /**
         * @example "BTCUSDT"
         */
        "symbol": string;
        /**
         * @example "-1183.984"
         */
        "amount": string;
        /**
         * @example "-22871089.96704"
         */
        "notionalValue": string;
    }[];
    /**
     * @example "4.79"
     */
    "nav": string;
    /**
     * @example "-2.316"
     */
    "realLeverage": string;
    /**
     * @example "0.001020"
     */
    "fundingRate": string;
    /**
     * @example "0.0001"
     */
    "dailyManagementFee": string;
    /**
     * @example "0.0010"
     */
    "purchaseFeePct": string;
    /**
     * @example "100000.00"
     */
    "dailyPurchaseLimit": string;
    /**
     * @example "0.0010"
     */
    "redeemFeePct": string;
    /**
     * @example "1000000.00"
     */
    "dailyRedeemLimit": string;
    /**
     * int64
     * @example 1583127900000
     */
    "timestamp": number;
}

interface GetSapiV1BlvtSubscribeRecordParams {
    /**
     * BTCDOWN, BTCUP
     */
    tokenName?: string;
    /**
     * int64
     */
    id?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BlvtSubscribeRecordResponse {
    /**
     * double
     * @example 1
     */
    "id": number;
    /**
     * @example "LINKUP"
     */
    "tokenName": string;
    /**
     * Subscription amount
     * @example "0.54216292"
     */
    "amount": string;
    /**
     * NAV price of subscription
     * @example "18.42621386"
     */
    "nav": string;
    /**
     * Subscription fee in usdt
     * @example "0.00999000"
     */
    "fee": string;
    /**
     * Subscription cost in usdt
     * @example "9.99999991"
     */
    "totalCharge": string;
    /**
     * int64
     * @example 1599127217916
     */
    "timestamp": number;
}

interface GetSapiV1BlvtRedeemRecordParams {
    /**
     * BTCDOWN, BTCUP
     */
    tokenName?: string;
    /**
     * int64
     */
    id?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * default 1000, max 1000
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BlvtRedeemRecordResponseItem {
    /**
     * double
     * @example 1
     */
    "id": number;
    /**
     * @example "LINKUP"
     */
    "tokenName": string;
    /**
     * Redemption amount
     * @example "0.54216292"
     */
    "amount": string;
    /**
     * NAV of redemption
     * @example "18.36345064"
     */
    "nav": string;
    /**
     * Reemption fee
     * @example "0.00995598"
     */
    "fee": string;
    /**
     * Net redemption value in usdt
     * @example "9.94602604"
     */
    "netProceed": string;
    /**
     * int64
     * @example 1599128003050
     */
    "timestamp": number;
}

interface GetSapiV1BlvtUserLimitParams {
    /**
     * BTCDOWN, BTCUP
     */
    tokenName?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BlvtUserLimitResponseItem {
    /**
     * @example "LINKUP"
     */
    "tokenName": string;
    /**
     * USDT
     * @example "1000"
     */
    "userDailyTotalPurchaseLimit": string;
    /**
     * USDT
     * @example "1000"
     */
    "userDailyTotalRedeemLimit": string;
}

interface GetSapiV1BswapPoolsResponseItem {
    /**
     * int64
     * @example 2
     */
    "poolId": number;
    /**
     * @example "BUSD/USDT"
     */
    "poolName": string;
    /**
     * @example ["BUSD","USDT"]
     */
    "assets": string[];
}

interface GetSapiV1BswapLiquidityParams {
    /**
     * int64
     */
    poolId?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapLiquidityResponseItem {
    /**
     * int64
     * @example 2
     */
    "poolId": number;
    /**
     * @example "BUSD/USDT"
     */
    "poolNmae": string;
    /**
     * int64
     * @example 1565769342148
     */
    "updateTime": number;
    "liquidity": {
        /**
         * double
         * @example 100000315.79
         */
        "BUSD": number;
        /**
         * double
         * @example 99999245.54
         */
        "USDT": number;
    };
    "share": {
        /**
         * double
         * @example 12415
         */
        "shareAmount": number;
        /**
         * double
         * @example 0.00006207
         */
        "sharePercentage": number;
        "asset": {
            /**
             * double
             * @example 6207.02
             */
            "BUSD": number;
            /**
             * double
             * @example 6206.95
             */
            "USDT": number;
        };
    };
}

interface GetSapiV1BswapLiquidityOpsParams {
    /**
     * int64
     */
    operationId?: number;
    /**
     * int64
     */
    poolId?: number;
    operation?: "ADD" | "REMOVE";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 500; max 1000.
     * int32
     * @example 500
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapLiquidityOpsResponseItem {
    /**
     * int64
     * @example 12341
     */
    "operationId": number;
    /**
     * int64
     * @example 2
     */
    "poolId": number;
    /**
     * @example "BUSD/USDT"
     */
    "poolName": string;
    /**
     * "ADD" or "REMOVE"
     * @example "ADD"
     */
    "operation": string;
    /**
     * 0: pending, 1: success, 2: failed
     * int32
     * @example 1
     */
    "status": number;
    /**
     * int64
     * @example 1565769342148
     */
    "updateTime": number;
    /**
     * @example "10.1"
     */
    "shareAmount": string;
}

interface GetSapiV1BswapQuoteParams {
    /**
     * @example "USDT"
     */
    quoteAsset: string;
    /**
     * @example "BUSD"
     */
    baseAsset: string;
    /**
     * double
     */
    quoteQty: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapQuoteResponse {
    /**
     * @example "USDT"
     */
    "quoteAsset": string;
    /**
     * @example "BUSD"
     */
    "baseAsset": string;
    /**
     * double
     * @example 300000
     */
    "quoteQty": number;
    /**
     * double
     * @example 299975
     */
    "baseQty": number;
    /**
     * double
     * @example 1.00008334
     */
    "price": number;
    /**
     * double
     * @example 0.00007245
     */
    "slippage": number;
    /**
     * double
     * @example 120
     */
    "fee": number;
}

interface GetSapiV1BswapSwapParams {
    /**
     * int64
     */
    swapId?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * * `0` - pending for swap
     * * `1` - success
     * * `2` - failed
     * int32
     */
    status?: 0 | 1 | 2;
    /**
     * @example "USDT"
     */
    quoteAsset?: string;
    /**
     * @example "BUSD"
     */
    baseAsset?: string;
    /**
     * default 3, max 100
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapSwapResponseItem {
    /**
     * int64
     * @example 2314
     */
    "swapId": number;
    /**
     * int64
     * @example 1565770342148
     */
    "swapTime": number;
    /**
     * 0: pending, 1: success, 2: failed
     * int32
     */
    "status": number;
    /**
     * @example "USDT"
     */
    "quoteAsset": string;
    /**
     * @example "BUSD"
     */
    "baseAsset": string;
    /**
     * double
     * @example 300000
     */
    "quoteQty": number;
    /**
     * double
     * @example 299975
     */
    "baseQty": number;
    /**
     * double
     * @example 1.00008334
     */
    "price": number;
    /**
     * double
     * @example 120
     */
    "fee": number;
}

interface GetSapiV1BswapPoolConfigureParams {
    /**
     * int64
     * @example 2
     */
    poolId?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapPoolConfigureResponseItem {
    /**
     * int64
     * @example 2
     */
    "poolId": number;
    /**
     * @example "BUSD/USDT"
     */
    "poolNmae": string;
    /**
     * int64
     * @example 1565769342148
     */
    "updateTime": number;
    "liquidity": {
        /**
         * "NA" if pool is an innovation pool
         * int64
         * @example 2000
         */
        "constantA": number;
        /**
         * double
         * @example 0.1
         */
        "minRedeemShare": number;
        /**
         * The swap proceeds only when the slippage is within the set range
         * double
         * @example 0.2
         */
        "slippageTolerance": number;
    };
    "assetConfigure": {
        "BUSD": {
            /**
             * int64
             * @example 10
             */
            "minAdd": number;
            /**
             * int64
             * @example 20
             */
            "maxAdd": number;
            /**
             * int64
             * @example 10
             */
            "minSwap": number;
            /**
             * int64
             * @example 30
             */
            "maxSwap": number;
        };
        "USDT": {
            /**
             * int64
             * @example 10
             */
            "minAdd": number;
            /**
             * int64
             * @example 20
             */
            "maxAdd": number;
            /**
             * int64
             * @example 10
             */
            "minSwap": number;
            /**
             * int64
             * @example 30
             */
            "maxSwap": number;
        };
    };
}

interface GetSapiV1BswapAddLiquidityPreviewParams {
    /**
     * int64
     * @example 2
     */
    poolId: number;
    /**
     * * `SINGLE` - for adding a single token
     * * `COMBINATION` - for adding dual tokens
     * @example "SINGLE"
     */
    type: "SINGLE" | "COMBINATION";
    /**
     * @example "USDT"
     */
    quoteAsset: string;
    /**
     * double
     */
    quoteQty: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapRemoveLiquidityPreviewParams {
    /**
     * int64
     * @example 2
     */
    poolId: number;
    /**
     * * `SINGLE` - remove and obtain a single token
     * * `COMBINATION` - remove and obtain dual token
     * @example "SINGLE"
     */
    type: "SINGLE" | "COMBINATION";
    /**
     * @example "USDT"
     */
    quoteAsset: string;
    /**
     * double
     */
    shareAmount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapUnclaimedRewardsParams {
    /**
     * 0: Swap rewards, 1: Liquidity rewards, default to 0
     * int32
     */
    type?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapUnclaimedRewardsResponse {
    "totalUnclaimedRewards": {
        /**
         * float
         * @example 100000315.79
         */
        "BUSD": number;
        /**
         * double
         * @example 1e-8
         */
        "BNB": number;
        /**
         * double
         * @example 2e-8
         */
        "USDT": number;
    };
    "details": {
        "BNB/USDT": {
            /**
             * float
             * @example 100000315.79
             */
            "BUSD": number;
            /**
             * double
             * @example 2e-8
             */
            "USDT": number;
        };
        "BNB/BTC": {
            /**
             * double
             * @example 1e-8
             */
            "BNB": number;
        };
    };
}

interface GetSapiV1BswapClaimedHistoryParams {
    /**
     * int64
     */
    poolId?: number;
    assetRewards?: string;
    /**
     * 0: Swap rewards, 1: Liquidity rewards, default to 0
     * int32
     */
    type?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 3, max 100
     * int32
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1BswapClaimedHistoryResponseItem {
    /**
     * int32
     * @example 52
     */
    "poolId": number;
    /**
     * @example "BNB/USDT"
     */
    "poolName": string;
    /**
     * @example "BNB"
     */
    "assetRewards": string;
    /**
     * int64
     * @example 1565769342148
     */
    "claimTime": number;
    /**
     * float
     * @example 2.3e-7
     */
    "claimAmount": number;
    /**
     * 0: pending, 1: success
     * int32
     * @example 1
     */
    "status": number;
}

interface GetSapiV1C2COrderMatchListUserOrderHistoryParams {
    tradeType: "BUY" | "SELL";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTimestamp?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTimestamp?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * default 100, max 100
     * int32
     */
    rows?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1C2COrderMatchListUserOrderHistoryResponse {
    /**
     * @example "000000"
     */
    "code": string;
    /**
     * @example "success"
     */
    "message": string;
    "data": {
        /**
         * @example "20219644646554779648"
         */
        "orderNumber": string;
        /**
         * @example "11218246497340923904"
         */
        "advNo": string;
        /**
         * @example "SELL"
         */
        "tradeType": string;
        /**
         * @example "BUSD"
         */
        "asset": string;
        /**
         * @example "CNY"
         */
        "fiat": string;
        /**
         * @example "￥"
         */
        "fiatSymbol": string;
        /**
         * Quantity (in Crypto)
         * @example "5000.00000000"
         */
        "amount": string;
        /**
         * @example "33400.00000000"
         */
        "totalPrice": string;
        /**
         * Unit Price (in Fiat)
         * @example "6.68"
         */
        "unitPrice": string;
        /**
         * PENDING, TRADING, BUYER_PAYED, DISTRIBUTING, COMPLETED, IN_APPEAL, CANCELLED, CANCELLED_BY_SYSTEM
         * @example "COMPLETED"
         */
        "orderStatus": string;
        /**
         * int64
         * @example 1619361369000
         */
        "createTime": number;
        /**
         * Transaction Fee (in Crypto)
         * @example "0"
         */
        "commission": string;
        /**
         * @example "ab***"
         */
        "counterPartNickName": string;
        /**
         * @example "TAKER"
         */
        "advertisementRole": string;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
    "success": boolean;
}

interface GetSapiV1LoanIncomeParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    /**
     * All types will be returned by default.
     * * `borrowIn`
     * * `collateralSpent`
     * * `repayAmount`
     * * `collateralReturn` - Collateral return after repayment
     * * `addCollateral`
     * * `removeCollateral`
     * * `collateralReturnAfterLiquidation`
     */
    type?: "borrowIn" | "collateralSpent" | "repayAmount" | "collateralReturn" | "addCollateral" | "removeCollateral" | "collateralReturnAfterLiquidation";
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * default 20, max 100
     * int32
     * @example 20
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LoanIncomeResponseItem {
    /**
     * @example "BUSD"
     */
    "asset": string;
    /**
     * @example "borrowIn"
     */
    "type": string;
    /**
     * @example "100"
     */
    "amount": string;
    /**
     * int64
     * @example 1633771139847
     */
    "timestamp": number;
    /**
     * @example "80423589583"
     */
    "tranId": string;
}

interface GetSapiV1LoanBorrowHistoryParams {
    /**
     * orderId in POST /sapi/v1/loan/borrow
     * int64
     * @example 10
     */
    orderId?: number;
    /**
     * Coin loaned
     * @example "BUSD"
     */
    loanCoin?: string;
    /**
     * Coin used as collateral
     * @example "BNB"
     */
    collateralCoin?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * default 10, max 100
     * int64
     * @example 10
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LoanBorrowHistoryResponse {
    "rows": {
        /**
         * int64
         * @example 100000001
         */
        "orderId": number;
        /**
         * @example "BUSD"
         */
        "loanCoin": string;
        /**
         * @example "10000"
         */
        "initialLoanAmount": string;
        /**
         * @example "0.000057"
         */
        "hourlyInterestRate": string;
        /**
         * @example "7"
         */
        "loanTerm": string;
        /**
         * @example "BNB"
         */
        "collateralCoin": string;
        /**
         * @example "49.27565492"
         */
        "initialCollateralAmount": string;
        /**
         * int64
         * @example 1575018510000
         */
        "borrowTime": number;
        /**
         * @example "Accruing_Interest, Overdue, Liquidating, Repaying, Repaid, Liquidated, Pending, Failed"
         */
        "status": string;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1LoanOngoingOrdersParams {
    /**
     * orderId in POST /sapi/v1/loan/borrow
     * int64
     * @example 10
     */
    orderId?: number;
    /**
     * Coin loaned
     * @example "BUSD"
     */
    loanCoin?: string;
    /**
     * Coin used as collateral
     * @example "BNB"
     */
    collateralCoin?: string;
    /**
     * Current querying page. Start from 1; default:1, max:1000
     * int32
     * @example 1
     */
    current?: number;
    /**
     * default 10, max 100
     * int64
     * @example 10
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LoanOngoingOrdersResponse {
    "rows": {
        /**
         * int64
         * @example 100000001
         */
        "orderId": number;
        /**
         * @example "BUSD"
         */
        "loanCoin": string;
        /**
         * @example "10000"
         */
        "totalDebt": string;
        /**
         * @example "10.27687923"
         */
        "residualInterest": string;
        /**
         * @example "BNB"
         */
        "collateralCoin": string;
        /**
         * @example "49.27565492"
         */
        "collateralAmount": string;
        /**
         * @example "0.57"
         */
        "currentLTV": string;
        /**
         * int64
         * @example 1575018510000
         */
        "expirationTime": number;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1LoanRepayHistoryParams {
    /**
     * Order ID
     * int64
     * @example 10
     */
    orderId?: number;
    /**
     * Coin loaned
     * @example "BUSD"
     */
    loanCoin?: string;
    /**
     * Coin used as collateral
     * @example "BNB"
     */
    collateralCoin?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * default 10, max 100
     * int64
     * @example 10
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LoanRepayHistoryResponse {
    "rows": {
        /**
         * @example "BUSD"
         */
        "loanCoin": string;
        /**
         * @example "10000"
         */
        "repayAmount": string;
        /**
         * @example "BNB"
         */
        "collateralCoin": string;
        /**
         * @example "0"
         */
        "collateralUsed": string;
        /**
         * @example "49.27565492"
         */
        "collateralReturn": string;
        /**
         * @example "1"
         */
        "repayType": string;
        /**
         * 'repayType': '1' // 1 for 'repay with borrowed coin', 2 for 'repay with collateral' 'repayStatus': 'Repaid' // Repaid, Repaying, Failed
         * @example "Repaid"
         */
        "repayStatus": string;
        /**
         * int64
         * @example 1575018510000
         */
        "repayTime": number;
        /**
         * int64
         * @example 756783308056935400
         */
        "orderId": number;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1LoanLtvAdjustmentHistoryParams {
    /**
     * Order ID
     * int64
     * @example 10
     */
    orderId?: number;
    /**
     * Coin loaned
     * @example "BUSD"
     */
    loanCoin?: string;
    /**
     * Coin used as collateral
     * @example "BNB"
     */
    collateralCoin?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * default 10, max 100
     * int64
     * @example 10
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1LoanLtvAdjustmentHistoryResponse {
    "rows": {
        /**
         * @example "BUSD"
         */
        "loanCoin": string;
        /**
         * @example "BNB"
         */
        "collateralCoin": string;
        /**
         * @example "ADDITIONAL"
         */
        "direction": string;
        /**
         * @example "5.235"
         */
        "amount": string;
        /**
         * @example "0.78"
         */
        "preLTV": string;
        /**
         * @example "0.56"
         */
        "afterLTV": string;
        /**
         * int64
         * @example 1575018510000
         */
        "adjustTime": number;
        /**
         * int64
         * @example 756783308056935400
         */
        "orderId": number;
    }[];
    /**
     * int32
     * @example 1
     */
    "total": number;
}

interface GetSapiV1PayTransactionsParams {
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * default 100, max 100
     * int32
     * @example 100
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1PayTransactionsResponse {
    /**
     * @example "000000"
     */
    "code": string;
    /**
     * @example "success"
     */
    "message": string;
    "data": {
        /**
         * Enum：PAY(C2B Merchant Acquiring Payment), PAY_REFUND(C2B Merchant Acquiring Payment,refund), C2C(C2C Transfer Payment),CRYPTO_BOX(Crypto box), CRYPTO_BOX_RF(Crypto Box, refund), C2C_HOLDING(Transfer to new Binance user), C2C_HOLDING_RF(Transfer to new Binance user,refund), PAYOUT(B2C Disbursement Payment)
         * @example "C2C"
         */
        "orderType": string;
        /**
         * @example "M_P_71505104267788288"
         */
        "transactionId": string;
        /**
         * int64
         * @example 1610090460133
         */
        "transactionTime": number;
        /**
         * order amount(up to 8 decimal places), positive is income, negative is expenditure
         * @example "23.72469206"
         */
        "amount": string;
        /**
         * @example "BNB"
         */
        "currency": string;
        "fundsDetail": {
            "currency": string;
            "amount": string;
        }[];
    }[];
    "success": boolean;
}

interface GetSapiV1ConvertTradeFlowParams {
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime: number;
    /**
     * default 100, max 1000
     * int32
     * @example 100
     */
    limit?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1ConvertTradeFlowResponse {
    "list": {
        /**
         * @example "f3b91c525b2644c7bc1e1cd31b6e1aa6"
         */
        "quoteId": string;
        /**
         * int64
         * @example 940708407462087200
         */
        "orderId": number;
        /**
         * @example "SUCCESS"
         */
        "orderStatus": string;
        /**
         * @example "USDT"
         */
        "fromAsset": string;
        /**
         * @example "20"
         */
        "fromAmount": string;
        /**
         * @example "BNB"
         */
        "toAsset": string;
        /**
         * @example "0.06154036"
         */
        "toAmount": string;
        /**
         * price ratio
         * @example "0.00307702"
         */
        "ratio": string;
        /**
         * inverse price
         * @example "324.99"
         */
        "inverseRatio": string;
        /**
         * int64
         * @example 1624248872184
         */
        "createTime": number;
    }[];
    /**
     * int64
     * @example 1623824139000
     */
    "startTime": number;
    /**
     * int64
     * @example 1626416139000
     */
    "endTime": number;
    /**
     * int32
     * @example 100
     */
    "limit": number;
    "moreData": boolean;
}

interface GetSapiV1RebateTaxQueryParams {
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1RebateTaxQueryResponse {
    /**
     * @example "OK"
     */
    "status": string;
    /**
     * @example "GENERAL"
     */
    "type": string;
    /**
     * @example "000000000"
     */
    "code": string;
    "data": {
        /**
         * int32
         * @example 1
         */
        "page": number;
        /**
         * int32
         * @example 2
         */
        "totalRecords": number;
        /**
         * int32
         * @example 1
         */
        "totalPageNum": number;
        "data": {
            /**
             * @example "USDT"
             */
            "asset": string;
            /**
             * rebate type：1 is commission rebate，2 is referral kickback
             * int32
             * @example 1
             */
            "type": number;
            /**
             * @example "0.0001126"
             */
            "amount": string;
            /**
             * int64
             * @example 1637651320000
             */
            "updateTime": number;
        }[];
    };
}

interface GetSapiV1NftHistoryTransactionsParams {
    /**
     * 0: purchase order, 1: sell order, 2: royalty income, 3: primary market order, 4: mint fee
     * int32
     * @example 1
     */
    orderType: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 50, Max 50
     * int32
     * @example 50
     */
    limit?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1NftHistoryTransactionsResponse {
    /**
     * int32
     * @example 1
     */
    "total": number;
    "list": {
        /**
         * 0: purchase order, 1: sell order, 2: royalty income, 3: primary market order, 4: mint fee
         * @example "1_470502070600699904"
         */
        "orderNo": string;
        "tokens": {
            /**
             * @example "BSC"
             */
            "network": string;
            /**
             * @example "216000000496"
             */
            "tokenId": string;
            /**
             * @example "MYSTERY_BOX0000087"
             */
            "contractAddress": string;
        }[];
        /**
         * int64
         * @example 1626941236000
         */
        "tradeTime": number;
        /**
         * @example "19.60000000"
         */
        "tradeAmount": string;
        /**
         * @example "BNB"
         */
        "tradeCurrency": string;
    }[];
}

interface GetSapiV1NftHistoryDepositParams {
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 50, Max 50
     * int32
     * @example 50
     */
    limit?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1NftHistoryDepositResponse {
    /**
     * int32
     * @example 1
     */
    "total": number;
    "list": {
        /**
         * @example "ETH"
         */
        "network": string;
        "txID": number;
        /**
         * @example "0xe507c961ee127d4439977a61af39c34eafee0dc6"
         */
        "contractAdrress": string;
        /**
         * @example "10014"
         */
        "tokenId": string;
        /**
         * int64
         * @example 1629986047000
         */
        "timestamp": number;
    }[];
}

interface GetSapiV1NftHistoryWithdrawParams {
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime?: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime?: number;
    /**
     * Default 50, Max 50
     * int32
     * @example 50
     */
    limit?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1NftHistoryWithdrawResponse {
    /**
     * int32
     * @example 178
     */
    "total": number;
    "list": {
        /**
         * @example "ETH"
         */
        "network": string;
        /**
         * @example "0x2be5eed31d787fdb4880bc631c8e76bdfb6150e137f5cf1732e0416ea206f57f"
         */
        "txID": string;
        /**
         * @example "0xe507c961ee127d4439977a61af39c34eafee0dc6"
         */
        "contractAdrress": string;
        /**
         * @example "1000001247"
         */
        "tokenId": string;
        /**
         * int64
         * @example 1633674433000
         */
        "timestamp": number;
        /**
         * @example 0.1
         */
        "fee": number;
        /**
         * @example "ETH"
         */
        "feeAsset": string;
    }[];
}

interface GetSapiV1NftUserGetAssetParams {
    /**
     * Default 50, Max 50
     * int32
     * @example 50
     */
    limit?: number;
    /**
     * Default 1
     * int32
     * @example 1
     */
    page?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1NftUserGetAssetResponse {
    /**
     * int32
     * @example 347
     */
    "total": number;
    "list": {
        /**
         * @example "BSC"
         */
        "network": string;
        /**
         * @example "REGULAR11234567891779"
         */
        "contractAddress": string;
        /**
         * @example "100900000017"
         */
        "tokenId": string;
    }[];
}

interface GetSapiV1GiftcardVerifyParams {
    /**
     * reference number
     */
    referenceNo: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1GiftcardVerifyResponse {
    /**
     * @example "000000"
     */
    "code": string;
    /**
     * @example "success"
     */
    "message": string;
    "data": {
        "valid": boolean;
        /**
         * @example "BNB"
         */
        "token": string;
        /**
         * @example "0.00000001"
         */
        "amount": string;
    };
    "success": boolean;
}

interface GetSapiV1GiftcardCryptographyRsaPublicKeyParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface GetSapiV1GiftcardCryptographyRsaPublicKeyResponse {
    /**
     * @example "000000"
     */
    "code": string;
    /**
     * @example "success"
     */
    "message": string;
    /**
     * @example "MIGfMA0GCSqGSIb3DQEBAQUAA4GNADCBiQKBgQCXBBVKLAc1GQ5FsIFFqOHrPTox5noBONIKr+IAedTR9FkVxq6e65updEbfdhRNkMOeYIO2i0UylrjGC0X8YSoIszmrVHeV0l06Zh1oJuZos1+7N+WLuz9JvlPaawof3GUakTxYWWCa9+8KIbLKsoKMdfS96VT+8iOXO3quMGKUmQIDAQAB"
     */
    "data": string;
    /**
     * @example true
     */
    "success": boolean;
}

interface PostApiV3OrderTestParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * @example "SELL"
     */
    side: "SELL" | "BUY";
    /**
     * Order type
     */
    type: "LIMIT" | "MARKET" | "STOP_LOSS" | "STOP_LOSS_LIMIT" | "TAKE_PROFIT" | "TAKE_PROFIT_LIMIT" | "LIMIT_MAKER";
    /**
     * Order time in force
     */
    timeInForce?: "GTC" | "IOC" | "FOK";
    /**
     * Order quantity
     * double
     */
    quantity?: number;
    /**
     * Quote quantity
     * double
     */
    quoteOrderQty?: number;
    /**
     * Order price
     * double
     */
    price?: number;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    newClientOrderId?: string;
    /**
     * int64
     */
    strategyId?: number;
    /**
     * The value cannot be less than 1000000.
     * int64
     */
    strategyType?: number;
    /**
     * Used with STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
     * double
     * @example 20.01
     */
    stopPrice?: number;
    /**
     * Used with STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
     * double
     */
    trailingDelta?: number;
    /**
     * Used with LIMIT, STOP_LOSS_LIMIT, and TAKE_PROFIT_LIMIT to create an iceberg order.
     * double
     */
    icebergQty?: number;
    /**
     * Set the response JSON. MARKET and LIMIT order types default to FULL, all other orders default to ACK.
     */
    newOrderRespType?: "ACK" | "RESULT" | "FULL";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostApiV3OrderTestResponse {
}

interface PostApiV3OrderParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * @example "SELL"
     */
    side: "SELL" | "BUY";
    /**
     * Order type
     */
    type: "LIMIT" | "MARKET" | "STOP_LOSS" | "STOP_LOSS_LIMIT" | "TAKE_PROFIT" | "TAKE_PROFIT_LIMIT" | "LIMIT_MAKER";
    /**
     * Order time in force
     */
    timeInForce?: "GTC" | "IOC" | "FOK";
    /**
     * Order quantity
     * double
     */
    quantity?: number;
    /**
     * Quote quantity
     * double
     */
    quoteOrderQty?: number;
    /**
     * Order price
     * double
     */
    price?: number;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    newClientOrderId?: string;
    /**
     * int64
     */
    strategyId?: number;
    /**
     * The value cannot be less than 1000000.
     * int64
     */
    strategyType?: number;
    /**
     * Used with STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
     * double
     * @example 20.01
     */
    stopPrice?: number;
    /**
     * Used with STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
     * double
     */
    trailingDelta?: number;
    /**
     * Used with LIMIT, STOP_LOSS_LIMIT, and TAKE_PROFIT_LIMIT to create an iceberg order.
     * double
     */
    icebergQty?: number;
    /**
     * Set the response JSON. MARKET and LIMIT order types default to FULL, all other orders default to ACK.
     */
    newOrderRespType?: "ACK" | "RESULT" | "FULL";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostApiV3OrderCancelReplaceParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * @example "SELL"
     */
    side: "SELL" | "BUY";
    /**
     * Order type
     */
    type: "LIMIT" | "MARKET" | "STOP_LOSS" | "STOP_LOSS_LIMIT" | "TAKE_PROFIT" | "TAKE_PROFIT_LIMIT" | "LIMIT_MAKER";
    /**
     * - `STOP_ON_FAILURE` If the cancel request fails, the new order placement will not be attempted.
     * - `ALLOW_FAILURES` If new order placement will be attempted even if cancel request fails.
     * @example "STOP_ON_FAILURE"
     */
    cancelReplaceMode: string;
    /**
     * Order time in force
     */
    timeInForce?: "GTC" | "IOC" | "FOK";
    /**
     * Order quantity
     * double
     */
    quantity?: number;
    /**
     * Quote quantity
     * double
     */
    quoteOrderQty?: number;
    /**
     * Order price
     * double
     */
    price?: number;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    cancelNewClientOrderId?: string;
    /**
     * Either the cancelOrigClientOrderId or cancelOrderId must be provided. If both are provided, cancelOrderId takes precedence.
     */
    cancelOrigClientOrderId?: string;
    /**
     * Either the cancelOrigClientOrderId or cancelOrderId must be provided. If both are provided, cancelOrderId takes precedence.
     * int64
     * @example 12
     */
    cancelOrderId?: number;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    newClientOrderId?: string;
    /**
     * int64
     */
    strategyId?: number;
    /**
     * The value cannot be less than 1000000.
     * int64
     */
    strategyType?: number;
    /**
     * Used with STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
     * double
     * @example 20.01
     */
    stopPrice?: number;
    /**
     * Used with STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
     * double
     */
    trailingDelta?: number;
    /**
     * Used with LIMIT, STOP_LOSS_LIMIT, and TAKE_PROFIT_LIMIT to create an iceberg order.
     * double
     */
    icebergQty?: number;
    /**
     * Set the response JSON. MARKET and LIMIT order types default to FULL, all other orders default to ACK.
     */
    newOrderRespType?: "ACK" | "RESULT" | "FULL";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostApiV3OrderCancelReplaceResponse {
    /**
     * @example "SUCCESS"
     */
    "cancelResult": string;
    /**
     * @example "SUCCESS"
     */
    "newOrderResult": string;
    "cancelResponse": {
        /**
         * @example "BTCUSDT"
         */
        "symbol": string;
        /**
         * @example "DnLo3vTAQcjha43lAZhZ0y"
         */
        "origClientOrderId": string;
        /**
         * int64
         * @example 9
         */
        "orderId": number;
        /**
         * int64
         * @example -1
         */
        "orderListId": number;
        /**
         * @example "osxN3JXAtJvKvCqGeMWMVR"
         */
        "clientOrderId": string;
        /**
         * @example "0.01000000"
         */
        "price": string;
        /**
         * @example "0.000100"
         */
        "origQty": string;
        /**
         * @example "0.00000000"
         */
        "executedQty": string;
        /**
         * @example "0.00000000"
         */
        "cummulativeQuoteQty": string;
        /**
         * @example "CANCELED"
         */
        "status": string;
        /**
         * @example "GTC"
         */
        "timeInForce": string;
        /**
         * @example "LIMIT"
         */
        "type": string;
        /**
         * @example "SELL"
         */
        "side": string;
    };
    "newOrderResponse": {
        /**
         * @example "BTCUSDT"
         */
        "symbol": string;
        /**
         * int64
         * @example 10
         */
        "orderId": number;
        /**
         * int64
         * @example -1
         */
        "orderListId": number;
        /**
         * @example "wOceeeOzNORyLiQfw7jd8S"
         */
        "clientOrderId": string;
        /**
         * int64
         * @example 1652928801803
         */
        "transactTime": number;
        /**
         * @example "0.02000000"
         */
        "price": string;
        /**
         * @example "0.040000"
         */
        "origQty": string;
        /**
         * @example "0.00000000"
         */
        "executedQty": string;
        /**
         * @example "0.00000000"
         */
        "cummulativeQuoteQty": string;
        /**
         * @example "NEW"
         */
        "status": string;
        /**
         * @example "GTC"
         */
        "timeInForce": string;
        /**
         * @example "LIMIT"
         */
        "type": string;
        /**
         * @example "BUY"
         */
        "side": string;
        "fills": string[];
    };
}

interface PostApiV3OrderOcoParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * A unique Id for the entire orderList
     */
    listClientOrderId?: string;
    /**
     * @example "SELL"
     */
    side: "SELL" | "BUY";
    /**
     * double
     */
    quantity: number;
    /**
     * A unique Id for the limit order
     */
    limitClientOrderId?: string;
    /**
     * int64
     */
    limitStrategyId?: number;
    /**
     * The value cannot be less than 1000000.
     * int64
     */
    limitStrategyType?: number;
    /**
     * Order price
     * double
     */
    price: number;
    /**
     * double
     */
    limitIcebergQty?: number;
    /**
     * double
     */
    trailingDelta?: number;
    /**
     * A unique Id for the stop loss/stop loss limit leg
     */
    stopClientOrderId?: string;
    /**
     * double
     */
    stopPrice: number;
    /**
     * int64
     */
    stopStrategyId?: number;
    /**
     * int64
     */
    stopStrategyType?: number;
    /**
     * If provided, stopLimitTimeInForce is required.
     * double
     */
    stopLimitPrice?: number;
    /**
     * double
     */
    stopIcebergQty?: number;
    stopLimitTimeInForce?: "GTC" | "FOK" | "IOC";
    /**
     * Set the response JSON.
     */
    newOrderRespType?: "ACK" | "RESULT" | "FULL";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostApiV3OrderOcoResponse {
    /**
     * int64
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "EXEC_STARTED"
     */
    "listStatusType": string;
    /**
     * @example "EXECUTING"
     */
    "listOrderStatus": string;
    /**
     * @example "JYVpp3F0f5CAG15DhtrqLp"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1563417480525
     */
    "transactionTime": number;
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
    /**
     * @example [{"symbol":"LTCBTC","orderId":2,"orderListId":0,"clientOrderId":"Kk7sqHb9J6mJWTMDVW7Vos","transactTime":1563417480525,"price":"0.000000","origQty":"0.624363","executedQty":"0.000000","cummulativeQuoteQty":"0.000000","status":"NEW","timeInForce":"GTC","type":"STOP_LOSS","side":"BUY","stopPrice":"0.960664"},{"symbol":"LTCBTC","orderId":3,"orderListId":0,"clientOrderId":"xTXKaGYd4bluPVp78IVRvl","transactTime":1563417480525,"price":"0.036435","origQty":"0.624363","executedQty":"0.000000","cummulativeQuoteQty":"0.000000","status":"NEW","timeInForce":"GTC","type":"LIMIT_MAKER","side":"BUY"}]
     */
    "orderReports": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        /**
         * int64
         */
        "orderListId": number;
        "clientOrderId": string;
        /**
         * int64
         */
        "transactTime": number;
        "price": string;
        "origQty": string;
        "executedQty": string;
        "cummulativeQuoteQty": string;
        "status": string;
        "timeInForce": string;
        "type": string;
        "side": string;
        "stopPrice": string;
    }[];
}

interface PostSapiV1MarginTransferParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * * `1` - transfer from main account to margin account
     * * `2` - transfer from margin account to main account
     * int32
     */
    type: 1 | 2;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MarginLoanParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MarginRepayParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MarginOrderParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * @example "SELL"
     */
    side: "SELL" | "BUY";
    /**
     * Order type
     */
    type: "LIMIT" | "MARKET" | "STOP_LOSS" | "STOP_LOSS_LIMIT" | "TAKE_PROFIT" | "TAKE_PROFIT_LIMIT" | "LIMIT_MAKER";
    /**
     * double
     */
    quantity: number;
    /**
     * Quote quantity
     * double
     */
    quoteOrderQty?: number;
    /**
     * Order price
     * double
     */
    price?: number;
    /**
     * Used with STOP_LOSS, STOP_LOSS_LIMIT, TAKE_PROFIT, and TAKE_PROFIT_LIMIT orders.
     * double
     * @example 20.01
     */
    stopPrice?: number;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    newClientOrderId?: string;
    /**
     * Used with LIMIT, STOP_LOSS_LIMIT, and TAKE_PROFIT_LIMIT to create an iceberg order.
     * double
     */
    icebergQty?: number;
    /**
     * Set the response JSON.
     */
    newOrderRespType?: "ACK" | "RESULT" | "FULL";
    /**
     * Default `NO_SIDE_EFFECT`
     */
    sideEffectType?: "NO_SIDE_EFFECT" | "MARGIN_BUY" | "AUTO_REPAY";
    /**
     * Order time in force
     */
    timeInForce?: "GTC" | "IOC" | "FOK";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MarginOrderOcoParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * A unique Id for the entire orderList
     */
    listClientOrderId?: string;
    /**
     * @example "SELL"
     */
    side: "SELL" | "BUY";
    /**
     * double
     */
    quantity: number;
    /**
     * A unique Id for the limit order
     */
    limitClientOrderId?: string;
    /**
     * Order price
     * double
     */
    price: number;
    /**
     * double
     */
    limitIcebergQty?: number;
    /**
     * A unique Id for the stop loss/stop loss limit leg
     */
    stopClientOrderId?: string;
    /**
     * double
     */
    stopPrice: number;
    /**
     * If provided, stopLimitTimeInForce is required.
     * double
     */
    stopLimitPrice?: number;
    /**
     * double
     */
    stopIcebergQty?: number;
    stopLimitTimeInForce?: "GTC" | "FOK" | "IOC";
    /**
     * Set the response JSON.
     */
    newOrderRespType?: "ACK" | "RESULT" | "FULL";
    /**
     * Default `NO_SIDE_EFFECT`
     */
    sideEffectType?: "NO_SIDE_EFFECT" | "MARGIN_BUY" | "AUTO_REPAY";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MarginOrderOcoResponse {
    /**
     * int64
     */
    "orderListId": number;
    /**
     * @example "OCO"
     */
    "contingencyType": string;
    /**
     * @example "EXEC_STARTED"
     */
    "listStatusType": string;
    /**
     * @example "EXECUTING"
     */
    "listOrderStatus": string;
    /**
     * @example "JYVpp3F0f5CAG15DhtrqLp"
     */
    "listClientOrderId": string;
    /**
     * int64
     * @example 1563417480525
     */
    "transactionTime": number;
    /**
     * @example "LTCBTC"
     */
    "symbol": string;
    /**
     * will not return if no margin trade happens
     * @example "5"
     */
    "marginBuyBorrowAmount": string;
    /**
     * will not return if no margin trade happens
     * @example "BTC"
     */
    "marginBuyBorrowAsset": string;
    "isIsolated": boolean;
    /**
     * @example [{"symbol":"LTCBTC","orderId":2,"clientOrderId":"Kk7sqHb9J6mJWTMDVW7Vos"},{"symbol":"LTCBTC","orderId":3,"clientOrderId":"xTXKaGYd4bluPVp78IVRvl"}]
     */
    "orders": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        "clientOrderId": string;
    }[];
    /**
     * @example [{"symbol":"LTCBTC","orderId":2,"orderListId":0,"clientOrderId":"Kk7sqHb9J6mJWTMDVW7Vos","transactTime":1563417480525,"price":"0.000000","origQty":"0.624363","executedQty":"0.000000","cummulativeQuoteQty":"0.000000","status":"NEW","timeInForce":"GTC","type":"STOP_LOSS","side":"BUY","stopPrice":"0.960664"},{"symbol":"LTCBTC","orderId":3,"orderListId":0,"clientOrderId":"xTXKaGYd4bluPVp78IVRvl","transactTime":1563417480525,"price":"0.036435","origQty":"0.624363","executedQty":"0.000000","cummulativeQuoteQty":"0.000000","status":"NEW","timeInForce":"GTC","type":"LIMIT_MAKER","side":"BUY"}]
     */
    "orderReports": {
        "symbol": string;
        /**
         * int64
         */
        "orderId": number;
        /**
         * int64
         */
        "orderListId": number;
        "clientOrderId": string;
        /**
         * int64
         */
        "transactTime": number;
        "price": string;
        "origQty": string;
        "executedQty": string;
        "cummulativeQuoteQty": string;
        "status": string;
        "timeInForce": string;
        "type": string;
        "side": string;
        "stopPrice": string;
    }[];
}

interface PostSapiV1MarginIsolatedTransferParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * @example "SPOT"
     */
    transFrom: "SPOT" | "ISOLATED_MARGIN";
    /**
     * @example "ISOLATED_MARGIN"
     */
    transTo: "SPOT" | "ISOLATED_MARGIN";
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MarginIsolatedTransferResponse {
}

interface PostSapiV1MarginIsolatedAccountParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MarginIsolatedAccountResponse {
    "success": boolean;
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
}

interface PostSapiV1BnbBurnParams {
    /**
     * Determines whether to use BNB to pay for trading fees on SPOT
     * @example "true"
     */
    spotBNBBurn?: "true" | "false";
    /**
     * Determines whether to use BNB to pay for margin loan's interest
     * @example "false"
     */
    interestBNBBurn?: "true" | "false";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AccountDisableFastWithdrawSwitchParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AccountDisableFastWithdrawSwitchResponse {
}

interface PostSapiV1AccountEnableFastWithdrawSwitchParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AccountEnableFastWithdrawSwitchResponse {
}

interface PostSapiV1CapitalWithdrawApplyParams {
    /**
     * Coin name
     * @example "BNB"
     */
    coin: string;
    /**
     * Client id for withdraw
     */
    withdrawOrderId?: string;
    /**
     * Get the value from `GET /sapi/v1/capital/config/getall`
     */
    network?: string;
    address: string;
    /**
     * Secondary address identifier for coins like XRP,XMR etc.
     */
    addressTag?: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * When making internal transfer
     * - `true` ->  returning the fee to the destination account;
     * - `false` -> returning the fee back to the departure account.
     */
    transactionFeeFlag?: boolean;
    name?: string;
    /**
     * The wallet type for withdraw，0-Spot wallet, 1- Funding wallet. Default is Spot wallet
     * int32
     */
    walletType?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1CapitalWithdrawApplyResponse {
    /**
     * @example "7213fea8e94b4a5593d507237e5a555b"
     */
    "id": string;
}

interface PostSapiV1AssetDustBtcParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AssetDustBtcResponse {
    "details": {
        /**
         * @example "ADA"
         */
        "asset": string;
        /**
         * @example "ADA"
         */
        "assetFullName": string;
        /**
         * Convertible amount
         * @example "6.21"
         */
        "amountFree": string;
        /**
         * BTC amount
         * @example "0.00016848"
         */
        "toBTC": string;
        /**
         * BNB amount（Not deducted commission fee
         * @example "0.01777302"
         */
        "toBNB": string;
        /**
         * BNB amount（Deducted commission fee
         * @example "0.01741756"
         */
        "toBNBOffExchange": string;
        /**
         * Commission fee
         * @example "0.00035546"
         */
        "exchange": string;
    }[];
    /**
     * @example "0.00016848"
     */
    "totalTransferBtc": string;
    /**
     * @example "0.01777302"
     */
    "totalTransferBNB": string;
    /**
     * Commission fee
     * @example "0.02"
     */
    "dribbletPercentage": string;
}

interface PostSapiV1AssetDustParams {
    /**
     * The asset being converted. For example, asset=BTC&asset=USDT
     */
    asset: string[];
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AssetDustResponse {
    /**
     * @example "0.02102542"
     */
    "totalServiceCharge": string;
    /**
     * @example "1.05127099"
     */
    "totalTransfered": string;
    "transferResult": {
        /**
         * @example "0.03000000"
         */
        "amount": string;
        /**
         * @example "ETH"
         */
        "fromAsset": string;
        /**
         * int64
         * @example 1563368549307
         */
        "operateTime": number;
        /**
         * @example "0.00500000"
         */
        "serviceChargeAmount": string;
        /**
         * int64
         * @example 2970932918
         */
        "tranId": number;
        /**
         * @example "0.25000000"
         */
        "transferedAmount": string;
    }[];
}

interface PostSapiV1AssetTransferParams {
    /**
     * Universal transfer type
     * @example "MAIN_C2C"
     */
    type: "MAIN_C2C" | "MAIN_UMFUTURE" | "MAIN_CMFUTURE" | "MAIN_MARGIN" | "MAIN_MINING" | "C2C_MAIN" | "C2C_UMFUTURE" | "C2C_MINING" | "C2C_MARGIN" | "UMFUTURE_MAIN" | "UMFUTURE_C2C" | "UMFUTURE_MARGIN" | "CMFUTURE_MAIN" | "CMFUTURE_MARGIN" | "MARGIN_MAIN" | "MARGIN_UMFUTURE" | "MARGIN_CMFUTURE" | "MARGIN_MINING" | "MARGIN_C2C" | "MINING_MAIN" | "MINING_UMFUTURE" | "MINING_C2C" | "MINING_MARGIN" | "MAIN_PAY" | "PAY_MAIN" | "ISOLATEDMARGIN_MARGIN" | "MARGIN_ISOLATEDMARGIN" | "ISOLATEDMARGIN_ISOLATEDMARGIN";
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * Must be sent when type are ISOLATEDMARGIN_MARGIN and ISOLATEDMARGIN_ISOLATEDMARGIN
     * @example "BNBUSDT"
     */
    fromSymbol?: string;
    /**
     * Must be sent when type are MARGIN_ISOLATEDMARGIN and ISOLATEDMARGIN_ISOLATEDMARGIN
     * @example "BNBUSDT"
     */
    toSymbol?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AssetTransferResponse {
    /**
     * int64
     * @example 13526853623
     */
    "tranId": number;
}

interface PostSapiV1AssetGetFundingAssetParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    needBtcValuation?: "true" | "false";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AssetGetFundingAssetResponseItem {
    /**
     * @example "USDT"
     */
    "asset": string;
    /**
     * @example "1"
     */
    "free": string;
    /**
     * @example "0"
     */
    "locked": string;
    /**
     * @example "0"
     */
    "freeze": string;
    /**
     * @example "0"
     */
    "withdrawing": string;
    /**
     * @example "0.00000091"
     */
    "btcValuation": string;
}

interface PostSapiV3AssetGetUserAssetParams {
    /**
     * @example "BNB"
     */
    asset?: string;
    needBtcValuation?: "true" | "false";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV3AssetGetUserAssetResponseItem {
    /**
     * @example "AVAX"
     */
    "asset": string;
    /**
     * @example "1"
     */
    "free": string;
    /**
     * @example "0"
     */
    "locked": string;
    /**
     * @example "0"
     */
    "freeze": string;
    /**
     * @example "0"
     */
    "withdrawing": string;
    /**
     * @example "0"
     */
    "ipoable": string;
    /**
     * @example "0"
     */
    "btcValuation": string;
}

interface PostSapiV1AssetConvertTransferParams {
    /**
     * The unique flag, the min length is 20
     */
    clientTranId: string;
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * Target asset you want to convert
     * @example "BNB"
     */
    targetAsset: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AssetConvertTransferResponse {
    /**
     * int64
     * @example 118263407119
     */
    "tranId": number;
    /**
     * @example "S"
     */
    "status": string;
}

interface PostSapiV1AssetConvertTransferQueryByPageParams {
    /**
     * The transaction id
     * int64
     * @example 118263615991
     */
    tranId?: number;
    /**
     * If it is blank, we will match deducted asset and target asset.
     * @example "BTC"
     */
    asset?: string;
    /**
     * UTC timestamp in ms
     * int64
     */
    startTime: number;
    /**
     * UTC timestamp in ms
     * int64
     */
    endTime: number;
    /**
     * MAIN: main account. CARD: funding account. If it is blank, we will query spot and card wallet, otherwise, we just query the corresponding wallet
     */
    accountType?: "MAIN" | "CARD";
    /**
     * Current querying page. Start from 1. Default:1
     * int32
     * @example 1
     */
    current?: number;
    /**
     * Default:10 Max:100
     * int32
     * @example 100
     */
    size?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AssetConvertTransferQueryByPageResponse {
    /**
     * int32
     * @example 1
     */
    "total": number;
    "rows": {
        /**
         * int64
         * @example 118263407119
         */
        "tranId": number;
        /**
         * int64
         * @example 244
         */
        "type": number;
        /**
         * int64
         * @example 1664442078000
         */
        "time": number;
        /**
         * @example "BUSD"
         */
        "deductedAsset": string;
        /**
         * @example "1"
         */
        "deductedAmount": string;
        /**
         * @example "USDC"
         */
        "targetAsset": string;
        /**
         * @example "1"
         */
        "targetAmount": string;
        /**
         * @example "S"
         */
        "status": string;
        /**
         * @example "MAIN"
         */
        "accountType": string;
    }[];
}

interface PostSapiV1SubAccountVirtualSubAccountParams {
    /**
     * Please input a string. We will create a virtual email using that string for you to register
     */
    subAccountString: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountVirtualSubAccountResponse {
    /**
     * @example "addsdd_virtual@aasaixwqnoemail.com"
     */
    "email": string;
}

interface PostSapiV1SubAccountFuturesInternalTransferParams {
    /**
     * Sender email
     */
    fromEmail: string;
    /**
     * Recipient email
     */
    toEmail: string;
    /**
     * 1:USDT-margined Futures,2: Coin-margined Futures
     * int32
     * @example 2
     */
    futuresType: number;
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountFuturesInternalTransferResponse {
    "success": boolean;
    /**
     * @example "2934662589"
     */
    "txnId": string;
}

interface PostSapiV1SubAccountMarginEnableParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountMarginEnableResponse {
    /**
     * @example "123@test.com"
     */
    "email": string;
    "isMarginEnabled": boolean;
}

interface PostSapiV1SubAccountFuturesEnableParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountFuturesEnableResponse {
    /**
     * @example "123@test.com"
     */
    "email": string;
    "isFuturesEnabled": boolean;
}

interface PostSapiV1SubAccountFuturesTransferParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * * `1` - transfer from subaccount's spot account to its USDT-margined futures account
     * * `2` - transfer from subaccount's USDT-margined futures account to its spot account
     * * `3` - transfer from subaccount's spot account to its COIN-margined futures account
     * * `4` - transfer from subaccount's COIN-margined futures account to its spot account
     * int32
     */
    type: 1 | 2 | 3 | 4;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountFuturesTransferResponse {
    /**
     * @example "2966662589"
     */
    "txnId": string;
}

interface PostSapiV1SubAccountMarginTransferParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * * `1` - transfer from subaccount's spot account to margin account
     * * `2` - transfer from subaccount's margin account to its spot account
     * int32
     */
    type: 1 | 2;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountMarginTransferResponse {
    /**
     * @example "2966662589"
     */
    "txnId": string;
}

interface PostSapiV1SubAccountTransferSubToSubParams {
    /**
     * Recipient email
     */
    toEmail: string;
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountTransferSubToSubResponse {
    /**
     * @example "2966662589"
     */
    "txnId": string;
}

interface PostSapiV1SubAccountTransferSubToMasterParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountTransferSubToMasterResponse {
    /**
     * @example "2966662589"
     */
    "txnId": string;
}

interface PostSapiV1SubAccountUniversalTransferParams {
    /**
     * Sub-account email
     */
    fromEmail?: string;
    /**
     * Sub-account email
     */
    toEmail?: string;
    fromAccountType: "SPOT" | "USDT_FUTURE" | "COIN_FUTURE" | "MARGIN" | "ISOLATED_MARGIN";
    toAccountType: "SPOT" | "USDT_FUTURE" | "COIN_FUTURE" | "MARGIN" | "ISOLATED_MARGIN";
    clientTranId?: string;
    /**
     * Only supported under ISOLATED_MARGIN type
     * @example "BNBUSDT"
     */
    symbol?: string;
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountUniversalTransferResponse {
    /**
     * int64
     * @example 11945860693
     */
    "tranId": number;
    /**
     * @example "11945860694"
     */
    "clientTranId": string;
}

interface PostSapiV1SubAccountBlvtEnableParams {
    /**
     * Sub-account email
     */
    email: string;
    /**
     * Only true for now
     */
    enableBlvt: boolean;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountBlvtEnableResponse {
    /**
     * @example "123@test.com"
     */
    "email": string;
    "enableBlvt": boolean;
}

interface PostSapiV1ManagedSubaccountDepositParams {
    /**
     * Recipient email
     */
    toEmail: string;
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1ManagedSubaccountDepositResponse {
    /**
     * int64
     * @example 66157362489
     */
    "tranId": number;
}

interface PostSapiV1ManagedSubaccountWithdrawParams {
    /**
     * Sender email
     */
    fromEmail: string;
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * Withdrawals is automatically occur on the transfer date(UTC0). If a date is not selected, the withdrawal occurs right now
     * int64
     */
    transferDate?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1ManagedSubaccountWithdrawResponse {
    /**
     * int64
     * @example 66157362489
     */
    "tranId": number;
}

interface PostSapiV1SubAccountSubAccountApiIpRestrictionParams {
    /**
     * Sub-account email
     */
    email: string;
    subAccountApiKey: string;
    /**
     * true or false
     */
    ipRestrict: boolean;
    /**
     * true or false
     */
    thirdParty?: boolean;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountSubAccountApiIpRestrictionResponse {
    /**
     * @example "true"
     */
    "ipRestrict": string;
    "ipList": (string | string)[];
    /**
     * int64
     * @example 1636369557189
     */
    "updateTime": number;
    /**
     * @example "k5V49ldtn4tszj6W3hystegdfvmGbqDzjmkCtpTvC0G74WhK7yd4rfCTo4lShf"
     */
    "apiKey": string;
}

interface PostSapiV1SubAccountSubAccountApiIpRestrictionIpListParams {
    /**
     * Sub-account email
     */
    email: string;
    subAccountApiKey: string;
    /**
     * Can be added in batches, separated by commas
     */
    ipAddress: string;
    /**
     * third party IP list name
     */
    thirdPartyName?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1SubAccountSubAccountApiIpRestrictionIpListResponse {
    /**
     * @example ["8.34.21.1015.24.40.1","thirdPartyName"]
     */
    "ip": string;
    /**
     * int64
     * @example 1636369557189
     */
    "updateTime": number;
    /**
     * @example "k5V49ldtn4tszj6W3hystegdfvmGbqDzjmkCtpTvC0G74WhK7yd4rfCTo4lShf"
     */
    "apiKey": string;
}

interface PostApiV3UserDataStreamResponse {
    /**
     * @example "pqia91ma19a5s61cv6a81va65sdf19v8a65a1a5s61cv6a81va65sdf19v8a65a1"
     */
    "listenKey": string;
}

interface PostSapiV1UserDataStreamResponse {
    /**
     * @example "pqia91ma19a5s61cv6a81va65sdf19v8a65a1a5s61cv6a81va65sdf19v8a65a1"
     */
    "listenKey": string;
}

interface PostSapiV1UserDataStreamIsolatedResponse {
    /**
     * @example "T3ee22BIYuWqmvne0HNq2A2WsFlEtLhvWCtItw6ffhhdmjifQ2tRbuKkTHhr"
     */
    "listenKey": string;
}

interface PostSapiV1LendingDailyPurchaseParams {
    productId: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1LendingDailyPurchaseResponse {
    /**
     * int64
     * @example 40607
     */
    "purchaseId": number;
}

interface PostSapiV1LendingDailyRedeemParams {
    productId: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    type: "FAST" | "NORMAL";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1LendingDailyRedeemResponse {
}

interface PostSapiV1LendingCustomizedFixedPurchaseParams {
    projectId: string;
    lot: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1LendingCustomizedFixedPurchaseResponse {
    /**
     * @example "18356"
     */
    "purchaseId": string;
}

interface PostSapiV1LendingPositionChangedParams {
    projectId: string;
    lot: string;
    positionId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1LendingPositionChangedResponse {
    /**
     * int64
     * @example 862290
     */
    "dailyPurchaseId": number;
    "success": boolean;
    /**
     * int64
     * @example 1577233578000
     */
    "time": number;
}

interface PostSapiV1StakingPurchaseParams {
    /**
     * * `STAKING` - for Locked Staking
     * * `F_DEFI` - for flexible DeFi Staking
     * * `L_DEFI` - for locked DeFi Staking
     */
    product: string;
    productId: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * true or false, default false. Active if product is `STAKING` or `L_DEFI`
     */
    renewable?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1StakingPurchaseResponse {
    /**
     * @example "12345"
     */
    "positionId": string;
    /**
     * @example true
     */
    "success": boolean;
}

interface PostSapiV1StakingRedeemParams {
    /**
     * * `STAKING` - for Locked Staking
     * * `F_DEFI` - for flexible DeFi Staking
     * * `L_DEFI` - for locked DeFi Staking
     */
    product: string;
    /**
     * Mandatory if product is `STAKING` or `L_DEFI`
     */
    positionId?: string;
    productId: string;
    /**
     * Mandatory if product is `F_DEFI`
     * double
     */
    amount?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1StakingRedeemResponse {
    /**
     * @example true
     */
    "success": boolean;
}

interface PostSapiV1StakingSetAutoStakingParams {
    /**
     * * `STAKING` - for Locked Staking
     * * `L_DEFI` - for locked DeFi Staking
     */
    product: string;
    positionId: string;
    /**
     * true or false
     */
    renewable: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1StakingSetAutoStakingResponse {
    /**
     * @example true
     */
    "success": boolean;
}

interface PostSapiV1MiningHashTransferConfigParams {
    /**
     * Mining Account
     */
    userName: string;
    /**
     * Algorithm(sha256)
     */
    algo: string;
    /**
     * Search date, millisecond timestamp, while empty query all
     */
    startDate?: string;
    /**
     * Search date, millisecond timestamp, while empty query all
     */
    endDate?: string;
    /**
     * Mining Account
     */
    toPoolUser: string;
    /**
     * Resale hashrate h/s must be transferred (BTC is greater than 500000000000 ETH is greater than 500000)
     */
    hashRate: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MiningHashTransferConfigResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    /**
     * Mining Account
     * int64
     * @example 171
     */
    "data": number;
}

interface PostSapiV1MiningHashTransferConfigCancelParams {
    /**
     * Mining ID
     */
    configId: string;
    /**
     * Mining Account
     */
    userName: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1MiningHashTransferConfigCancelResponse {
    /**
     * int64
     */
    "code": number;
    "msg": string;
    "data": boolean;
}

interface PostSapiV1FuturesTransferParams {
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * 1: transfer from spot account to USDT-Ⓜ futures account. 2: transfer from USDT-Ⓜ futures account to spot account. 3: transfer from spot account to COIN-Ⓜ futures account. 4: transfer from COIN-Ⓜ futures account to spot account.
     * int64
     */
    type: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1FuturesTransferResponse {
    /**
     * int64
     * @example 100000001
     */
    "tranId": number;
}

interface PostSapiV1AlgoFuturesNewOrderVpParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * @example "SELL"
     */
    side: "SELL" | "BUY";
    /**
     * Default BOTH for One-way Mode ; LONG or SHORT for Hedge Mode. It must be sent in Hedge Mode.
     * @example "BOTH"
     */
    positionSide?: "BOTH" | "LONG" | "SHORT";
    /**
     * Quantity of base asset; The notional (quantity * mark price(base asset)) must be more than the equivalent of 10,000 USDT and less than the equivalent of 1,000,000 USDT
     * double
     */
    quantity: number;
    /**
     * Represent the relative speed of the current execution; ENUM: LOW, MEDIUM, HIGH
     * @example "LOW"
     */
    urgency: "LOW" | "MEDIUM" | "HIGH";
    /**
     * A unique id among Algo orders (length should be 32 characters)， If it is not sent, we will give default value
     * @example "00358ce6a268403398bd34eaa36dffe7"
     */
    clientAlgoId?: string;
    /**
     * 'true' or 'false'. Default 'false'; Cannot be sent in Hedge Mode; Cannot be sent when you open a position
     */
    reduceOnly?: boolean;
    /**
     * Limit price of the order; If it is not sent, will place order by market price by default
     * double
     */
    limitPrice?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AlgoFuturesNewOrderVpResponse {
    /**
     * @example "00358ce6a268403398bd34eaa36dffe7"
     */
    "clientAlgoId": string;
    /**
     * @example true
     */
    "success": boolean;
    /**
     * int64
     */
    "code": number;
    /**
     * @example "OK"
     */
    "msg": string;
}

interface PostSapiV1AlgoFuturesNewOrderTwapParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * @example "SELL"
     */
    side: "SELL" | "BUY";
    /**
     * Default BOTH for One-way Mode ; LONG or SHORT for Hedge Mode. It must be sent in Hedge Mode.
     * @example "BOTH"
     */
    positionSide?: "BOTH" | "LONG" | "SHORT";
    /**
     * Quantity of base asset; The notional (quantity * mark price(base asset)) must be more than the equivalent of 10,000 USDT and less than the equivalent of 1,000,000 USDT
     * double
     */
    quantity: number;
    /**
     * Duration for TWAP orders in seconds. [300, 86400];Less than 5min => defaults to 5 min; Greater than 24h => defaults to 24h
     * int64
     * @example 300
     */
    duration: number;
    /**
     * A unique id among Algo orders (length should be 32 characters)， If it is not sent, we will give default value
     * @example "00358ce6a268403398bd34eaa36dffe7"
     */
    clientAlgoId?: string;
    /**
     * 'true' or 'false'. Default 'false'; Cannot be sent in Hedge Mode; Cannot be sent when you open a position
     */
    reduceOnly?: boolean;
    /**
     * Limit price of the order; If it is not sent, will place order by market price by default
     * double
     */
    limitPrice?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1AlgoFuturesNewOrderTwapResponse {
    /**
     * @example "00358ce6a268403398bd34eaa36dffe7"
     */
    "clientAlgoId": string;
    /**
     * @example true
     */
    "success": boolean;
    /**
     * int64
     */
    "code": number;
    /**
     * @example "OK"
     */
    "msg": string;
}

interface PostSapiV1PortfolioRepayParams {
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1PortfolioRepayResponse {
    /**
     * int64
     * @example 58203331886213500
     */
    "tranId": number;
}

interface PostSapiV1BlvtSubscribeParams {
    /**
     * BTCDOWN, BTCUP
     */
    tokenName: string;
    /**
     * Spot balance
     * double
     */
    cost: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1BlvtSubscribeResponse {
    /**
     * double
     * @example 123
     */
    "id": number;
    /**
     * S, P, and F for "success", "pending", and "failure"
     * @example "S"
     */
    "status": string;
    /**
     * @example "LINKUP"
     */
    "tokenName": string;
    /**
     * subscribed token amount
     * @example "0.9559090500"
     */
    "amount": string;
    /**
     * subscription cost in usdt
     * @example "9.99999995"
     */
    "cost": string;
    /**
     * int64
     * @example 1600249972899
     */
    "timestamp": number;
}

interface PostSapiV1BlvtRedeemParams {
    /**
     * BTCDOWN, BTCUP
     */
    tokenName: string;
    /**
     * double
     * @example "1.01"
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1BlvtRedeemResponse {
    /**
     * double
     * @example 123
     */
    "id": number;
    /**
     * S, P, and F for "success", "pending", and "failure"
     * @example "S"
     */
    "status": string;
    /**
     * @example "LINKUP"
     */
    "tokenName": string;
    /**
     * Redemption token amount
     * @example "0.95590905"
     */
    "redeemAmount": string;
    /**
     * Redemption value in usdt
     * @example "10.05022099"
     */
    "amount": string;
    /**
     * int64
     * @example 1600250279614
     */
    "timestamp": number;
}

interface PostSapiV1BswapLiquidityAddParams {
    /**
     * int64
     */
    poolId: number;
    /**
     * * `Single` - to add a single token
     * * `Combination` - to add dual tokens
     * @example "Single"
     */
    type?: "Single" | "Combination";
    /**
     * @example "BTC"
     */
    asset: string;
    /**
     * double
     */
    quantity: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1BswapLiquidityAddResponse {
    /**
     * int64
     * @example 12341
     */
    "operationId": number;
}

interface PostSapiV1BswapLiquidityRemoveParams {
    /**
     * int64
     */
    poolId: number;
    /**
     * * `SINGLE` - for single asset removal
     * * `COMBINATION` - for combination of all coins removal
     * @example "SINGLE"
     */
    type: "SINGLE" | "COMBINATION";
    /**
     * Mandatory for single asset removal
     * @example "BNB"
     */
    asset?: string;
    /**
     * double
     */
    shareAmount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1BswapLiquidityRemoveResponse {
    /**
     * int64
     * @example 12341
     */
    "operationId": number;
}

interface PostSapiV1BswapSwapParams {
    /**
     * @example "USDT"
     */
    quoteAsset: string;
    /**
     * @example "BUSD"
     */
    baseAsset: string;
    /**
     * double
     */
    quoteQty: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1BswapSwapResponse {
    /**
     * int64
     * @example 2314
     */
    "swapId": number;
}

interface PostSapiV1BswapClaimRewardsParams {
    /**
     * 0: Swap rewards, 1: Liquidity rewards, default to 0
     * int32
     */
    type?: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1BswapClaimRewardsResponse {
    "success": boolean;
}

interface PostSapiV1LoanBorrowParams {
    /**
     * Coin loaned
     * @example "BUSD"
     */
    loanCoin: string;
    /**
     * Mandatory when collateralAmount is empty
     * double
     * @example 100.5
     */
    loanAmount?: number;
    /**
     * Coin used as collateral
     * @example "BNB"
     */
    collateralCoin: string;
    /**
     * Mandatory when loanAmount is empty
     * double
     * @example 50.5
     */
    collateralAmount?: number;
    /**
     * 7/14/30/90/180 days
     * int32
     * @example 30
     */
    loanTerm: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1LoanBorrowResponse {
    /**
     * @example "BUSD"
     */
    "loanCoin": string;
    /**
     * @example "100.5"
     */
    "loanAmount": string;
    /**
     * @example "BNB"
     */
    "collateralCoin": string;
    /**
     * @example "50.5"
     */
    "collateralAmount": string;
    /**
     * @example "0.001234"
     */
    "hourlyInterestRate": string;
    /**
     * @example "100000001"
     */
    "orderId": string;
}

interface PostSapiV1LoanRepayParams {
    /**
     * Order ID
     * int64
     * @example 123456789
     */
    orderId: number;
    /**
     * Repayment Amount
     * double
     * @example 100.5
     */
    amount: number;
    /**
     * Default: 1. 1 for 'repay with borrowed coin'; 2 for 'repay with collateral'.
     * int32
     * @example 1
     */
    type?: number;
    /**
     * Default: TRUE. TRUE: Return extra collateral to spot account; FALSE: Keep extra collateral in the order.
     */
    collateralReturn?: boolean;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1LoanAdjustLtvParams {
    /**
     * Order ID
     * int64
     * @example 123456789
     */
    orderId: number;
    /**
     * Amount
     * double
     * @example 100.5
     */
    amount: number;
    /**
     * 'ADDITIONAL', 'REDUCED'
     * @example "ADDITIONAL"
     */
    direction: "ADDITIONAL" | "REDUCED";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1LoanAdjustLtvResponse {
    /**
     * @example "BUSD"
     */
    "loanCoin": string;
    /**
     * @example "BNB"
     */
    "collateralCoin": string;
    /**
     * @example "ADDITIONAL"
     */
    "direction": string;
    /**
     * @example "5.235"
     */
    "amount": string;
    /**
     * @example "0.52"
     */
    "currentLTV": string;
}

interface PostSapiV1GiftcardCreateCodeParams {
    /**
     * The coin type contained in the Binance Code
     */
    token: string;
    /**
     * The amount of the coin
     * double
     */
    amount: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1GiftcardCreateCodeResponse {
    /**
     * @example "000000"
     */
    "code": string;
    /**
     * @example "success"
     */
    "message": string;
    "data": {
        /**
         * @example "0033002327977405"
         */
        "referenceNo": string;
        /**
         * @example "AOGANK3NB4GIT3C6"
         */
        "code": string;
    };
    "success": boolean;
}

interface PostSapiV1GiftcardRedeemCodeParams {
    /**
     * Binance Code
     */
    code: string;
    /**
     * Each external unique ID represents a unique user on the partner platform. The function helps you to identify the redemption behavior of different users, such as redemption frequency and amount. It also helps risk and limit control of a single account, such as daily limit on redemption volume, frequency, and incorrect number of entries. This will also prevent a single user account reach the partner's daily redemption limits. We strongly recommend you to use this feature and transfer us the User ID of your users if you have different users redeeming Binance codes on your platform. To protect user data privacy, you may choose to transfer the user id in any desired format (max. 400 characters).
     */
    externalUid?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface PostSapiV1GiftcardRedeemCodeResponse {
    /**
     * @example "000000"
     */
    "code": string;
    /**
     * @example "success"
     */
    "message": string;
    "data": {
        /**
         * @example "BNB"
         */
        "token": string;
        /**
         * @example "10"
         */
        "amount": string;
        /**
         * @example "0033002327977405"
         */
        "referenceNo": string;
        /**
         * @example "10316281761814589440"
         */
        "identityNo": string;
    };
    "success": boolean;
}

interface DeleteApiV3OrderParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * Order id
     * int64
     */
    orderId?: number;
    /**
     * Order id from client
     */
    origClientOrderId?: string;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    newClientOrderId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteApiV3OpenOrdersParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteApiV3OrderListParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * Order list id
     * int64
     */
    orderListId?: number;
    /**
     * A unique Id for the entire orderList
     */
    listClientOrderId?: string;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    newClientOrderId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteSapiV1MarginOrderParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Order id
     * int64
     */
    orderId?: number;
    /**
     * Order id from client
     */
    origClientOrderId?: string;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    newClientOrderId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteSapiV1MarginOpenOrdersParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteSapiV1MarginOrderListParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * * `TRUE` - For isolated margin
     * * `FALSE` - Default, not for isolated margin
     */
    isIsolated?: "TRUE" | "FALSE";
    /**
     * Order list id
     * int64
     */
    orderListId?: number;
    /**
     * A unique Id for the entire orderList
     */
    listClientOrderId?: string;
    /**
     * Used to uniquely identify this cancel. Automatically generated by default
     */
    newClientOrderId?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteSapiV1MarginIsolatedAccountParams {
    /**
     * Trading symbol, e.g. BNBUSDT
     * @example "BNBUSDT"
     */
    symbol: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteSapiV1MarginIsolatedAccountResponse {
    "success": boolean;
    /**
     * @example "BTCUSDT"
     */
    "symbol": string;
}

interface DeleteSapiV1SubAccountSubAccountApiIpRestrictionIpListParams {
    /**
     * Sub-account email
     */
    email: string;
    subAccountApiKey: string;
    /**
     * Can be added in batches, separated by commas
     */
    ipAddress?: string;
    /**
     * third party IP list name
     */
    thirdPartyName?: string;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteSapiV1SubAccountSubAccountApiIpRestrictionIpListResponse {
    /**
     * @example "true"
     */
    "ipRestrict": string;
    /**
     * @example ["69.210.67.14","thirdPartyName"]
     */
    "ipList": string[];
    /**
     * int64
     * @example 1636369557189
     */
    "updateTime": number;
    /**
     * @example "k5V49ldtn4tszj6W3hystegdfvmGbqDzjmkCtpTvC0G74WhK7yd4rfCTo4lShf"
     */
    "apiKey": string;
}

interface DeleteApiV3UserDataStreamParams {
    /**
     * User websocket listen key
     * @example "pqia91ma19a5s61cv6a81va65sdf19v8a65a1a5s61cv6a81va65sdf19v8a65a1"
     */
    listenKey?: string;
}

interface DeleteApiV3UserDataStreamResponse {
}

interface DeleteSapiV1UserDataStreamParams {
    /**
     * User websocket listen key
     * @example "pqia91ma19a5s61cv6a81va65sdf19v8a65a1a5s61cv6a81va65sdf19v8a65a1"
     */
    listenKey?: string;
}

interface DeleteSapiV1UserDataStreamResponse {
}

interface DeleteSapiV1UserDataStreamIsolatedParams {
    /**
     * User websocket listen key
     * @example "pqia91ma19a5s61cv6a81va65sdf19v8a65a1a5s61cv6a81va65sdf19v8a65a1"
     */
    listenKey?: string;
}

interface DeleteSapiV1UserDataStreamIsolatedResponse {
}

interface DeleteSapiV1AlgoFuturesOrderParams {
    /**
     * Eg. 14511
     * int64
     */
    algoId: number;
    /**
     * The value cannot be greater than 60000
     * int64
     * @example 5000
     */
    recvWindow?: number;
}

interface DeleteSapiV1AlgoFuturesOrderResponse {
    /**
     * int64
     * @example 14511
     */
    "algoId": number;
    /**
     * @example true
     */
    "success": boolean;
    /**
     * int64
     */
    "code": number;
    /**
     * @example "OK"
     */
    "msg": string;
}

interface PutApiV3UserDataStreamParams {
    /**
     * User websocket listen key
     * @example "pqia91ma19a5s61cv6a81va65sdf19v8a65a1a5s61cv6a81va65sdf19v8a65a1"
     */
    listenKey?: string;
}

interface PutApiV3UserDataStreamResponse {
}

interface PutSapiV1UserDataStreamParams {
    /**
     * User websocket listen key
     * @example "pqia91ma19a5s61cv6a81va65sdf19v8a65a1a5s61cv6a81va65sdf19v8a65a1"
     */
    listenKey?: string;
}

interface PutSapiV1UserDataStreamResponse {
}

interface PutSapiV1UserDataStreamIsolatedParams {
    /**
     * User websocket listen key
     * @example "pqia91ma19a5s61cv6a81va65sdf19v8a65a1a5s61cv6a81va65sdf19v8a65a1"
     */
    listenKey?: string;
}

interface PutSapiV1UserDataStreamIsolatedResponse {
}

export type BinanceRouteWeight = { "IP": number } | { "UID": number };

export interface BinanceRouteOptions {
    params?: Record<string, any>;
    weight?: BinanceRouteWeight;
}

export interface BinanceRoute<A, R> {
    args: A;
    response: R;
}

export type BinanceRouteArgs<M extends keyof BinanceRoutes, P extends keyof BinanceRoutes[M]> =
    "args" extends keyof BinanceRoutes[M][P]
        ? (BinanceRoutes[M][P]["args"] extends any[]
            ? BinanceRoutes[M][P]["args"]
            : never)
        : never;

export type BinanceRouteResponse<M extends keyof BinanceRoutes, P extends keyof BinanceRoutes[M]> =
    "response" extends keyof BinanceRoutes[M][P]
        ? BinanceRoutes[M][P]["response"]
        : never;

export interface BinanceRoutes {
    "GET": {
        "/api/v3/ping": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], GetApiV3PingResponse>;
        "/api/v3/time": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], GetApiV3TimeResponse>;
        "/api/v3/exchangeInfo": BinanceRoute<[
            options?: {
                params?: GetApiV3ExchangeInfoParams;
                weight?: BinanceRouteWeight;
            }
        ], GetApiV3ExchangeInfoResponse>;
        "/api/v3/depth": BinanceRoute<[
            options: {
                params: GetApiV3DepthParams;
                weight: BinanceRouteWeight;
            }
        ], GetApiV3DepthResponse>;
        "/api/v3/trades": BinanceRoute<[
            options: {
                params: GetApiV3TradesParams;
                weight?: BinanceRouteWeight;
            }
        ], Trade[]>;
        "/api/v3/historicalTrades": BinanceRoute<[
            options: {
                params: GetApiV3HistoricalTradesParams;
                weight?: BinanceRouteWeight;
            }
        ], Trade[]>;
        "/api/v3/aggTrades": BinanceRoute<[
            options: {
                params: GetApiV3AggTradesParams;
                weight?: BinanceRouteWeight;
            }
        ], AggTrade[]>;
        "/api/v3/klines": BinanceRoute<[
            options: {
                params: GetApiV3KlinesParams;
                weight?: BinanceRouteWeight;
            }
        ], (number | string)[][]>;
        "/api/v3/uiKlines": BinanceRoute<[
            options: {
                params: GetApiV3UiKlinesParams;
                weight?: BinanceRouteWeight;
            }
        ], (number | string)[][]>;
        "/api/v3/avgPrice": BinanceRoute<[
            options: {
                params: GetApiV3AvgPriceParams;
                weight?: BinanceRouteWeight;
            }
        ], GetApiV3AvgPriceResponse>;
        "/api/v3/ticker/24hr": BinanceRoute<[
            options: {
                params?: GetApiV3Ticker24HrParams;
                weight: BinanceRouteWeight;
            }
        ], Ticker | TickerList>;
        "/api/v3/ticker/price": BinanceRoute<[
            options: {
                params?: GetApiV3TickerPriceParams;
                weight: BinanceRouteWeight;
            }
        ], PriceTicker | PriceTickerList>;
        "/api/v3/ticker/bookTicker": BinanceRoute<[
            options: {
                params?: GetApiV3TickerBookTickerParams;
                weight: BinanceRouteWeight;
            }
        ], BookTicker | BookTickerList>;
        "/api/v3/ticker": BinanceRoute<[
            options: {
                params?: GetApiV3TickerParams;
                weight: BinanceRouteWeight;
            }
        ], GetApiV3TickerResponse>;
        "/api/v3/order": BinanceRoute<[
            options: {
                params: GetApiV3OrderParams;
                weight?: BinanceRouteWeight;
            }
        ], OrderDetails>;
        "/api/v3/openOrders": BinanceRoute<[
            options: {
                params: GetApiV3OpenOrdersParams;
                weight: BinanceRouteWeight;
            }
        ], OrderDetails[]>;
        "/api/v3/allOrders": BinanceRoute<[
            options: {
                params: GetApiV3AllOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], OrderDetails[]>;
        "/api/v3/orderList": BinanceRoute<[
            options: {
                params: GetApiV3OrderListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetApiV3OrderListResponse>;
        "/api/v3/allOrderList": BinanceRoute<[
            options: {
                params: GetApiV3AllOrderListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetApiV3AllOrderListResponseItem[]>;
        "/api/v3/openOrderList": BinanceRoute<[
            options: {
                params: GetApiV3OpenOrderListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetApiV3OpenOrderListResponseItem[]>;
        "/api/v3/account": BinanceRoute<[
            options: {
                params: GetApiV3AccountParams;
                weight?: BinanceRouteWeight;
            }
        ], Account>;
        "/api/v3/myTrades": BinanceRoute<[
            options: {
                params: GetApiV3MyTradesParams;
                weight?: BinanceRouteWeight;
            }
        ], MyTrade[]>;
        "/api/v3/rateLimit/order": BinanceRoute<[
            options: {
                params: GetApiV3RateLimitOrderParams;
                weight?: BinanceRouteWeight;
            }
        ], GetApiV3RateLimitOrderResponseItem[]>;
        "/sapi/v1/margin/transfer": BinanceRoute<[
            options: {
                params: GetSapiV1MarginTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginTransferResponse>;
        "/sapi/v1/margin/loan": BinanceRoute<[
            options: {
                params: GetSapiV1MarginLoanParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginLoanResponse>;
        "/sapi/v1/margin/repay": BinanceRoute<[
            options: {
                params: GetSapiV1MarginRepayParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginRepayResponse>;
        "/sapi/v1/margin/asset": BinanceRoute<[
            options: {
                params: GetSapiV1MarginAssetParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginAssetResponse>;
        "/sapi/v1/margin/pair": BinanceRoute<[
            options: {
                params: GetSapiV1MarginPairParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginPairResponse>;
        "/sapi/v1/margin/allAssets": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginAllAssetsResponseItem[]>;
        "/sapi/v1/margin/allPairs": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginAllPairsResponseItem[]>;
        "/sapi/v1/margin/priceIndex": BinanceRoute<[
            options: {
                params: GetSapiV1MarginPriceIndexParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginPriceIndexResponse>;
        "/sapi/v1/margin/order": BinanceRoute<[
            options: {
                params: GetSapiV1MarginOrderParams;
                weight?: BinanceRouteWeight;
            }
        ], MarginOrderDetail>;
        "/sapi/v1/margin/interestHistory": BinanceRoute<[
            options: {
                params: GetSapiV1MarginInterestHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginInterestHistoryResponse>;
        "/sapi/v1/margin/forceLiquidationRec": BinanceRoute<[
            options: {
                params: GetSapiV1MarginForceLiquidationRecParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginForceLiquidationRecResponse>;
        "/sapi/v1/margin/account": BinanceRoute<[
            options: {
                params: GetSapiV1MarginAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginAccountResponse>;
        "/sapi/v1/margin/openOrders": BinanceRoute<[
            options: {
                params: GetSapiV1MarginOpenOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], MarginOrderDetail[]>;
        "/sapi/v1/margin/allOrders": BinanceRoute<[
            options: {
                params: GetSapiV1MarginAllOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], MarginOrderDetail[]>;
        "/sapi/v1/margin/orderList": BinanceRoute<[
            options: {
                params: GetSapiV1MarginOrderListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginOrderListResponse>;
        "/sapi/v1/margin/allOrderList": BinanceRoute<[
            options: {
                params: GetSapiV1MarginAllOrderListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginAllOrderListResponseItem[]>;
        "/sapi/v1/margin/openOrderList": BinanceRoute<[
            options: {
                params: GetSapiV1MarginOpenOrderListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginOpenOrderListResponseItem[]>;
        "/sapi/v1/margin/myTrades": BinanceRoute<[
            options: {
                params: GetSapiV1MarginMyTradesParams;
                weight?: BinanceRouteWeight;
            }
        ], MarginTrade[]>;
        "/sapi/v1/margin/maxBorrowable": BinanceRoute<[
            options: {
                params: GetSapiV1MarginMaxBorrowableParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginMaxBorrowableResponse>;
        "/sapi/v1/margin/maxTransferable": BinanceRoute<[
            options: {
                params: GetSapiV1MarginMaxTransferableParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginMaxTransferableResponse>;
        "/sapi/v1/margin/tradeCoeff": BinanceRoute<[
            options: {
                params: GetSapiV1MarginTradeCoeffParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginTradeCoeffResponse>;
        "/sapi/v1/margin/isolated/transfer": BinanceRoute<[
            options: {
                params: GetSapiV1MarginIsolatedTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], MarginTransferDetails>;
        "/sapi/v1/margin/isolated/account": BinanceRoute<[
            options: {
                params: GetSapiV1MarginIsolatedAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], IsolatedMarginAccountInfo>;
        "/sapi/v1/margin/isolated/accountLimit": BinanceRoute<[
            options: {
                params: GetSapiV1MarginIsolatedAccountLimitParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginIsolatedAccountLimitResponse>;
        "/sapi/v1/margin/isolated/pair": BinanceRoute<[
            options: {
                params: GetSapiV1MarginIsolatedPairParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginIsolatedPairResponse>;
        "/sapi/v1/margin/isolated/allPairs": BinanceRoute<[
            options: {
                params: GetSapiV1MarginIsolatedAllPairsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginIsolatedAllPairsResponseItem[]>;
        "/sapi/v1/bnbBurn": BinanceRoute<[
            options: {
                params: GetSapiV1BnbBurnParams;
                weight?: BinanceRouteWeight;
            }
        ], BnbBurnStatus>;
        "/sapi/v1/margin/interestRateHistory": BinanceRoute<[
            options: {
                params: GetSapiV1MarginInterestRateHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginInterestRateHistoryResponseItem[]>;
        "/sapi/v1/margin/crossMarginData": BinanceRoute<[
            options: {
                params: GetSapiV1MarginCrossMarginDataParams;
                weight: BinanceRouteWeight;
            }
        ], GetSapiV1MarginCrossMarginDataResponseItem[]>;
        "/sapi/v1/margin/isolatedMarginData": BinanceRoute<[
            options: {
                params: GetSapiV1MarginIsolatedMarginDataParams;
                weight: BinanceRouteWeight;
            }
        ], GetSapiV1MarginIsolatedMarginDataResponseItem[]>;
        "/sapi/v1/margin/isolatedMarginTier": BinanceRoute<[
            options: {
                params: GetSapiV1MarginIsolatedMarginTierParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginIsolatedMarginTierResponseItem[]>;
        "/sapi/v1/margin/rateLimit/order": BinanceRoute<[
            options: {
                params: GetSapiV1MarginRateLimitOrderParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginRateLimitOrderResponseItem[]>;
        "/sapi/v1/margin/dribblet": BinanceRoute<[
            options: {
                params: GetSapiV1MarginDribbletParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MarginDribbletResponse>;
        "/sapi/v1/system/status": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SystemStatusResponse>;
        "/sapi/v1/capital/config/getall": BinanceRoute<[
            options: {
                params: GetSapiV1CapitalConfigGetallParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1CapitalConfigGetallResponseItem[]>;
        "/sapi/v1/accountSnapshot": BinanceRoute<[
            options: {
                params: GetSapiV1AccountSnapshotParams;
                weight?: BinanceRouteWeight;
            }
        ], SnapshotSpot | SnapshotMargin | SnapshotFutures>;
        "/sapi/v1/capital/deposit/hisrec": BinanceRoute<[
            options: {
                params: GetSapiV1CapitalDepositHisrecParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1CapitalDepositHisrecResponseItem[]>;
        "/sapi/v1/capital/withdraw/history": BinanceRoute<[
            options: {
                params: GetSapiV1CapitalWithdrawHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1CapitalWithdrawHistoryResponseItem[]>;
        "/sapi/v1/capital/deposit/address": BinanceRoute<[
            options: {
                params: GetSapiV1CapitalDepositAddressParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1CapitalDepositAddressResponse>;
        "/sapi/v1/account/status": BinanceRoute<[
            options: {
                params: GetSapiV1AccountStatusParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AccountStatusResponse>;
        "/sapi/v1/account/apiTradingStatus": BinanceRoute<[
            options: {
                params: GetSapiV1AccountApiTradingStatusParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AccountApiTradingStatusResponse>;
        "/sapi/v1/asset/dribblet": BinanceRoute<[
            options: {
                params: GetSapiV1AssetDribbletParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AssetDribbletResponse>;
        "/sapi/v1/asset/assetDividend": BinanceRoute<[
            options: {
                params: GetSapiV1AssetAssetDividendParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AssetAssetDividendResponse>;
        "/sapi/v1/asset/assetDetail": BinanceRoute<[
            options: {
                params: GetSapiV1AssetAssetDetailParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AssetAssetDetailResponse>;
        "/sapi/v1/asset/tradeFee": BinanceRoute<[
            options: {
                params: GetSapiV1AssetTradeFeeParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AssetTradeFeeResponseItem[]>;
        "/sapi/v1/asset/transfer": BinanceRoute<[
            options: {
                params: GetSapiV1AssetTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AssetTransferResponse>;
        "/sapi/v1/account/apiRestrictions": BinanceRoute<[
            options: {
                params: GetSapiV1AccountApiRestrictionsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AccountApiRestrictionsResponse>;
        "/sapi/v1/sub-account/list": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountListResponse>;
        "/sapi/v1/sub-account/sub/transfer/history": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountSubTransferHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountSubTransferHistoryResponseItem[]>;
        "/sapi/v1/sub-account/futures/internalTransfer": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountFuturesInternalTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountFuturesInternalTransferResponse>;
        "/sapi/v3/sub-account/assets": BinanceRoute<[
            options: {
                params: GetSapiV3SubAccountAssetsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV3SubAccountAssetsResponse>;
        "/sapi/v1/sub-account/spotSummary": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountSpotSummaryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountSpotSummaryResponse>;
        "/sapi/v1/capital/deposit/subAddress": BinanceRoute<[
            options: {
                params: GetSapiV1CapitalDepositSubAddressParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1CapitalDepositSubAddressResponse>;
        "/sapi/v1/capital/deposit/subHisrec": BinanceRoute<[
            options: {
                params: GetSapiV1CapitalDepositSubHisrecParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1CapitalDepositSubHisrecResponseItem[]>;
        "/sapi/v1/sub-account/status": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountStatusParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountStatusResponseItem[]>;
        "/sapi/v1/sub-account/margin/account": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountMarginAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountMarginAccountResponse>;
        "/sapi/v1/sub-account/margin/accountSummary": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountMarginAccountSummaryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountMarginAccountSummaryResponse>;
        "/sapi/v1/sub-account/futures/account": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountFuturesAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountFuturesAccountResponse>;
        "/sapi/v1/sub-account/futures/accountSummary": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountFuturesAccountSummaryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountFuturesAccountSummaryResponse>;
        "/sapi/v1/sub-account/futures/positionRisk": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountFuturesPositionRiskParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountFuturesPositionRiskResponseItem[]>;
        "/sapi/v1/sub-account/transfer/subUserHistory": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountTransferSubUserHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountTransferSubUserHistoryResponseItem[]>;
        "/sapi/v1/sub-account/universalTransfer": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountUniversalTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountUniversalTransferResponseItem[]>;
        "/sapi/v2/sub-account/futures/account": BinanceRoute<[
            options: {
                params: GetSapiV2SubAccountFuturesAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], SubAccountUSDTFuturesDetails | SubAccountCOINFuturesDetails>;
        "/sapi/v2/sub-account/futures/accountSummary": BinanceRoute<[
            options: {
                params: GetSapiV2SubAccountFuturesAccountSummaryParams;
                weight?: BinanceRouteWeight;
            }
        ], SubAccountUSDTFuturesSummary | SubAccountCOINFuturesSummary>;
        "/sapi/v2/sub-account/futures/positionRisk": BinanceRoute<[
            options: {
                params: GetSapiV2SubAccountFuturesPositionRiskParams;
                weight?: BinanceRouteWeight;
            }
        ], SubAccountUSDTFuturesPositionRisk | SubAccountCOINFuturesPositionRisk>;
        "/sapi/v1/managed-subaccount/asset": BinanceRoute<[
            options: {
                params: GetSapiV1ManagedSubaccountAssetParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1ManagedSubaccountAssetResponseItem[]>;
        "/sapi/v1/managed-subaccount/accountSnapshot": BinanceRoute<[
            options: {
                params: GetSapiV1ManagedSubaccountAccountSnapshotParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1ManagedSubaccountAccountSnapshotResponse>;
        "/sapi/v1/sub-account/subAccountApi/ipRestriction": BinanceRoute<[
            options: {
                params: GetSapiV1SubAccountSubAccountApiIpRestrictionParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1SubAccountSubAccountApiIpRestrictionResponse>;
        "/sapi/v1/fiat/orders": BinanceRoute<[
            options: {
                params: GetSapiV1FiatOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1FiatOrdersResponse>;
        "/sapi/v1/fiat/payments": BinanceRoute<[
            options: {
                params: GetSapiV1FiatPaymentsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1FiatPaymentsResponse>;
        "/sapi/v1/lending/daily/product/list": BinanceRoute<[
            options: {
                params: GetSapiV1LendingDailyProductListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LendingDailyProductListResponseItem[]>;
        "/sapi/v1/lending/daily/userLeftQuota": BinanceRoute<[
            options: {
                params: GetSapiV1LendingDailyUserLeftQuotaParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LendingDailyUserLeftQuotaResponse>;
        "/sapi/v1/lending/daily/userRedemptionQuota": BinanceRoute<[
            options: {
                params: GetSapiV1LendingDailyUserRedemptionQuotaParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LendingDailyUserRedemptionQuotaResponse>;
        "/sapi/v1/lending/daily/token/position": BinanceRoute<[
            options: {
                params: GetSapiV1LendingDailyTokenPositionParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LendingDailyTokenPositionResponseItem[]>;
        "/sapi/v1/lending/project/list": BinanceRoute<[
            options: {
                params: GetSapiV1LendingProjectListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LendingProjectListResponseItem[]>;
        "/sapi/v1/lending/project/position/list": BinanceRoute<[
            options: {
                params: GetSapiV1LendingProjectPositionListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LendingProjectPositionListResponseItem[]>;
        "/sapi/v1/lending/union/account": BinanceRoute<[
            options: {
                params: GetSapiV1LendingUnionAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LendingUnionAccountResponse>;
        "/sapi/v1/lending/union/purchaseRecord": BinanceRoute<[
            options: {
                params: GetSapiV1LendingUnionPurchaseRecordParams;
                weight: BinanceRouteWeight;
            }
        ], SavingsFlexiblePurchaseRecord | SavingsFixedActivityPurchaseRecord>;
        "/sapi/v1/lending/union/redemptionRecord": BinanceRoute<[
            options: {
                params: GetSapiV1LendingUnionRedemptionRecordParams;
                weight?: BinanceRouteWeight;
            }
        ], SavingsFlexibleRedemptionRecord | SavingsFixedActivityRedemptionRecord>;
        "/sapi/v1/lending/union/interestHistory": BinanceRoute<[
            options: {
                params: GetSapiV1LendingUnionInterestHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LendingUnionInterestHistoryResponseItem[]>;
        "/sapi/v1/staking/productList": BinanceRoute<[
            options: {
                params: GetSapiV1StakingProductListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1StakingProductListResponseItem[]>;
        "/sapi/v1/staking/position": BinanceRoute<[
            options: {
                params: GetSapiV1StakingPositionParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1StakingPositionResponseItem[]>;
        "/sapi/v1/staking/stakingRecord": BinanceRoute<[
            options: {
                params: GetSapiV1StakingStakingRecordParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1StakingStakingRecordResponseItem[]>;
        "/sapi/v1/staking/personalLeftQuota": BinanceRoute<[
            options: {
                params: GetSapiV1StakingPersonalLeftQuotaParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1StakingPersonalLeftQuotaResponseItem[]>;
        "/sapi/v1/mining/pub/algoList": BinanceRoute<[
            options: {
                params: GetSapiV1MiningPubAlgoListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningPubAlgoListResponse>;
        "/sapi/v1/mining/pub/coinList": BinanceRoute<[
            options: {
                params: GetSapiV1MiningPubCoinListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningPubCoinListResponse>;
        "/sapi/v1/mining/worker/detail": BinanceRoute<[
            options: {
                params: GetSapiV1MiningWorkerDetailParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningWorkerDetailResponse>;
        "/sapi/v1/mining/worker/list": BinanceRoute<[
            options: {
                params: GetSapiV1MiningWorkerListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningWorkerListResponse>;
        "/sapi/v1/mining/payment/list": BinanceRoute<[
            options: {
                params: GetSapiV1MiningPaymentListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningPaymentListResponse>;
        "/sapi/v1/mining/payment/other": BinanceRoute<[
            options: {
                params: GetSapiV1MiningPaymentOtherParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningPaymentOtherResponse>;
        "/sapi/v1/mining/hash-transfer/config/details/list": BinanceRoute<[
            options: {
                params: GetSapiV1MiningHashTransferConfigDetailsListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningHashTransferConfigDetailsListResponse>;
        "/sapi/v1/mining/hash-transfer/profit/details": BinanceRoute<[
            options: {
                params: GetSapiV1MiningHashTransferProfitDetailsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningHashTransferProfitDetailsResponse>;
        "/sapi/v1/mining/statistics/user/status": BinanceRoute<[
            options: {
                params: GetSapiV1MiningStatisticsUserStatusParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningStatisticsUserStatusResponse>;
        "/sapi/v1/mining/statistics/user/list": BinanceRoute<[
            options: {
                params: GetSapiV1MiningStatisticsUserListParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningStatisticsUserListResponse>;
        "/sapi/v1/mining/payment/uid": BinanceRoute<[
            options: {
                params: GetSapiV1MiningPaymentUidParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1MiningPaymentUidResponse>;
        "/sapi/v1/futures/transfer": BinanceRoute<[
            options: {
                params: GetSapiV1FuturesTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1FuturesTransferResponse>;
        "/sapi/v1/futures/loan/borrow/history": BinanceRoute<[
            options: {
                params: GetSapiV1FuturesLoanBorrowHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1FuturesLoanBorrowHistoryResponse>;
        "/sapi/v1/futures/loan/repay/history": BinanceRoute<[
            options: {
                params: GetSapiV1FuturesLoanRepayHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1FuturesLoanRepayHistoryResponse>;
        "/sapi/v2/futures/loan/wallet": BinanceRoute<[
            options: {
                params: GetSapiV2FuturesLoanWalletParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV2FuturesLoanWalletResponse>;
        "/sapi/v1/futures/loan/adjustCollateral/history": BinanceRoute<[
            options: {
                params: GetSapiV1FuturesLoanAdjustCollateralHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1FuturesLoanAdjustCollateralHistoryResponse>;
        "/sapi/v1/futures/loan/liquidationHistory": BinanceRoute<[
            options: {
                params: GetSapiV1FuturesLoanLiquidationHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1FuturesLoanLiquidationHistoryResponse>;
        "/sapi/v1/futures/loan/interestHistory": BinanceRoute<[
            options: {
                params: GetSapiV1FuturesLoanInterestHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1FuturesLoanInterestHistoryResponse>;
        "/sapi/v1/algo/futures/openOrders": BinanceRoute<[
            options: {
                params: GetSapiV1AlgoFuturesOpenOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AlgoFuturesOpenOrdersResponse>;
        "/sapi/v1/algo/futures/historicalOrders": BinanceRoute<[
            options: {
                params: GetSapiV1AlgoFuturesHistoricalOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AlgoFuturesHistoricalOrdersResponse>;
        "/sapi/v1/algo/futures/subOrders": BinanceRoute<[
            options: {
                params: GetSapiV1AlgoFuturesSubOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1AlgoFuturesSubOrdersResponse>;
        "/sapi/v1/portfolio/account": BinanceRoute<[
            options: {
                params: GetSapiV1PortfolioAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1PortfolioAccountResponse>;
        "/sapi/v1/portfolio/collateralRate": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1PortfolioCollateralRateResponseItem[]>;
        "/sapi/v1/portfolio/pmLoan": BinanceRoute<[
            options: {
                params: GetSapiV1PortfolioPmLoanParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1PortfolioPmLoanResponse>;
        "/sapi/v1/blvt/tokenInfo": BinanceRoute<[
            options?: {
                params?: GetSapiV1BlvtTokenInfoParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BlvtTokenInfoResponseItem[]>;
        "/sapi/v1/blvt/subscribe/record": BinanceRoute<[
            options: {
                params: GetSapiV1BlvtSubscribeRecordParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BlvtSubscribeRecordResponse>;
        "/sapi/v1/blvt/redeem/record": BinanceRoute<[
            options: {
                params: GetSapiV1BlvtRedeemRecordParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BlvtRedeemRecordResponseItem[]>;
        "/sapi/v1/blvt/userLimit": BinanceRoute<[
            options: {
                params: GetSapiV1BlvtUserLimitParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BlvtUserLimitResponseItem[]>;
        "/sapi/v1/bswap/pools": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BswapPoolsResponseItem[]>;
        "/sapi/v1/bswap/liquidity": BinanceRoute<[
            options: {
                params: GetSapiV1BswapLiquidityParams;
                weight: BinanceRouteWeight;
            }
        ], GetSapiV1BswapLiquidityResponseItem[]>;
        "/sapi/v1/bswap/liquidityOps": BinanceRoute<[
            options: {
                params: GetSapiV1BswapLiquidityOpsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BswapLiquidityOpsResponseItem[]>;
        "/sapi/v1/bswap/quote": BinanceRoute<[
            options: {
                params: GetSapiV1BswapQuoteParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BswapQuoteResponse>;
        "/sapi/v1/bswap/swap": BinanceRoute<[
            options: {
                params: GetSapiV1BswapSwapParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BswapSwapResponseItem[]>;
        "/sapi/v1/bswap/poolConfigure": BinanceRoute<[
            options: {
                params: GetSapiV1BswapPoolConfigureParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BswapPoolConfigureResponseItem[]>;
        "/sapi/v1/bswap/addLiquidityPreview": BinanceRoute<[
            options: {
                params: GetSapiV1BswapAddLiquidityPreviewParams;
                weight?: BinanceRouteWeight;
            }
        ], BswapAddLiquidityPreviewCombination | BswapAddLiquidityPreviewSingle>;
        "/sapi/v1/bswap/removeLiquidityPreview": BinanceRoute<[
            options: {
                params: GetSapiV1BswapRemoveLiquidityPreviewParams;
                weight?: BinanceRouteWeight;
            }
        ], BswapRmvLiquidityPreviewCombination | BswapRmvLiquidityPreviewSingle>;
        "/sapi/v1/bswap/unclaimedRewards": BinanceRoute<[
            options: {
                params: GetSapiV1BswapUnclaimedRewardsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BswapUnclaimedRewardsResponse>;
        "/sapi/v1/bswap/claimedHistory": BinanceRoute<[
            options: {
                params: GetSapiV1BswapClaimedHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1BswapClaimedHistoryResponseItem[]>;
        "/sapi/v1/c2c/orderMatch/listUserOrderHistory": BinanceRoute<[
            options: {
                params: GetSapiV1C2COrderMatchListUserOrderHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1C2COrderMatchListUserOrderHistoryResponse>;
        "/sapi/v1/loan/income": BinanceRoute<[
            options: {
                params: GetSapiV1LoanIncomeParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LoanIncomeResponseItem[]>;
        "/sapi/v1/loan/borrow/history": BinanceRoute<[
            options: {
                params: GetSapiV1LoanBorrowHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LoanBorrowHistoryResponse>;
        "/sapi/v1/loan/ongoing/orders": BinanceRoute<[
            options: {
                params: GetSapiV1LoanOngoingOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LoanOngoingOrdersResponse>;
        "/sapi/v1/loan/repay/history": BinanceRoute<[
            options: {
                params: GetSapiV1LoanRepayHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LoanRepayHistoryResponse>;
        "/sapi/v1/loan/ltv/adjustment/history": BinanceRoute<[
            options: {
                params: GetSapiV1LoanLtvAdjustmentHistoryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1LoanLtvAdjustmentHistoryResponse>;
        "/sapi/v1/pay/transactions": BinanceRoute<[
            options: {
                params: GetSapiV1PayTransactionsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1PayTransactionsResponse>;
        "/sapi/v1/convert/tradeFlow": BinanceRoute<[
            options: {
                params: GetSapiV1ConvertTradeFlowParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1ConvertTradeFlowResponse>;
        "/sapi/v1/rebate/taxQuery": BinanceRoute<[
            options: {
                params: GetSapiV1RebateTaxQueryParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1RebateTaxQueryResponse>;
        "/sapi/v1/nft/history/transactions": BinanceRoute<[
            options: {
                params: GetSapiV1NftHistoryTransactionsParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1NftHistoryTransactionsResponse>;
        "/sapi/v1/nft/history/deposit": BinanceRoute<[
            options: {
                params: GetSapiV1NftHistoryDepositParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1NftHistoryDepositResponse>;
        "/sapi/v1/nft/history/withdraw": BinanceRoute<[
            options: {
                params: GetSapiV1NftHistoryWithdrawParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1NftHistoryWithdrawResponse>;
        "/sapi/v1/nft/user/getAsset": BinanceRoute<[
            options: {
                params: GetSapiV1NftUserGetAssetParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1NftUserGetAssetResponse>;
        "/sapi/v1/giftcard/verify": BinanceRoute<[
            options: {
                params: GetSapiV1GiftcardVerifyParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1GiftcardVerifyResponse>;
        "/sapi/v1/giftcard/cryptography/rsa-public-key": BinanceRoute<[
            options: {
                params: GetSapiV1GiftcardCryptographyRsaPublicKeyParams;
                weight?: BinanceRouteWeight;
            }
        ], GetSapiV1GiftcardCryptographyRsaPublicKeyResponse>;
    };
    "POST": {
        "/api/v3/order/test": BinanceRoute<[
            options: {
                params: PostApiV3OrderTestParams;
                weight?: BinanceRouteWeight;
            }
        ], PostApiV3OrderTestResponse>;
        "/api/v3/order": BinanceRoute<[
            options: {
                params: PostApiV3OrderParams;
                weight?: BinanceRouteWeight;
            }
        ], OrderResponseAck | OrderResponseResult | OrderResponseFull>;
        "/api/v3/order/cancelReplace": BinanceRoute<[
            options: {
                params: PostApiV3OrderCancelReplaceParams;
                weight?: BinanceRouteWeight;
            }
        ], PostApiV3OrderCancelReplaceResponse>;
        "/api/v3/order/oco": BinanceRoute<[
            options: {
                params: PostApiV3OrderOcoParams;
                weight?: BinanceRouteWeight;
            }
        ], PostApiV3OrderOcoResponse>;
        "/sapi/v1/margin/transfer": BinanceRoute<[
            options: {
                params: PostSapiV1MarginTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], Transaction>;
        "/sapi/v1/margin/loan": BinanceRoute<[
            options: {
                params: PostSapiV1MarginLoanParams;
                weight?: BinanceRouteWeight;
            }
        ], Transaction>;
        "/sapi/v1/margin/repay": BinanceRoute<[
            options: {
                params: PostSapiV1MarginRepayParams;
                weight?: BinanceRouteWeight;
            }
        ], Transaction>;
        "/sapi/v1/margin/order": BinanceRoute<[
            options: {
                params: PostSapiV1MarginOrderParams;
                weight?: BinanceRouteWeight;
            }
        ], MarginOrderResponseAck | MarginOrderResponseResult | MarginOrderResponseFull>;
        "/sapi/v1/margin/order/oco": BinanceRoute<[
            options: {
                params: PostSapiV1MarginOrderOcoParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1MarginOrderOcoResponse>;
        "/sapi/v1/margin/isolated/transfer": BinanceRoute<[
            options: {
                params: PostSapiV1MarginIsolatedTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1MarginIsolatedTransferResponse>;
        "/sapi/v1/margin/isolated/account": BinanceRoute<[
            options: {
                params: PostSapiV1MarginIsolatedAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1MarginIsolatedAccountResponse>;
        "/sapi/v1/bnbBurn": BinanceRoute<[
            options: {
                params: PostSapiV1BnbBurnParams;
                weight?: BinanceRouteWeight;
            }
        ], BnbBurnStatus>;
        "/sapi/v1/account/disableFastWithdrawSwitch": BinanceRoute<[
            options: {
                params: PostSapiV1AccountDisableFastWithdrawSwitchParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AccountDisableFastWithdrawSwitchResponse>;
        "/sapi/v1/account/enableFastWithdrawSwitch": BinanceRoute<[
            options: {
                params: PostSapiV1AccountEnableFastWithdrawSwitchParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AccountEnableFastWithdrawSwitchResponse>;
        "/sapi/v1/capital/withdraw/apply": BinanceRoute<[
            options: {
                params: PostSapiV1CapitalWithdrawApplyParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1CapitalWithdrawApplyResponse>;
        "/sapi/v1/asset/dust-btc": BinanceRoute<[
            options: {
                params: PostSapiV1AssetDustBtcParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AssetDustBtcResponse>;
        "/sapi/v1/asset/dust": BinanceRoute<[
            options: {
                params: PostSapiV1AssetDustParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AssetDustResponse>;
        "/sapi/v1/asset/transfer": BinanceRoute<[
            options: {
                params: PostSapiV1AssetTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AssetTransferResponse>;
        "/sapi/v1/asset/get-funding-asset": BinanceRoute<[
            options: {
                params: PostSapiV1AssetGetFundingAssetParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AssetGetFundingAssetResponseItem[]>;
        "/sapi/v3/asset/getUserAsset": BinanceRoute<[
            options: {
                params: PostSapiV3AssetGetUserAssetParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV3AssetGetUserAssetResponseItem[]>;
        "/sapi/v1/asset/convert-transfer": BinanceRoute<[
            options: {
                params: PostSapiV1AssetConvertTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AssetConvertTransferResponse>;
        "/sapi/v1/asset/convert-transfer/queryByPage": BinanceRoute<[
            options: {
                params: PostSapiV1AssetConvertTransferQueryByPageParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AssetConvertTransferQueryByPageResponse>;
        "/sapi/v1/sub-account/virtualSubAccount": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountVirtualSubAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountVirtualSubAccountResponse>;
        "/sapi/v1/sub-account/futures/internalTransfer": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountFuturesInternalTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountFuturesInternalTransferResponse>;
        "/sapi/v1/sub-account/margin/enable": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountMarginEnableParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountMarginEnableResponse>;
        "/sapi/v1/sub-account/futures/enable": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountFuturesEnableParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountFuturesEnableResponse>;
        "/sapi/v1/sub-account/futures/transfer": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountFuturesTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountFuturesTransferResponse>;
        "/sapi/v1/sub-account/margin/transfer": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountMarginTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountMarginTransferResponse>;
        "/sapi/v1/sub-account/transfer/subToSub": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountTransferSubToSubParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountTransferSubToSubResponse>;
        "/sapi/v1/sub-account/transfer/subToMaster": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountTransferSubToMasterParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountTransferSubToMasterResponse>;
        "/sapi/v1/sub-account/universalTransfer": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountUniversalTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountUniversalTransferResponse>;
        "/sapi/v1/sub-account/blvt/enable": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountBlvtEnableParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountBlvtEnableResponse>;
        "/sapi/v1/managed-subaccount/deposit": BinanceRoute<[
            options: {
                params: PostSapiV1ManagedSubaccountDepositParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1ManagedSubaccountDepositResponse>;
        "/sapi/v1/managed-subaccount/withdraw": BinanceRoute<[
            options: {
                params: PostSapiV1ManagedSubaccountWithdrawParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1ManagedSubaccountWithdrawResponse>;
        "/sapi/v1/sub-account/subAccountApi/ipRestriction": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountSubAccountApiIpRestrictionParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountSubAccountApiIpRestrictionResponse>;
        "/sapi/v1/sub-account/subAccountApi/ipRestriction/ipList": BinanceRoute<[
            options: {
                params: PostSapiV1SubAccountSubAccountApiIpRestrictionIpListParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1SubAccountSubAccountApiIpRestrictionIpListResponse>;
        "/api/v3/userDataStream": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], PostApiV3UserDataStreamResponse>;
        "/sapi/v1/userDataStream": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1UserDataStreamResponse>;
        "/sapi/v1/userDataStream/isolated": BinanceRoute<[
            options?: {
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1UserDataStreamIsolatedResponse>;
        "/sapi/v1/lending/daily/purchase": BinanceRoute<[
            options: {
                params: PostSapiV1LendingDailyPurchaseParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1LendingDailyPurchaseResponse>;
        "/sapi/v1/lending/daily/redeem": BinanceRoute<[
            options: {
                params: PostSapiV1LendingDailyRedeemParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1LendingDailyRedeemResponse>;
        "/sapi/v1/lending/customizedFixed/purchase": BinanceRoute<[
            options: {
                params: PostSapiV1LendingCustomizedFixedPurchaseParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1LendingCustomizedFixedPurchaseResponse>;
        "/sapi/v1/lending/positionChanged": BinanceRoute<[
            options: {
                params: PostSapiV1LendingPositionChangedParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1LendingPositionChangedResponse>;
        "/sapi/v1/staking/purchase": BinanceRoute<[
            options: {
                params: PostSapiV1StakingPurchaseParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1StakingPurchaseResponse>;
        "/sapi/v1/staking/redeem": BinanceRoute<[
            options: {
                params: PostSapiV1StakingRedeemParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1StakingRedeemResponse>;
        "/sapi/v1/staking/setAutoStaking": BinanceRoute<[
            options: {
                params: PostSapiV1StakingSetAutoStakingParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1StakingSetAutoStakingResponse>;
        "/sapi/v1/mining/hash-transfer/config": BinanceRoute<[
            options: {
                params: PostSapiV1MiningHashTransferConfigParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1MiningHashTransferConfigResponse>;
        "/sapi/v1/mining/hash-transfer/config/cancel": BinanceRoute<[
            options: {
                params: PostSapiV1MiningHashTransferConfigCancelParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1MiningHashTransferConfigCancelResponse>;
        "/sapi/v1/futures/transfer": BinanceRoute<[
            options: {
                params: PostSapiV1FuturesTransferParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1FuturesTransferResponse>;
        "/sapi/v1/algo/futures/newOrderVp": BinanceRoute<[
            options: {
                params: PostSapiV1AlgoFuturesNewOrderVpParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AlgoFuturesNewOrderVpResponse>;
        "/sapi/v1/algo/futures/newOrderTwap": BinanceRoute<[
            options: {
                params: PostSapiV1AlgoFuturesNewOrderTwapParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1AlgoFuturesNewOrderTwapResponse>;
        "/sapi/v1/portfolio/repay": BinanceRoute<[
            options: {
                params: PostSapiV1PortfolioRepayParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1PortfolioRepayResponse>;
        "/sapi/v1/blvt/subscribe": BinanceRoute<[
            options: {
                params: PostSapiV1BlvtSubscribeParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1BlvtSubscribeResponse>;
        "/sapi/v1/blvt/redeem": BinanceRoute<[
            options: {
                params: PostSapiV1BlvtRedeemParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1BlvtRedeemResponse>;
        "/sapi/v1/bswap/liquidityAdd": BinanceRoute<[
            options: {
                params: PostSapiV1BswapLiquidityAddParams;
                weight: BinanceRouteWeight;
            }
        ], PostSapiV1BswapLiquidityAddResponse>;
        "/sapi/v1/bswap/liquidityRemove": BinanceRoute<[
            options: {
                params: PostSapiV1BswapLiquidityRemoveParams;
                weight: BinanceRouteWeight;
            }
        ], PostSapiV1BswapLiquidityRemoveResponse>;
        "/sapi/v1/bswap/swap": BinanceRoute<[
            options: {
                params: PostSapiV1BswapSwapParams;
                weight: BinanceRouteWeight;
            }
        ], PostSapiV1BswapSwapResponse>;
        "/sapi/v1/bswap/claimRewards": BinanceRoute<[
            options: {
                params: PostSapiV1BswapClaimRewardsParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1BswapClaimRewardsResponse>;
        "/sapi/v1/loan/borrow": BinanceRoute<[
            options: {
                params: PostSapiV1LoanBorrowParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1LoanBorrowResponse>;
        "/sapi/v1/loan/repay": BinanceRoute<[
            options: {
                params: PostSapiV1LoanRepayParams;
                weight?: BinanceRouteWeight;
            }
        ], RepaymentInfo | RepaymentInfo2>;
        "/sapi/v1/loan/adjust/ltv": BinanceRoute<[
            options: {
                params: PostSapiV1LoanAdjustLtvParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1LoanAdjustLtvResponse>;
        "/sapi/v1/giftcard/createCode": BinanceRoute<[
            options: {
                params: PostSapiV1GiftcardCreateCodeParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1GiftcardCreateCodeResponse>;
        "/sapi/v1/giftcard/redeemCode": BinanceRoute<[
            options: {
                params: PostSapiV1GiftcardRedeemCodeParams;
                weight?: BinanceRouteWeight;
            }
        ], PostSapiV1GiftcardRedeemCodeResponse>;
    };
    "DELETE": {
        "/api/v3/order": BinanceRoute<[
            options: {
                params: DeleteApiV3OrderParams;
                weight?: BinanceRouteWeight;
            }
        ], Order>;
        "/api/v3/openOrders": BinanceRoute<[
            options: {
                params: DeleteApiV3OpenOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], (Order | OcoOrder)[]>;
        "/api/v3/orderList": BinanceRoute<[
            options: {
                params: DeleteApiV3OrderListParams;
                weight?: BinanceRouteWeight;
            }
        ], OcoOrder>;
        "/sapi/v1/margin/order": BinanceRoute<[
            options: {
                params: DeleteSapiV1MarginOrderParams;
                weight?: BinanceRouteWeight;
            }
        ], MarginOrder>;
        "/sapi/v1/margin/openOrders": BinanceRoute<[
            options: {
                params: DeleteSapiV1MarginOpenOrdersParams;
                weight?: BinanceRouteWeight;
            }
        ], (CanceledMarginOrderDetail | MarginOcoOrder)[]>;
        "/sapi/v1/margin/orderList": BinanceRoute<[
            options: {
                params: DeleteSapiV1MarginOrderListParams;
                weight?: BinanceRouteWeight;
            }
        ], MarginOcoOrder>;
        "/sapi/v1/margin/isolated/account": BinanceRoute<[
            options: {
                params: DeleteSapiV1MarginIsolatedAccountParams;
                weight?: BinanceRouteWeight;
            }
        ], DeleteSapiV1MarginIsolatedAccountResponse>;
        "/sapi/v1/sub-account/subAccountApi/ipRestriction/ipList": BinanceRoute<[
            options: {
                params: DeleteSapiV1SubAccountSubAccountApiIpRestrictionIpListParams;
                weight?: BinanceRouteWeight;
            }
        ], DeleteSapiV1SubAccountSubAccountApiIpRestrictionIpListResponse>;
        "/api/v3/userDataStream": BinanceRoute<[
            options?: {
                params?: DeleteApiV3UserDataStreamParams;
                weight?: BinanceRouteWeight;
            }
        ], DeleteApiV3UserDataStreamResponse>;
        "/sapi/v1/userDataStream": BinanceRoute<[
            options?: {
                params?: DeleteSapiV1UserDataStreamParams;
                weight?: BinanceRouteWeight;
            }
        ], DeleteSapiV1UserDataStreamResponse>;
        "/sapi/v1/userDataStream/isolated": BinanceRoute<[
            options?: {
                params?: DeleteSapiV1UserDataStreamIsolatedParams;
                weight?: BinanceRouteWeight;
            }
        ], DeleteSapiV1UserDataStreamIsolatedResponse>;
        "/sapi/v1/algo/futures/order": BinanceRoute<[
            options: {
                params: DeleteSapiV1AlgoFuturesOrderParams;
                weight?: BinanceRouteWeight;
            }
        ], DeleteSapiV1AlgoFuturesOrderResponse>;
    };
    "PUT": {
        "/api/v3/userDataStream": BinanceRoute<[
            options?: {
                params?: PutApiV3UserDataStreamParams;
                weight?: BinanceRouteWeight;
            }
        ], PutApiV3UserDataStreamResponse>;
        "/sapi/v1/userDataStream": BinanceRoute<[
            options?: {
                params?: PutSapiV1UserDataStreamParams;
                weight?: BinanceRouteWeight;
            }
        ], PutSapiV1UserDataStreamResponse>;
        "/sapi/v1/userDataStream/isolated": BinanceRoute<[
            options?: {
                params?: PutSapiV1UserDataStreamIsolatedParams;
                weight?: BinanceRouteWeight;
            }
        ], PutSapiV1UserDataStreamIsolatedResponse>;
    };
}

export var binanceRoutesConfig = {
    "GET": {
        "/api/v3/ping": {
            weight: { IP: 1 }
        },
        "/api/v3/time": {
            weight: { IP: 1 }
        },
        "/api/v3/exchangeInfo": {
            weight: { IP: 10 }
        },
        "/api/v3/depth": {},
        "/api/v3/trades": {
            weight: { IP: 1 }
        },
        "/api/v3/historicalTrades": {
            weight: { IP: 5 }
        },
        "/api/v3/aggTrades": {
            weight: { IP: 1 }
        },
        "/api/v3/klines": {
            weight: { IP: 1 }
        },
        "/api/v3/uiKlines": {
            weight: { IP: 1 }
        },
        "/api/v3/avgPrice": {
            weight: { IP: 1 }
        },
        "/api/v3/ticker/24hr": {},
        "/api/v3/ticker/price": {},
        "/api/v3/ticker/bookTicker": {},
        "/api/v3/ticker": {},
        "/api/v3/order": {
            weight: { IP: 2 },
            isSigned: true
        },
        "/api/v3/openOrders": {
            isSigned: true
        },
        "/api/v3/allOrders": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/api/v3/orderList": {
            weight: { IP: 2 },
            isSigned: true
        },
        "/api/v3/allOrderList": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/api/v3/openOrderList": {
            weight: { IP: 3 },
            isSigned: true
        },
        "/api/v3/account": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/api/v3/myTrades": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/api/v3/rateLimit/order": {
            weight: { IP: 20 },
            isSigned: true
        },
        "/sapi/v1/margin/transfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/loan": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/repay": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/asset": {
            weight: { IP: 10 }
        },
        "/sapi/v1/margin/pair": {
            weight: { IP: 10 }
        },
        "/sapi/v1/margin/allAssets": {
            weight: { IP: 1 }
        },
        "/sapi/v1/margin/allPairs": {
            weight: { IP: 1 }
        },
        "/sapi/v1/margin/priceIndex": {
            weight: { IP: 10 }
        },
        "/sapi/v1/margin/order": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/interestHistory": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/forceLiquidationRec": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/account": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/openOrders": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/allOrders": {
            weight: { IP: 200 },
            isSigned: true
        },
        "/sapi/v1/margin/orderList": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/allOrderList": {
            weight: { IP: 200 },
            isSigned: true
        },
        "/sapi/v1/margin/openOrderList": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/myTrades": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/maxBorrowable": {
            weight: { IP: 50 },
            isSigned: true
        },
        "/sapi/v1/margin/maxTransferable": {
            weight: { IP: 50 },
            isSigned: true
        },
        "/sapi/v1/margin/tradeCoeff": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/isolated/transfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/isolated/account": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/isolated/accountLimit": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/isolated/pair": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/isolated/allPairs": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/bnbBurn": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/interestRateHistory": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/crossMarginData": {
            isSigned: true
        },
        "/sapi/v1/margin/isolatedMarginData": {
            isSigned: true
        },
        "/sapi/v1/margin/isolatedMarginTier": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/rateLimit/order": {
            weight: { IP: 20 },
            isSigned: true
        },
        "/sapi/v1/margin/dribblet": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/system/status": {
            weight: { IP: 1 }
        },
        "/sapi/v1/capital/config/getall": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/accountSnapshot": {
            weight: { IP: 2400 },
            isSigned: true
        },
        "/sapi/v1/capital/deposit/hisrec": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/capital/withdraw/history": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/capital/deposit/address": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/account/status": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/account/apiTradingStatus": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/asset/dribblet": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/asset/assetDividend": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/asset/assetDetail": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/asset/tradeFee": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/asset/transfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/account/apiRestrictions": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/list": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/sub/transfer/history": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/futures/internalTransfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v3/sub-account/assets": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/spotSummary": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/capital/deposit/subAddress": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/capital/deposit/subHisrec": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/status": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/sub-account/margin/account": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/sub-account/margin/accountSummary": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/sub-account/futures/account": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/sub-account/futures/accountSummary": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/futures/positionRisk": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/sub-account/transfer/subUserHistory": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/universalTransfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v2/sub-account/futures/account": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v2/sub-account/futures/accountSummary": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v2/sub-account/futures/positionRisk": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/managed-subaccount/asset": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/managed-subaccount/accountSnapshot": {
            weight: { IP: 2400 },
            isSigned: true
        },
        "/sapi/v1/sub-account/subAccountApi/ipRestriction": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/fiat/orders": {
            weight: { UID: 90000 },
            isSigned: true
        },
        "/sapi/v1/fiat/payments": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/daily/product/list": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/daily/userLeftQuota": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/daily/userRedemptionQuota": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/daily/token/position": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/project/list": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/project/position/list": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/union/account": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/union/purchaseRecord": {
            isSigned: true
        },
        "/sapi/v1/lending/union/redemptionRecord": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/union/interestHistory": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/staking/productList": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/staking/position": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/staking/stakingRecord": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/staking/personalLeftQuota": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/mining/pub/algoList": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/mining/pub/coinList": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/mining/worker/detail": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/worker/list": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/payment/list": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/payment/other": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/hash-transfer/config/details/list": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/hash-transfer/profit/details": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/statistics/user/status": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/statistics/user/list": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/payment/uid": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/futures/transfer": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/futures/loan/borrow/history": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/futures/loan/repay/history": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v2/futures/loan/wallet": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/futures/loan/adjustCollateral/history": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/futures/loan/liquidationHistory": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/futures/loan/interestHistory": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/algo/futures/openOrders": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/algo/futures/historicalOrders": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/algo/futures/subOrders": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/portfolio/account": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/portfolio/collateralRate": {
            weight: { IP: 50 }
        },
        "/sapi/v1/portfolio/pmLoan": {
            weight: { UID: 500 },
            isSigned: true
        },
        "/sapi/v1/blvt/tokenInfo": {
            weight: { IP: 1 }
        },
        "/sapi/v1/blvt/subscribe/record": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/blvt/redeem/record": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/blvt/userLimit": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/bswap/pools": {
            weight: { IP: 1 }
        },
        "/sapi/v1/bswap/liquidity": {
            isSigned: true
        },
        "/sapi/v1/bswap/liquidityOps": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/bswap/quote": {
            weight: { UID: 150 },
            isSigned: true
        },
        "/sapi/v1/bswap/swap": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/bswap/poolConfigure": {
            weight: { IP: 150 },
            isSigned: true
        },
        "/sapi/v1/bswap/addLiquidityPreview": {
            weight: { IP: 150 },
            isSigned: true
        },
        "/sapi/v1/bswap/removeLiquidityPreview": {
            weight: { IP: 150 },
            isSigned: true
        },
        "/sapi/v1/bswap/unclaimedRewards": {
            weight: { UID: 1000 },
            isSigned: true
        },
        "/sapi/v1/bswap/claimedHistory": {
            weight: { UID: 1000 },
            isSigned: true
        },
        "/sapi/v1/c2c/orderMatch/listUserOrderHistory": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/loan/income": {
            weight: { UID: 6000 },
            isSigned: true
        },
        "/sapi/v1/loan/borrow/history": {
            weight: { IP: 400 },
            isSigned: true
        },
        "/sapi/v1/loan/ongoing/orders": {
            weight: { IP: 400 },
            isSigned: true
        },
        "/sapi/v1/loan/repay/history": {
            weight: { IP: 400 },
            isSigned: true
        },
        "/sapi/v1/loan/ltv/adjustment/history": {
            weight: { IP: 400 },
            isSigned: true
        },
        "/sapi/v1/pay/transactions": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/convert/tradeFlow": {
            weight: { UID: 100 },
            isSigned: true
        },
        "/sapi/v1/rebate/taxQuery": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/nft/history/transactions": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/nft/history/deposit": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/nft/history/withdraw": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/nft/user/getAsset": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/giftcard/verify": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/giftcard/cryptography/rsa-public-key": {
            weight: { IP: 1 },
            isSigned: true
        }
    },
    "POST": {
        "/api/v3/order/test": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/api/v3/order": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/api/v3/order/cancelReplace": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/api/v3/order/oco": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/transfer": {
            weight: { IP: 600 },
            isSigned: true
        },
        "/sapi/v1/margin/loan": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/margin/repay": {
            weight: { IP: 3000 },
            isSigned: true
        },
        "/sapi/v1/margin/order": {
            weight: { UID: 6 },
            isSigned: true
        },
        "/sapi/v1/margin/order/oco": {
            weight: { UID: 6 },
            isSigned: true
        },
        "/sapi/v1/margin/isolated/transfer": {
            weight: { UID: 600 },
            isSigned: true
        },
        "/sapi/v1/margin/isolated/account": {
            weight: { UID: 300 },
            isSigned: true
        },
        "/sapi/v1/bnbBurn": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/account/disableFastWithdrawSwitch": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/account/enableFastWithdrawSwitch": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/capital/withdraw/apply": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/asset/dust-btc": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/asset/dust": {
            weight: { UID: 10 },
            isSigned: true
        },
        "/sapi/v1/asset/transfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/asset/get-funding-asset": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v3/asset/getUserAsset": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/asset/convert-transfer": {
            weight: { UID: 5 },
            isSigned: true
        },
        "/sapi/v1/asset/convert-transfer/queryByPage": {
            weight: { UID: 5 },
            isSigned: true
        },
        "/sapi/v1/sub-account/virtualSubAccount": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/futures/internalTransfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/margin/enable": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/futures/enable": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/futures/transfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/margin/transfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/transfer/subToSub": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/transfer/subToMaster": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/universalTransfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/blvt/enable": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/managed-subaccount/deposit": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/managed-subaccount/withdraw": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/sub-account/subAccountApi/ipRestriction": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/sub-account/subAccountApi/ipRestriction/ipList": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/api/v3/userDataStream": {
            weight: { IP: 1 }
        },
        "/sapi/v1/userDataStream": {
            weight: { IP: 1 }
        },
        "/sapi/v1/userDataStream/isolated": {
            weight: { IP: 1 }
        },
        "/sapi/v1/lending/daily/purchase": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/daily/redeem": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/customizedFixed/purchase": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/lending/positionChanged": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/staking/purchase": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/staking/redeem": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/staking/setAutoStaking": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/mining/hash-transfer/config": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/mining/hash-transfer/config/cancel": {
            weight: { IP: 5 },
            isSigned: true
        },
        "/sapi/v1/futures/transfer": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/algo/futures/newOrderVp": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/algo/futures/newOrderTwap": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/portfolio/repay": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/sapi/v1/blvt/subscribe": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/blvt/redeem": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/bswap/liquidityAdd": {
            isSigned: true
        },
        "/sapi/v1/bswap/liquidityRemove": {
            isSigned: true
        },
        "/sapi/v1/bswap/swap": {
            isSigned: true
        },
        "/sapi/v1/bswap/claimRewards": {
            weight: { UID: 1000 },
            isSigned: true
        },
        "/sapi/v1/loan/borrow": {
            weight: { UID: 6000 },
            isSigned: true
        },
        "/sapi/v1/loan/repay": {
            weight: { UID: 6000 },
            isSigned: true
        },
        "/sapi/v1/loan/adjust/ltv": {
            weight: { UID: 6000 },
            isSigned: true
        },
        "/sapi/v1/giftcard/createCode": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/giftcard/redeemCode": {
            weight: { IP: 1 },
            isSigned: true
        }
    },
    "DELETE": {
        "/api/v3/order": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/api/v3/openOrders": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/api/v3/orderList": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/order": {
            weight: { IP: 10 },
            isSigned: true
        },
        "/sapi/v1/margin/openOrders": {
            weight: { IP: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/orderList": {
            weight: { UID: 1 },
            isSigned: true
        },
        "/sapi/v1/margin/isolated/account": {
            weight: { UID: 300 },
            isSigned: true
        },
        "/sapi/v1/sub-account/subAccountApi/ipRestriction/ipList": {
            weight: { UID: 3000 },
            isSigned: true
        },
        "/api/v3/userDataStream": {
            weight: { IP: 1 }
        },
        "/sapi/v1/userDataStream": {
            weight: { IP: 1 }
        },
        "/sapi/v1/userDataStream/isolated": {
            weight: { IP: 1 }
        },
        "/sapi/v1/algo/futures/order": {
            weight: { IP: 1 },
            isSigned: true
        }
    },
    "PUT": {
        "/api/v3/userDataStream": {
            weight: { IP: 1 }
        },
        "/sapi/v1/userDataStream": {
            weight: { IP: 1 }
        },
        "/sapi/v1/userDataStream/isolated": {
            weight: { IP: 1 }
        }
    }
};
