export interface BinanceSymbol {
	symbol: string;
	baseAsset: string;
	quoteAsset: string;
}

export interface BinanceRateLimit {
	rateLimitType: string;
	interval: string;
	intervalNum: number;
	limit: number;
}

export interface BinanceExchangeInfo {
	timezone: string;
	serverTime: number;
	rateLimits: BinanceRateLimit[];
	symbols: BinanceSymbol[];
}
