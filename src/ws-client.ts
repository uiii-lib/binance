import { Dictionary, forEach, map, omit } from "lodash";
import WebSocket from "ws";
import { sleep } from "../helpers/sleep";

export interface WsClientOptions {
	keepAlive: boolean;
	reconnectTimeout: number;
	requestRateLimit: number;
}

export interface WsSendOptions {
	forefront?: boolean; // prioritize this message by adding it to the beginning of the queue
}

interface WsRequest {
	message: any;
	retryCount: number;
	onSuccess: () => void;
	onFailure: () => void;
}

export class WsClient {
	private options: WsClientOptions = {
		keepAlive: false,
		reconnectTimeout: 5000,
		requestRateLimit: 0,
	};

	private ws: WebSocket|undefined;
	private listeners: Dictionary<Array<(...args: any[]) => void>> = {};

	private requestQueue: WsRequest[] = [];
	private lastRequestTime: number = 0;

	private isProcessingRequestQueue: boolean = false;
	private isConnecting: boolean = false;

	constructor(private uri: string, options: Partial<WsClientOptions> = {}) {
		this.options = {...this.options, ...options};
	}

	connect() {
		this.doConnect();
	}

	on(eventType: string, callback: (...args: any[]) => void) {
		if (!this.listeners[eventType]) {
			this.listeners[eventType] = [];
		}

		this.listeners[eventType]!.push(callback);

		!this.ws && console.log("on", this.ws);
		if (this.ws && eventType !== 'reconnect') {
			this.ws.on(eventType, callback);
		}
	}

	send(message: any, options: WsSendOptions = {}) {
		return new Promise<boolean>((resolve) => {
			const request = {
				message,
				retryCount: 0,
				onSuccess: () => resolve(true),
				onFailure: () => resolve(false)
			};

			if (options.forefront) {
				this.requestQueue.unshift(request);
			} else {
				this.requestQueue.push(request);
			}

			this.processRequestQueue();
		});
	}

	private doConnect(reconnect: boolean = false) {
		if (!this.isConnecting || reconnect) {
			this.isConnecting = true;

			// if there is already some previous ws instance
			this.ws?.removeAllListeners();
			this.ws?.terminate();

			console.log("start connect");
			this.ws = new WebSocket(this.uri);

			this.ws.on('open', () => {
				console.log("ws: connected");

				if (reconnect) {
					this.listeners['reconnect']?.forEach(callback => callback());
				}

				this.isConnecting = false;
				this.processRequestQueue();
			});

			this.ws.on('close', (code, reason) => {
				console.log(`ws: closed (${code}; ${Buffer.from(reason).toString('utf-8')})`);
				console.log("ws: reconnecting ...");
				setTimeout(
					() => this.doConnect(true),
					this.options.reconnectTimeout
				);
			});

			this.ws.on('ping', () => {
				// Pong messages are automatically sent -> count these messages into rate limit.
				// The pong message might be sent too early after the previous message so use
				// the time conforming the rate limit.
				this.lastRequestTime = Math.max(this.lastRequestTime + this.options.requestRateLimit, Date.now());
			})

			forEach(omit(this.listeners, "reconnect"), (callbacks, eventType) => {
				callbacks.forEach(callback =>
					this.ws!.on(eventType, callback)
				)
			})
		}
	}

	private async doSend(request: WsRequest) {
		return new Promise<void>((resolve, reject) => {
			this.ws?.send(JSON.stringify(request.message), (err) => {
				if (err) {
					console.error("ws send error:", err);
					return reject(err);
				}

				console.log("ws send", request.message);
				this.lastRequestTime = Date.now();
				resolve();
			});
		});
	}

	private async processRequestQueue() {
		if (this.isProcessingRequestQueue) {
			return;
		}

		this.isProcessingRequestQueue = true;

		while (this.requestQueue.length > 0) {
			while (Date.now() - this.lastRequestTime < this.options.requestRateLimit) {
				// Checking the rate limit in a loop because during the sleep the pong message could be sent.
				console.log(this.lastRequestTime, Date.now());
				console.log("wait", this.options.requestRateLimit - (Date.now() - this.lastRequestTime));

				await sleep(this.options.requestRateLimit - (Date.now() - this.lastRequestTime));
			}

			if (this.ws?.readyState !== WebSocket.OPEN) {
				console.error("ws send error: connection is not ready");
				break;
			}

			const request = this.requestQueue.shift()!;

			try {
				await this.doSend(request);
				request.onSuccess();
			} catch (e) {
				if (request.retryCount < 3) {
					++request.retryCount;
					setTimeout(() => {
						this.requestQueue.push(request)
						this.processRequestQueue();
					}, 5000);
				} else {
					request.onFailure();
				}
			}
		}

		this.isProcessingRequestQueue = false;
	}
}
