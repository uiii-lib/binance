import axios from 'axios';
import fs from 'fs';
import { camelCase, capitalize, get, upperFirst, values } from 'lodash';
import path from 'path';
import yaml from "yaml";
import ts, { ArrayTypeNode, SyntaxKind, TypeElement, TypeLiteralNode, TypeNode } from 'typescript';

const binanceSwaggerApiUrl = 'https://raw.githubusercontent.com/binance/binance-api-swagger/master/spot_api.yaml';

const typeMap: Record<string, string> = {
	string: 'string',
	number: 'number',
	integer: 'number',
	boolean: 'boolean'
};

function expandRef(schema: any, object: any) {
	const refPath = object?.['$ref']?.slice(2).replace(/\//g, '.');

	if (!refPath) {
		return object;
	}

	return get(schema, refPath);
}

function createCommentText(lines: string[]) {
	const nonEmptyLines = lines.filter(Boolean);

	if (nonEmptyLines.length === 0) {
		return undefined;
	}

	return `*\n${lines.filter(Boolean).map(text => text.split('\n').map(line => ` * ${line}`).join('\n')).join('\n')}\n `;
}

function formatExample(example: string) {
	return JSON.stringify(example);
}

function nodeWithComment(node: TypeElement, comment?: string) {
	if (!comment) {
		return node;
	}

	return ts.addSyntheticLeadingComment(
		node,
		SyntaxKind.MultiLineCommentTrivia,
		comment,
		true
	);
}

function createTypeNode(schema: any, response: any): TypeNode {
	if (response.$ref) {
		if (response.$ref.startsWith('#/components/schemas')) {
			const name = response.$ref.split('/').reverse()[0];
			return ts.factory.createTypeReferenceNode(`${upperFirst(name)}`);
		}

		return createTypeNode(schema, expandRef(schema, response));
	} else if (response.type === 'array') {
		return ts.factory.createArrayTypeNode(
			createTypeNode(schema, response.items)
		);
	} else if (response.type === 'object') {
		return ts.factory.createTypeLiteralNode(
			Object.entries<any>(response.properties || {}).map(([name, p]) =>
				nodeWithComment(
					ts.factory.createPropertySignature(
						undefined,
						ts.factory.createStringLiteral(name),
						response.required?.includes(name) ? undefined : ts.factory.createToken(ts.SyntaxKind.QuestionToken),
						createTypeNode(schema, p)
					),
					createCommentText([
						p.description,
						p.format,
						p.example && `@example ${formatExample(p.example)}`
					])
				)
			)
		)
	} else if (response.oneOf || response.anyOf) {
		return ts.factory.createUnionTypeNode(
			(response.oneOf || response.anyOf).map((it: any) => createTypeNode(schema, it))
		)
	} else if (typeMap[response.type]) {
		if (response.enum) {
			return ts.factory.createUnionTypeNode(response.enum.map((it: any) => {
				switch(typeMap[response.type]) {
					case 'string': return ts.factory.createStringLiteral(it)
					case 'number': return ts.factory.createNumericLiteral(it)
					default: throw new Error("Unsupported enum type");
				}
			}))
		}

		return ts.factory.createTypeReferenceNode(typeMap[response.type]);
	} else {
		throw new Error("Unsupported type");
	}
}

function createParametersTypeNode(method: string, route: any, schema: any, nodes: any[]) {
	const parametersTypeName = `${capitalize(method)}${upperFirst(camelCase(route.path))}Params`;
	let parametersTypeNode = ts.factory.createTypeReferenceNode(parametersTypeName);

	const parameters = route.parameters?.filter((p: any) => !["timestamp", "signature"].includes(p.name));

	if (!parameters) {
		return undefined;
	}

	nodes.push(
		ts.factory.createInterfaceDeclaration(
			undefined,
			parametersTypeName,
			undefined,
			undefined,
			parameters.map((p: any) => {
				return nodeWithComment(
					ts.factory.createPropertySignature(
						undefined,
						p.name,
						p.required ? undefined : ts.factory.createToken(ts.SyntaxKind.QuestionToken),
						createTypeNode(schema, p.schema)
					),
					createCommentText([
						p.description,
						p.schema.format,
						p.schema.example && `@example ${formatExample(p.schema.example)}`
					])
				)
			})
		),
		ts.factory.createIdentifier("\n")
	)

	return parametersTypeNode;
}

function createResponseTypeNode(method: string, route: any, schema: any, nodes: any[]) {
	const response = route.responses[200].content['application/json'].schema;

	let responseTypeNode = createTypeNode(schema, response);

	if (responseTypeNode.kind === SyntaxKind.TypeLiteral) {
		const responseTypeName = `${capitalize(method)}${upperFirst(camelCase(route.path))}Response`;

		nodes.push(
			ts.factory.createInterfaceDeclaration(
				undefined,
				responseTypeName,
				undefined,
				undefined,
				(responseTypeNode as TypeLiteralNode).members
			),
			ts.factory.createIdentifier("\n")
		);

		responseTypeNode = ts.factory.createTypeReferenceNode(responseTypeName);
	} else if (responseTypeNode.kind === SyntaxKind.ArrayType) {
		if ((responseTypeNode as ArrayTypeNode).elementType.kind === SyntaxKind.TypeLiteral) {
			const responseItemTypeName = `${capitalize(method)}${upperFirst(camelCase(route.path))}ResponseItem`;

			nodes.push(
				ts.factory.createInterfaceDeclaration(
					undefined,
					responseItemTypeName,
					undefined,
					undefined,
					((responseTypeNode as ArrayTypeNode).elementType as TypeLiteralNode).members
				),
				ts.factory.createIdentifier("\n")
			);

			responseTypeNode = ts.factory.updateArrayTypeNode(
				responseTypeNode as ArrayTypeNode,
				ts.factory.createTypeReferenceNode(responseItemTypeName)
			);
		}
	}

	return responseTypeNode;
}

async function main() {
	const response = await axios.get(binanceSwaggerApiUrl);
	const schema = yaml.parse(response.data);

	const routesByMethod: any = {};

	for (const routePath of Object.keys(schema.paths)) {
		const routePathDef = schema.paths[routePath];

		for (const method of Object.keys(routePathDef)) {
			if (!routesByMethod[method.toUpperCase()]) {
				routesByMethod[method.toUpperCase()] = {};
			}

			const route = routePathDef[method];

			routesByMethod[method.toUpperCase()][routePath] = {
				...route,
				path: routePath,
				parameters: route.parameters?.map((p: any) => expandRef(schema, p))
			}
		}
	}

	const nodes = [];
	const routeNodes: [string, any][] = [];
	const routeWeights: [string, string, number][] = [];

	for (const [name, typeDef] of Object.entries(schema.components.schemas)) {
		const type = createTypeNode(schema, typeDef);
		if (type.kind === SyntaxKind.TypeLiteral) {
			nodes.push(ts.factory.createInterfaceDeclaration(
				undefined,
				`${upperFirst(name)}`,
				undefined,
				undefined,
				(type as TypeLiteralNode).members
			));
		} else {
			nodes.push(ts.factory.createTypeAliasDeclaration(
				undefined,
				`${upperFirst(name)}`,
				undefined,
				type
			));
		}

		nodes.push(ts.factory.createIdentifier("\n"));
	}

	for (const [method, routes] of Object.entries<any>(routesByMethod)) {
		for (const route of Object.values<any>(routes)) {
			const weightMatch = route.description.match(/(^|\n) *Weight(\((IP|UID)\))?: +(\d+) *(\n|$)/);
			if (weightMatch) {
				route.weight = {
					[weightMatch[3] || "IP"]: weightMatch[4]
				}
			}

			const hasRequiredParams = route.parameters?.some((p: any) => p.required);
			const requiresWeight = !route.weight;

			const routeOptionsTypeNodes = [];

			const parametersTypeNode = createParametersTypeNode(method, route, schema, nodes);
			if (parametersTypeNode) {
				routeOptionsTypeNodes.push(
					ts.factory.createPropertySignature(
						undefined,
						ts.factory.createIdentifier("params"),
						hasRequiredParams ? undefined : ts.factory.createToken(ts.SyntaxKind.QuestionToken),
						parametersTypeNode
					)
				);
			}

			routeOptionsTypeNodes.push(
				ts.factory.createPropertySignature(
					undefined,
					ts.factory.createIdentifier("weight"),
					requiresWeight ? undefined : ts.factory.createToken(ts.SyntaxKind.QuestionToken),
					ts.factory.createTypeReferenceNode('BinanceRouteWeight')
				)
			);

			routeNodes.push([
				method,
				ts.factory.createPropertySignature(
					undefined,
					ts.factory.createStringLiteral(route.path),
					undefined,
					ts.factory.createTypeReferenceNode("BinanceRoute", [
						ts.factory.createTupleTypeNode([
							ts.factory.createNamedTupleMember(
								undefined,
								ts.factory.createIdentifier("options"),
								(hasRequiredParams || requiresWeight) ? undefined : ts.factory.createToken(ts.SyntaxKind.QuestionToken),
								ts.factory.createTypeLiteralNode(routeOptionsTypeNodes)
							)
						]),
						createResponseTypeNode(method, route, schema, nodes)
					])
				)
			]);
		}
	}

	nodes.push(
		ts.factory.createIdentifier(`
			export type BinanceRouteWeight = { "IP": number } | { "UID": number };

			export interface BinanceRouteOptions {
			    params?: Record<string, any>;
			    weight?: BinanceRouteWeight;
			}

			export interface BinanceRoute<A, R> {
			    args: A;
			    response: R;
			}

			export type BinanceRouteArgs<M extends keyof BinanceRoutes, P extends keyof BinanceRoutes[M]> =
			    "args" extends keyof BinanceRoutes[M][P]
			        ? (BinanceRoutes[M][P]["args"] extends any[]
			            ? BinanceRoutes[M][P]["args"]
			            : never)
			        : never;

			export type BinanceRouteResponse<M extends keyof BinanceRoutes, P extends keyof BinanceRoutes[M]> =
			    "response" extends keyof BinanceRoutes[M][P]
			        ? BinanceRoutes[M][P]["response"]
			        : never;
		`.replace(/(^\n|\t)/g, '')),
		ts.factory.createIdentifier("\n")
	);

	nodes.push(
		ts.factory.createInterfaceDeclaration(
			[ts.factory.createModifier(SyntaxKind.ExportKeyword)],
			'BinanceRoutes',
			undefined,
			undefined,
			Object.keys(routesByMethod).map((method: string) =>
				ts.factory.createPropertySignature(
					undefined,
					ts.factory.createStringLiteral(method),
					undefined,
					ts.factory.createTypeLiteralNode(
						routeNodes
							.filter(it => it[0] === method)
							.map(it => it[1])
					)
				)
			)
		),
		ts.factory.createIdentifier("\n")
	);

	nodes.push(
		ts.factory.createVariableStatement(
			[ts.factory.createModifier(ts.SyntaxKind.ExportKeyword)],
			[
				ts.factory.createVariableDeclaration(
					ts.factory.createIdentifier("binanceRoutesConfig"),
					undefined,
					undefined,
					ts.factory.createObjectLiteralExpression(
						Object.keys(routesByMethod).map((method: string) =>
							ts.factory.createPropertyAssignment(
								ts.factory.createStringLiteral(method),
								ts.factory.createObjectLiteralExpression(
									Object.values(routesByMethod[method]).map((route: any) =>
										ts.factory.createPropertyAssignment(
											ts.factory.createStringLiteral(route.path),
											ts.factory.createObjectLiteralExpression(
												[
													route.weight && (
														ts.factory.createPropertyAssignment(
															"weight",
															ts.factory.createObjectLiteralExpression(
																Object.entries(route.weight).map(([weightType, weight]) =>
																	ts.factory.createPropertyAssignment(
																		weightType,
																		ts.factory.createNumericLiteral(weight as number)
																	)
																),
															)
														)
													),
													route.parameters?.find((p: any) => p.name === "signature") && (
														ts.factory.createPropertyAssignment(
															"isSigned",
															ts.factory.createIdentifier("true")
														)
													)
												].filter(Boolean),
												true
											)
										)
									),
									true
								)
							)
						),
						true
					)
				)
			]
		)
	)

	const code = ts.factory.createNodeArray(nodes);

	const resultFile = ts.createSourceFile("model.ts", "", ts.ScriptTarget.Latest, /*setParentNodes*/ false, ts.ScriptKind.TS);
	const printer = ts.createPrinter({ newLine: ts.NewLineKind.LineFeed });

	const result = printer.printList(ts.ListFormat.MultiLine, code, resultFile);
	fs.writeFileSync(path.join(__dirname, 'src', 'model.ts'), result);
}

main();
